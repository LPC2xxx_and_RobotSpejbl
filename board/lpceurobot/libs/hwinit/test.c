#include <lpc21xx.h>                            /* LPC21xx definitions */
#include <errno.h>
#include "startcfg.h"
#include <deb_led.h>

extern unsigned int adc_val[4];

#define LEDS 4

int main (void)
{
	unsigned leds[] = {LEDR, LEDG, LEDB, LEDY};
	int i=0;
	while(1)
	{
		deb_led_set(leds[i]);
		if (++i == LEDS) i=0;
		wait();
	
	}
	return 0;
}
