#ifndef ERROR_H
#define ERROR_H

enum error {
	SUCCESS = 0,
	ERANGE = 11,
};

void error(enum error err);

#endif
