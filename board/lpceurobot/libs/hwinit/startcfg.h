


/** Fosc is crystal oscilator / resonator
 *  If is not defined, is used  14,745MHz nominal oscilator (CTU DRAGONS LPC BOARD )
 */
#ifndef FOSC // FIXME: rename to CPU_REF_HZ
  #define FOSC	14745
#endif


/** If memory is in RAM, it must be defined! Otherwise is defined MEM_FLASH
 *  This is important for MAM module.
 */
#ifndef MEM_RAM
  #define MEM_FLASH	
#endif


/// Absolute minimum and maximum ratings
#define FCCO_MIN 156000		// internal minimal clock
#define FCCO_MAX 320000		// internal maximal clock
#define CCLK_MIN 10000		// processor minimal clock
#define CCLK_MAX 60000		// processor maximal clock


/// PLL Available multiplicators 
#define PLL_MUL_1	0
#define PLL_MUL_2	1
#define PLL_MUL_3	2
#define PLL_MUL_4	3
#define PLL_MUL_5	4
#define PLL_MUL_6	5
#define PLL_MUL_7	6

/// PLL Available divisors
#define PLL_DIV_2	2
#define PLL_DIV_4	4
#define PLL_DIV_8	8
#define PLL_DIV_16	16

/// PLL mode
#define PLL_MODE_DISABLE	1
#define PLL_MODE_ENABLE		0

/// MAM definitions
#define MAM_OFF 	0 	// MAM functions disabled
#define MAM_PARTIAL 1 	// MAM functions partially enabled
#define MAM_FULL	2	// MAM functions fully enabled


/// APB divider
///@{
#define APB_DIV_1	1
#define APB_DIV_2	2
#define APB_DIV_4	0
///@}





void wait(void);  // in future this should be deleted



/** deb_led controls system debug leds
 *  @param	leds	LED value
 */
void deb_led(char leds);

/** Enables PLL for higher Fosc values
 *  @return 0 or #ERANGE
 *  @param	mul		PLL multiplicator
 *  @param	div		PLL divider
 *  @param	disable	disables PLL
 */
unsigned char init_PLL(char mul,char div, char mode);

/** get_sys_speed returns actual speed of Cclk (proc. frequency) in kHz
 */
unsigned int get_sys_speed(void);


/** Setup MAM module
 *  @return 0 or ERANGE
 *  @param	mode	MAM mode
 */
unsigned char init_MAM(char mode);

/** Sets APB divisor
 *  @return 0 
 *  @note VPB = APB   - name conflict FIXME
 *  @param	div	divisor of APB
 */
unsigned char set_APB(char div);

/** Returns actual speed of APB (peripheral frequency) in kHz
 *  @note VPB = APB   - name conflict FIXME
 */
unsigned int get_apb_speed(void);

