#include "startcfg.h"
#include <errno.h>		
#include <lpc21xx.h>                            /* LPC21xx definitions */


// -----------------   PLL part ------------------------
#define PLL_OFF			0		// fully disable PLL
#define PLL_ACTIVE		1		// activate PLL	
#define PLL_CONNECT		3		// connect PLL to Cclk

#define PLL_FEED_1		0xAA	// PLL feed sequence 1
#define PLL_FEED_2		0x55	// PLL feed sequence 3 							
#define PLL_LOCK_MASK	0x400	// PLL lock mask



#define PLL_DIV_MASK_2	(0<<5)
#define PLL_DIV_MASK_4	(1<<5)
#define PLL_DIV_MASK_8	(2<<5)
#define PLL_DIV_MASK_16	(3<<5)

// -----------------   MAM part ------------------------

#define MAM_1ST_BOUND	20000
#define MAM_2ND_BOUND	40000
#define MAM_TIM_1	1
#define MAM_TIM_2	2
#define MAM_TIM_3	3


//------------------  code  ----------------------------

void wait(void)
{
	unsigned int i =2000000;
	while(--i);
}


void deb_led(char leds)
{
	IO0DIR |= ((1<<21) | (1<<22) | (1<<23) | (1<<24));

	if (leds & 0x1) IOSET0 |=  (1<<21);
		else IOCLR0 |= (1<<21);

	if (leds & 0x2) IOSET0 |=  (1<<22);
		else IOCLR0 |= (1<<22);

	if (leds & 0x4) IOSET0 |=  (1<<23);
		else IOCLR0 |= (1<<23);

	if (leds & 0x8) IOSET0 |=  (1<<24);
		else IOCLR0 |= (1<<24);
}





// setup procesor PLL
unsigned char init_PLL(char mul,char div, char mode)
{
	
	unsigned int fcco, cclk; 


	PLLCON = PLL_OFF;		// disable PLL		
	PLLFEED = PLL_FEED_1; 	// PLL change sequence
	PLLFEED = PLL_FEED_2;
	
	if (mode == PLL_MODE_DISABLE)
	{
		return 0;
	}

	fcco = FOSC * 2 * (mul + 1) * div ;	   		// count Fcco
	if (( FCCO_MIN > fcco)|(fcco >  FCCO_MAX))	// check Fcco range
	{
		return 	 ERANGE;
	}

	cclk = (mul + 1) * FOSC;							// count cclk
	if (( CCLK_MIN > cclk)|(cclk >  CCLK_MAX))	// check cclk range
	{
		return 	 ERANGE;
	}

	switch(div)
	{
		case(PLL_DIV_2):	div = PLL_DIV_MASK_2;
							break;

		case(PLL_DIV_4):	div = PLL_DIV_MASK_4;
							break;

		case(PLL_DIV_8):	div = PLL_DIV_MASK_8;
							break;

		case(PLL_DIV_16):	div = PLL_DIV_MASK_16;
							break;

		default:			return ERANGE;
	}


	PLLCFG = mul | div;		// write multiplicator and dividet to PLL config
 	PLLCON = PLL_ACTIVE;	// enable PLL		
	PLLFEED = PLL_FEED_1; 	// PLL change sequence
	PLLFEED = PLL_FEED_2;

	while( (PLLSTAT & PLL_LOCK_MASK) == 0); 	// wait for PLL LOCK

	PLLCON = PLL_CONNECT;	// connect PLL to Cclk
	PLLFEED = PLL_FEED_1; 	// PLL change sequence
	PLLFEED = PLL_FEED_2;

	return 0;
}


// get procesor speed
unsigned int get_sys_speed(void)
{
	if( (PLLCON & PLL_CONNECT) == PLL_CONNECT) // if PLL is connected return PLL-Cclk speed
	{
    	return( FOSC * ((PLLCFG & 0x1F) + 1));
    }

 	return FOSC;	// if PLL is disabled return native Fosc

}


// setup MAM module

unsigned char init_MAM(char mode)
{
	unsigned int clk;

	MAMCR = MAM_OFF;

	#ifdef MEM_RAM	  // FIXME udelat pomoci linker scriptu
		return 0;
	#endif


	if (mode == MAM_OFF) return 0;
	

	clk = get_sys_speed();

	if(clk > MAM_2ND_BOUND)
	{
		MAMTIM = MAM_TIM_3; 
	} 
	else if (clk > MAM_1ST_BOUND) 
	{
		MAMTIM = MAM_TIM_2;
	}
	else MAMTIM = MAM_TIM_1;

	MAMCR =	mode;

	return 0;
}

// setup APB module
unsigned char set_APB(char div)
{
	VPBDIV = div;
	return 0;
}


unsigned int get_apb_speed(void)
{
	int sysClk,div;

	switch(	VPBDIV & 0x03)
	{
		case APB_DIV_1:  div = 1;
			break;

		case APB_DIV_2:  div = 2;
			break;

		case APB_DIV_4:  div = 4;
			break;

		default:  return ERANGE;
	}


	sysClk = get_sys_speed();

	return (sysClk / div);


}


