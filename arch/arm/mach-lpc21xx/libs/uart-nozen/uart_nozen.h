/********************************************************/
/*	NOZEN UART library for LPC ARM			*/
/*							*/
/*		ver. 3.0 release			*/
/*							*/
/* (c) 2006, 2007 Ondrej Spinka, DCE FEE CTU Prague	*/
/* modified by Jiri Kubias 2008, DCE FEE CTU Prague	*/
/*							*/
/* no animals were harmed during development/testing 	*/
/* of this software product, except the author himself	*/
/*							*/
/* you may use this library for whatever purpose you 	*/
/* like, safe for satanistic rituals.			*/
/********************************************************/

#include <types.h>
#include <lpc21xx.h>


/*! @defgroup defines Constants Definitions
* @{
*/

/*!\def SYSTEM_CLK
* System clock frequency in Hz.
*/
// #define SYSTEM_CLK 10000000

/*!\def UART_BUFF_LEN
* Incomming data buffer length.
*/
#ifndef UART_BUFF_LEN 
#define UART_BUFF_LEN 64
#endif

/*!\def UART_BITS
* Number of bits.
*/
#define UART_BITS_5	0x0
#define UART_BITS_6	0x1
#define UART_BITS_7	0x2
#define UART_BITS_8	0x3

/*!\def UART_BITS
* Number of stopbits.
*/
#define UART_STOP_BIT_1		(0x0 << 2)
#define UART_STOP_BIT_2 	(0x1 << 2)

/*!\def UART_BITS
* UART parits.
*/
#define UART_PARIT_ENA		(0x1 << 3)
#define UART_PARIT_OFF		(0x0 << 3)
#define UART_PARIT_ODD   	(0x0 << 4)
#define UART_PARIT_EVEN  	(0x1 << 4)
#define UART_PARIT_FORCE_1 	(0x2 << 4)
#define UART_PARIT_FORCE_0 	(0x3 << 4)
/*! @} */

/*!\def UART1_OFFSET
* UART1 address offset.
*/
#define UART1_OFFSET 0x4000

/*!\def UART0
* UART0 number.
*/
#define UART0 0

/*!\def UART1
* UART1 number.
*/
#define UART1 1
/*! @} */

/*! @defgroup prototypes Function Prototypes
* @{
*/

/*! UART initialization function.
* Takes three arguments:
* \param uart_num unsigned 8-bit int UART device number
* \param baud_rate unsigned 32-bit int baud rate in bps
* \param rx_isr_vect unsigned 32-bit int interrupt vector number
* \param bits  unsigned 8-bit int number of transmited bits
* \param stopbit unsigned 8bit int number of stop
* \param parit_en  unsigned 8-bit enable or dislable parity generator
* \param parit_mode unsigned 8-bit selects type of parity (if parit_en is ON)
*/
void UART_init ( uint8_t uart_num, unsigned rx_isr_vect, uint32_t baud_rate ,uint8_t bits, uint8_t stopbit, uint8_t parit_en, char parit_mode);


/*! Data read function.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns readed character (unsigned 8-bit int).
*/
uint8_t read_UART_data ( uint8_t uart_num );

/*! Determine possible UART transmission errors.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns error code (0 no errors, -1 UART error, -2 incoming data buffer overrun).
*/
int8_t UART_test_err ( uint8_t uart_num );

/*! Determine whether new data wait in the UART round buffer.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns 1 if there are unreaded data in the incoming data buffer, 0 otherwise.
*/
inline uint8_t UART_new_data ( uint8_t uart_num );

/*! Data write function.
* Takes two arguments:
* \param uart_num unsigned 8-bit UART number (0 or 1)
* \param data unsigned 8-bit int byte (character) to send
*/
void write_UART_data ( uint8_t uart_num, uint8_t data );
/*! @} */
