/********************************************************/
/*	ZEN UART library for LPC ARM			*/
/*							*/
/*		ver. 3.0 release			*/
/*							*/
/* (c) 2006, 2007 Ondrej Spinka, DCE FEE CTU Prague	*/
/* modified by Jiri Kubias 2008, DCE FEE CTU Prague	*/
/*							*/
/* no animals were harmed during development/testing 	*/
/* of this software product, except the author himself	*/
/*							*/
/* you may use this library for whatever purpose you 	*/
/* like, safe for satanistic rituals.			*/
/********************************************************/


#include <system_def.h>
#include "uart_nozen.h"

/*! @defgroup defines Constants Definitions
* @{
*/

/*!\def RX_MASK
* Interrupt identification mask.
*/
#define RX_MASK 0x0E

/*!\def RX_INT
* Receive data available mask.
*/
#define RX_INT 0x04

/*!\def TX_EMPTY
* Transmitter holding register empty mask.
*/
#define TX_EMPTY 0x20

/*!\def UART_REG_OFFSET
* Makro for register address computation according respective UART number.
*/
#define UART_REG_ADDR(base_reg, dev_num) ( *( (volatile char *) ( ( (volatile void *) &base_reg ) + ( (dev_num) * UART1_OFFSET ) ) ) )
/*! @} */

#define  UART_DLAB		0x80   // FIXME

volatile uint8_t err_flag[2] = {0, 0}, new_data_flag[2] = {0, 0}, over_flag[2] = {0, 0}; //!< UART0 and UART1 error flags, new data flags and buffer overflow flags
volatile uint8_t i_w[2] = {0, 0}, i_r[2] = {0, 0}; //!< UART0 and UART1 read and write buffer indexes
uint8_t buff[2][UART_BUFF_LEN]; //!< UART0 and UART1 data buffers

/*! UART0 interrupt handler prototype */
void UART0_irq ( void ) __attribute__ ( ( interrupt ) );

/*! UART1 interrupt handler prototype */
void UART1_irq ( void ) __attribute__ ( ( interrupt ) );

/*! UART interrupt service routine.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
*/
void UART_isr ( uint8_t uart_num ) {
	if ( i_w[uart_num] > UART_BUFF_LEN - 1 ) i_w[uart_num] = 0; // when buffer is full, start overwriting oldest data
	
	err_flag[uart_num] = UART_REG_ADDR ( U0LSR, uart_num ) & 0x8E; // check the line status
	
	if ( ( UART_REG_ADDR ( U0IIR, uart_num ) & RX_MASK ) == RX_INT ) { // if Rx data ready IRQ is pending
		buff[uart_num][i_w[uart_num]++] = UART_REG_ADDR ( U0RBR, uart_num ); // store new character into the receive buffer
		new_data_flag[uart_num] = 1; // set the new data flag
	}
	
	if ( i_w[uart_num] == i_r[uart_num] ) { // when round buffer is full
		i_r[uart_num]++; // shift the oldest byte pointer
		over_flag[uart_num] = 1; // set the data overwrite flag
	}
	
	VICVectAddr = 0; // int acknowledge
}

/*! UART0 Rx interrupt service rutine.
* This rutine might be modified according user demands.
*/
void UART0_irq ( void ) {
	UART_isr ( UART0 );
}

/*! UART1 Rx interrupt service rutine.
* This rutine might be modified according user demands.
*/
void UART1_irq ( void ) {
	UART_isr ( UART1 );
}

/*! UART initialization function.
* Takes three arguments:
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \param baud_rate unsigned 32-bit int baud rate in bps
* \param rx_isr_vect unsigned 32-bit int interrupt vector number
*/
void UART_init ( uint8_t uart_num, unsigned rx_isr_vect, uint32_t baud_rate ,uint8_t bits, uint8_t stopbit, uint8_t parit_en, char parit_mode )
 {
	uint16_t divisor =  (CPU_VPB_HZ + (baud_rate * 16)/2) / (baud_rate * 16); // baud divisor computation
	
	
	PINSEL0 &= ( 0xFFFFFFF0 - ( uart_num * 0xEFFF1 ) ); //enable UART functionality on respective processor pins
	PINSEL0 |= ( 0x00000005 + ( uart_num * 0x4FFFB ) );
	UART_REG_ADDR ( U0LCR, uart_num ) =  UART_DLAB | bits | stopbit | parit_en | parit_mode;  //0x83; // 8-bit data, no parity, set DLAB = 1
	UART_REG_ADDR ( U0DLL, uart_num ) = *(uint8_t *) &divisor; // write divisor lo-byte
	UART_REG_ADDR ( U0DLM, uart_num ) = *( ( (uint8_t *) &divisor ) + 1 ); // write divisor hi-byte
	UART_REG_ADDR ( U0LCR, uart_num ) &= 0x7F; // clear DLAB
	UART_REG_ADDR ( U0FCR, uart_num ) = 0x01; // enable UART FIFO, interrupt level 1 byte - this might be modified by the user
	UART_REG_ADDR ( U0IER, uart_num ) = 0x01; // enable RBR interrupt
	
	if ( !uart_num ) ( ( uint32_t * ) &VICVectAddr0 )[rx_isr_vect] = ( uint32_t ) UART0_irq; // if UART0 register UART0 interrupt handler
	else ( ( uint32_t * ) &VICVectAddr0 )[rx_isr_vect] = ( uint32_t ) UART1_irq; // else register UART1 interrupt handler

	( ( uint32_t * ) &VICVectCntl0 )[rx_isr_vect] = 0x20 | ( 6 + uart_num ); // enable IRQ slot, set UART interrupt number
	VICIntEnable = ( ( uart_num + 1 ) * 0x00000040 ); //enable UART IRQ
}

/*! Data read function.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns readed character (unsigned 8-bit int).
*/
uint8_t read_UART_data ( uint8_t uart_num ) {
	uint8_t data; // readed data byte
	
	while ( !new_data_flag[uart_num] ); // wait for new Rx data
	
	if ( i_r[uart_num] > UART_BUFF_LEN - 1 ) i_r[uart_num] = 0; // when top of the buffer is reached, return the read pointer to 0
	data = buff[uart_num][i_r[uart_num]++]; // read new data from the data buffer
	if ( i_r[uart_num] == i_w[uart_num] ) new_data_flag[uart_num] = 0; // when all data was read clear the new data flag
	
	return data; // return new data byte
}

/*! Determine possible UART transmission errors.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns error code (0 no errors, -1 UART error, -2 incoming data buffer overrun).
*/
int8_t UART_test_err ( uint8_t uart_num ) {
	if ( err_flag[uart_num] ) { // check the UART0 error flag
		err_flag[uart_num] = 0; // reset the UART0 error flag
		return -1; // return error code
	}
	
	if ( over_flag[uart_num] ) { // check the UART0 incoming data buffer overrun flag
		over_flag[uart_num] = 0; // reset the UART0 incoming data buffer overrun flag
		return -2; // return error code
	}
	
	return 0; // no errors
}

/*! Determine whether new data wait in the UART round buffer.
* \param uart_num unsigned 8-bit int UART number (0 or 1)
* \return Returns 1 if there are unreaded data in the incoming data buffer, 0 otherwise.
*/
inline uint8_t UART_new_data ( uint8_t uart_num ) {
	return new_data_flag[uart_num]; // return UART new data flag
}

/*! Data write function.
* Takes two arguments:
* \param uart_num unsigned 8-bit UART number (0 or 1)
* \param data unsigned 8-bit int byte (character) to send
*/
void write_UART_data ( uint8_t uart_num, uint8_t data ) {
	while ( !( UART_REG_ADDR ( U0LSR, uart_num ) & TX_EMPTY ) ); // wait for the transmitter holding register emptying
	UART_REG_ADDR ( U0THR, uart_num ) = data; // write the data byte to the transmitter holding register
}
