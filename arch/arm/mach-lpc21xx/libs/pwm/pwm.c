#include "pwm.h"

int PWM_PINSEL[] = {
  /*nothing*/ 1, /*PWM1*/ 1, 15, 3, 17, /*PWM5*/ 11, /*PWM6*/ 19
};

uint32_t *PWM_MR[] = {
  (uint32_t*)&(PWMMR0),
  (uint32_t*)&(PWMMR1),
  (uint32_t*)&(PWMMR2),
  (uint32_t*)&(PWMMR3),
  (uint32_t*)&(PWMMR4),
  (uint32_t*)&(PWMMR5),
  (uint32_t*)&(PWMMR6)
};

void pwm_channel(int n, int double_edge) {
  uint32_t bit;

  PWMPCR |= (0x100 | (double_edge && n)) << n;
  if (n == 5) {
    PINSEL1 |= 0x00000400;
    PINSEL1 &= 0xfffff7ff;
  }
  else {
    bit = 1 << PWM_PINSEL[n];
    PINSEL0 |= bit;
    bit = ~(bit >> 1);
    PINSEL0 &= bit;
  }
}

void pwm_init(uint32_t prescale, uint32_t period) {
  PWMPR = prescale - 1;
  PWMMR0 = period;
  PWMLER |= 0x1;
  PWMMCR |= 0x00000002;
  PWMTCR &= ~0x2;
  PWMTCR |= 0x9;
}
