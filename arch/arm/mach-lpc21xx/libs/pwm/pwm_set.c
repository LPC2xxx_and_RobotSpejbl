#include "pwm.h"

void pwm_set(int n, uint32_t when) {
  *PWM_MR[n] = when;
  PWMLER |= 1 << n;
}

void pwm_set_double(int n, uint32_t from, uint32_t to) {
  *PWM_MR[n-1] = from;
  *PWM_MR[n] = to;
  PWMLER |= 0x3 << (n-1);
}
