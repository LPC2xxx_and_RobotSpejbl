#ifndef _CAN_VCA_H
#define _CAN_VCA_H

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************/
/* VCA Basic Functions */

#define VCA_OK 0

typedef int vca_handle_t;

long vca_h2log(vca_handle_t vcah);

#define VCA_O_NONBLOCK 1

int lpc_vca_open_handle(vca_handle_t *vcah_p, int dev, int flags, uint32_t btr, unsigned rx_ivect, unsigned tx_ivect, unsigned err_ivect);
int vca_close_handle(vca_handle_t vcah);
int vca_send_msg_seq(vca_handle_t vcah, canmsg_t *messages, int count);
int vca_rec_msg_seq(vca_handle_t vcah, canmsg_t *messages, int count);
int vca_wait(vca_handle_t vcah, int wait_msec, int what);


#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _CAN_VCA_H */
