#include <string.h>
#include <periph/can.h>

/*
 * transmit CAN message, which will be immediately received by
 * transmitter itself
 * (rudimentary workaround for CAN.7 erratum)
 */
int can_self_tx_msg(can_msg_t *tx_msg) {
  uint32_t data[2];

  /* check, if buffer is ready (previous Tx completed) */
  if ((C1SR & 0x4) == 0)
    return -1; /* busy */

  memcpy(data, tx_msg->data, 8);
  C1TFI1 = (tx_msg->flags & 0xc0000000) |
    ((tx_msg->dlc<<16) & 0x000f0000);
  C1TID1 = tx_msg->id;
  C1TDA1 = data[0];
  C1TDB1 = data[1];
  /* start transmission */
  C1CMR = 0x30;
  return 0; /* OK */
}

/*EOF*/
