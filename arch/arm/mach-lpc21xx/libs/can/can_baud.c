
////////////////////////////////////////////////////////////////////////////////
//
//                 Philips LPC2129 CAN Example
//
// Description
// -----------
//  This example demonstrates setup of CAN module
//
// Author : Jiri Kubias DCE CVUT
// Modified : 13.03.08: added Autobaud calc.	Jiri Kubias DCE CVUT	
//	
// Prereq: startcfg.h startcfg.h types.h
//
////////////////////////////////////////////////////////////////////////////////





#include <periph/can.h>
#include <system_def.h>

//#include "types.h"

//volatile int can_msg_received = 0;
//volatile can_msg_t can_rx_msg;

/* default receive interrupt handler */
//void can_rx_isr() __attribute__((interrupt));

/* private global variables */
//uint32_t *can_rx_msg_data = (uint32_t*)can_rx_msg.data;
//can_rx_callback can_rx_cb;



//void can_init(uint32_t bitrate, unsigned rx_isr_vect, can_rx_callback rx_cb)
void can_init_baudrate(uint32_t baudrate, unsigned rx_isr_vect, can_rx_callback rx_cb) 
{
	uint32_t tseg1, tseg2, i, quanta, best, sjw, divider, clk, q, btr_calc, diff;

	best = 0xFFFFFFFF; 
	quanta = 0;


	clk = CPU_APB_HZ;//get_apb_speed() * 1000; FIXME  convert to system_def.h CPU_APB_HZ

	for ( i = 8 ; i <= 25 ; i++){
		int q = baudrate * i;
		divider = clk / q;

		btr_calc = clk / (i * divider);
		
		if(btr_calc > baudrate)  diff = btr_calc - baudrate;
		else diff = baudrate - btr_calc;

		if (diff < best) {
			best = diff;
			quanta = i;
		}
	}

	q = baudrate * quanta;
	divider  = (clk + q/2) / q;

	tseg1  = ((quanta * 70) / 100);	// this is tseg1 
	tseg2  = quanta - tseg1;		//(quanta[i] - ((quanta[i] * 70) / 100)); // this is Tseg2
	
	tseg1 -= 1;
	tseg2 -= 1;
	divider -= 1;

	
	if (tseg2  < 5)
		sjw = tseg2 -1;
	else 
		sjw = 4;


	// TODO samling point
		

	/* enable CAN1 Rx pin */
	PINSEL1 |= 0x00040000;
	PINSEL1 &= 0xfff7ffff;

	/* receive all messages, no filtering */
	AFMR = 0x2;

	/* reset mode */
	C1MOD = 0x1;

	/* -- addition from lpc2000 maillist msg #3052: */
	C1CMR = 0x0e; //Clear receive buffer, data overrun, abort tx

	/* -- end of addition */
	C1IER = 0x0;
	C1GSR = 0x0;

	/* set baudrate & timing */

	C1BTR = divider  | (tseg1  << 16) \
		| (tseg2  << 20) | (sjw<<14);

	/* register Rx handler */
	can_rx_cb = rx_cb;

	/* set interrupt vector */
	((uint32_t*)&VICVectAddr0)[rx_isr_vect] = (uint32_t)can_rx_isr;
	((uint32_t*)&VICVectCntl0)[rx_isr_vect] = 0x20 | 26;

	/* enable Rx int */
	VICIntEnable = 0x04000000;
	C1IER = 0x1;

	/* normal (operating) mode */
	C1MOD = 0x0;

	#if 0
	/* LPC2119 CAN.5 erratum workaround */
	C1TFI1 = 0x00000000;
	C1TID1 = 0x0;
	C1CMR = 0x23;
	#endif
}



/*EOF*/
