#ifndef _CANMSG_T_H
#define _CANMSG_T_H

#include <types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CAN_MSG_LENGTH 8

typedef uint32_t canmsg_id_t;

/**
 * struct canmsg_t - structure representing CAN message
 * @flags:  message flags
 *     %MSG_RTR .. message is Remote Transmission Request,
 *     %MSG_EXT .. message with extended ID, 
 *     %MSG_OVR .. indication of queue overflow condition (!unused!),
 *     %MSG_LOCAL .. message originates from this node (!unused!).
 * @id:     ID of CAN message
 * @length: length of used data
 * @data:   data bytes buffer
 *
 * Header: canmsg.h
 */
struct canmsg_t {
  uint8_t flags;
  canmsg_id_t id;
  uint8_t length;
  uint8_t data[CAN_MSG_LENGTH];
};

typedef struct canmsg_t canmsg_t;

/* Definitions to use for canmsg_t and canfilt_t flags */
#define MSG_RTR   (1<<0)
#define MSG_OVR   (1<<1)
#define MSG_EXT   (1<<2)
#define MSG_LOCAL (1<<3)

/* Can message ID mask */
#define MSG_ID_MASK ((1l<<29)-1)

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_CANMSG_T_H*/
