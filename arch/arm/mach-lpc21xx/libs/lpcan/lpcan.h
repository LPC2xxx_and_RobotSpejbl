#include <stdlib.h>
#include <types.h>
#include <lpc21xx.h>

#define CAN_NUM_DEVICES    2

#define CAN_RX   (1<<0)
#define CAN_TX   (1<<1)
#define CAN_ERR  (1<<2)

#define CAN_ERR_RXOVER     (1<<0)
#define CAN_ERR_ERRSTATUS  (1<<1)
#define CAN_ERR_BUSOFF     (1<<2)
#define CAN_ERR_RXBUSERR   (1<<3)
#define CAN_ERR_TXBUSERR   (1<<4)

typedef void (*can_callback)(int device, int events);

/* function prototypes */
void can_init(int device, uint32_t btr,
	      unsigned rx_ivect, unsigned tx_ivect, unsigned err_ivect,
	      can_callback cb, int cb_event_mask);
void can_off(int device);
int can_rx_msg(int device, canmsg_t *msg);
int can_tx_msg(int device, canmsg_t *msg);
int can_rx_empty(int device);
int can_tx_empty(int device);
unsigned can_error(int device);

int lpcan_btr(uint32_t *btr, int rate, int clock,
	      int sjw, int sampl_pt, int sam);

/*EOF*/
