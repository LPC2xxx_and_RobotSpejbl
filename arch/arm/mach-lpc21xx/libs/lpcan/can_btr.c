/*
 * ON-TRACK CAN driver library for Philips LPC2xxx microcontroller family
 * Marek Peca <mp@duch.cz>, 2008/04
 *
 * Bitrate calculation (SJA1000 semi-compatible)
 *   copied and adapted from OCERA LinCAN driver
 *   by Pavel Pisa <pisa@cmp.felk.cvut.cz>
 */

#include <types.h>
#include <limits.h>

#define MAX_TSEG1 15
#define MAX_TSEG2 7

/**
 * @param btr pointer to resulting BTR bit timing register value
 * @param rate bit rate [bit/s]
 * @param clock peripheral bus clock frequency [Hz]
 * @param sjw Synchronization Jump Width (0..3)
 * @param sampl_pt sampling point position 0<sampl_pt<100 [%]
 * @param sam 0 for bus being sampled once, 1 for bus being sampled 3x
 * @return 0 if calculation successful, nonzero otherwise
 *
 * Calculate the bit timing register (BTR) value according to given
 * frequency and sampling specification.
 */
int lpcan_btr(uint32_t *btr, int rate, int clock, int sjw, int sampl_pt, int sam) {
  int best_error = INT_MAX, error;
  int best_tseg = 0, best_brp = 0, best_rate = 0, brp = 0;
  int tseg = 0, tseg1 = 0, tseg2 = 0;
        
  /* tseg even = round down, odd = round up */
  for (tseg = (0+0+2)*2; tseg <= (MAX_TSEG2+MAX_TSEG1+2)*2+1; tseg++) {
    brp = clock/((1+tseg/2)*rate)+tseg%2;
    if ((brp == 0) || (brp > 1024))
      continue;
    error = rate - clock/(brp*(1+tseg/2));
    if (error < 0)
      error = -error;
    if (error <= best_error) {
      best_error = error;
      best_tseg = tseg/2;
      best_brp = brp-1;
      best_rate = clock/(brp*(1+tseg/2));
    }
  }
  if (best_error && (rate/best_error < 10)) {
    /*
    printf("baud rate %d is not possible with %d Hz clock\n",
	   rate, 2*clock);
    printf("%d bps. brp=%d, best_tseg=%d, tseg1=%d, tseg2=%d\n",
	   best_rate, best_brp, best_tseg, tseg1, tseg2);
    */
    return -1;
  }
  tseg2 = best_tseg-(sampl_pt*(best_tseg+1))/100;
  if (tseg2 < 0)
    tseg2 = 0;
  if (tseg2 > MAX_TSEG2)
    tseg2 = MAX_TSEG2;
  tseg1 = best_tseg-tseg2-2;
  if (tseg1 > MAX_TSEG1) {
    tseg1 = MAX_TSEG1;
    tseg2 = best_tseg-tseg1-2;
  }

  /*
  printf("Setting %d bps.\n", best_rate);
  printf("brp=%d, best_tseg=%d, tseg1=%d, tseg2=%d, sampl_pt=%d\n",
	 best_brp, best_tseg, tseg1, tseg2,
	 (100*(best_tseg-tseg2)/(best_tseg+1)));
  */

  *btr = (sam<<23) | (tseg2<<20) | (tseg1<<16) | (sjw<<14) | best_brp;
  return 0;
}

/* EOF */
