#include <lpc21xx.h>
#include "pll.h"

/**
 * @param f_cclk CPU clock (PLL output) frequency [Hz]
 * @param f_osc input oscillator/crystal clock frequency [Hz]
 *
 * Switch LPC2xxx PLL on.
 */
void sys_pll_init(uint32_t f_cclk, uint32_t f_osc) {
  uint32_t m, p, p_max;

  /* turn memory acceleration off */
  MAMCR = 0x0;

  /* compute PLL divider and internal osc multiplier */
  m = f_cclk/f_osc;
  p_max = 160000000/f_cclk;
  for (p = 3; ((1<<p) > p_max) && (p > 0); p = p>>1);
  /* switch PLL on */
  PLLCFG = (p<<5) | ((m-1)&0x1f);
  PLLCON = 0x1;
  PLLFEED = 0xaa;  PLLFEED = 0x55;
  /* wait for lock */
  while (!(PLLSTAT & (1<<10)));
  /* connect to clock */
  PLLCON = 0x3;
  PLLFEED = 0xaa;  PLLFEED = 0x55;

  /* turn memory acceleration on */
  MAMTIM = f_cclk/20000000;
  MAMCR = 0x2;
}
