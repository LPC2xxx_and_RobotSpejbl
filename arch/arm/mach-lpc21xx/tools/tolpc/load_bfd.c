#include <stdlib.h>
#include <bfd.h>
#include <stdio.h>
#include "tolpc_fn.h"
#include "load_bfd.h"


/**
 * extract loadable sections from bfd file and load them
 * @param fname bfd binary file name
 * @param target NULL for default or string description (eg. "elf32-m68hc12")
 * @param loader_cb section loader callback
 * @param start_address code start address extracted from bfd
 * @param verbose operation is silent if 0, sections are listed otherwise
 * @return 0 if successful
 */
int load_bfd(char *fname, char *target, load_section loader_cb,
	     bfd_vma *start_address, int verbose) {
  bfd *abfd;
  asection *sec;
  bfd_vma lma, vma, size;
  bfd_boolean loadable;
  void *contents;

  bfd_init();
  if ((abfd = bfd_openr(fname, target)) == NULL) {
    bfd_perror("load_bfd: bfd_openr");
    return(1);
  }
  if (!bfd_check_format(abfd, bfd_object)) {
    bfd_perror("load_bfd: bfd_check_format");
    return(1);
  }

  /* determine start address */
  *start_address = bfd_get_start_address(abfd);

  tolpc_verbose(1, "Loading %s\n"
                    "start address 0x%x\n"
                    "loading sections:\n", fname, (unsigned)*start_address);
  
  /* load sections */
  for (sec = abfd->sections; sec != NULL; sec = sec->next) {
    /* get section load address and size */
    lma = bfd_get_section_lma(abfd, sec);
    vma = bfd_get_section_vma(abfd, sec);
    size = bfd_section_size(abfd, sec);
    loadable = bfd_get_section_flags(abfd, sec) & SEC_LOAD;
    if (!loadable || (size == 0))
      continue;
    
    tolpc_verbose(1, "%s : ", bfd_get_section_name(abfd, sec));
    tolpc_verbose(1, "lma=0x%x, vma=0x%x, len=%d",
	     (unsigned)lma, (unsigned)vma, (unsigned)size);
    
    /* extract section contents */
    if ((contents = malloc(size)) == NULL) {
      perror("load_bfd: malloc");
      return(1);
    }
    if (!bfd_get_section_contents(abfd, sec, contents, /*offs.?*/ 0, size)) {
      bfd_perror("load_bfd: bfd_get_section_contents");
      return(1);
    }
    /* call the loader */
    if (loader_cb(lma, size, contents))
      return(1);
    free(contents);
  }

  tolpc_verbose(1, "Loading of %s finished.\n", fname);

  bfd_close(abfd);
  /* OK */
  return(0);
}

#if TRAPARNA
int null_loader(bfd_vma lma, bfd_size_type size, void *data) {
  return(0);
}

main(int argc, char *argv[]) {
  char *fname = argv[1];
  bfd_vma start;

  if (!load_bfd(fname, NULL, null_loader, &start, 1))
    printf("Nahrano, staci skocit na adresu 0x%x.\n", start);
  else
    printf("@#%!@$# neco se posrabilo.\n");
}
#endif
