/******************************************************************************
 *
 * $RCSfile: lpcUSB.h,v $
 * $Revision: 1.3 $
 *
 * Header file for Philips LPC214x USB enabled ARM Processors 
 * Copyright 2006 Pavel Pisa <pisa@cmp.felk.cvut.cz>
 *
 * No guarantees, warrantees, or promises, implied or otherwise.
 * May be used for hobby or commercial purposes provided copyright
 * notice remains intact or GPL license is applied.
 *
 *****************************************************************************/

/* USBIntSt - USB Interrupt Status (R/W) */
#define   USB_INT_REQ_LP	(1<<0) /*Low priority interrupt line status (RO) */
#define   USB_INT_REQ_HP	(1<<1) /*High priority interrupt line status. (RO) */
#define   USB_INT_REQ_DMA	(1<<2) /*DMA interrupt line status. This bit is read only. (LPC2146/8 only) 0*/
#define   USB_need_clock	(1<<8) /*USB need clock indicator*/
#define   USB_EN_USB_INTS	(1<<31) /*Enable all USB interrupts*/

/* Device interrupt registers */
#define USBDevIntSt_o	0x0000	/* USB Device Interrupt Status (RO) */
#define USBDevIntEn_o	0x0004	/* USB Device Interrupt Enable (R/W) */
#define USBDevIntClr_o	0x0008	/* USB Device Interrupt Clear (WO) */
#define USBDevIntSet_o	0x000C	/* USB Device Interrupt Set (WO) */
#define   USBDevInt_FRAME	(1<<0) /*Frame interrupt @1kHz for ISO transfers*/
#define   USBDevInt_EP_FAST	(1<<1) /*Fast interrupt transfer for the endpoint*/
#define   USBDevInt_EP_SLOW	(1<<2) /*Slow interrupt transfer for the endpoint*/
#define   USBDevInt_DEV_STAT	(1<<3) /*USB Bus reset, USB suspend change or Connect occured*/
#define   USBDevInt_CCEMTY	(1<<4) /*Command code register is empty/ready for CMD*/
#define   USBDevInt_CDFULL	(1<<5) /*Command data register is full/data available*/
#define   USBDevInt_RxENDPKT	(1<<6) /*Current packet in the FIFO is transferred to the CPU*/
#define   USBDevInt_TxENDPKT	(1<<7) /*TxPacket bytes written to FIFO*/
#define   USBDevInt_EP_RLZED	(1<<8) /*Endpoints realized after Maxpacket size update*/
#define   USBDevInt_ERR_INT	(1<<9) /*Error Interrupt - Use Read Error Status Command 0xFB*/

#define USBDevIntPri_o	0x002C	/* USB Device Interrupt Priority (WO) */
#define   USBDevIntPri_FRAME	(1<<0) /*0/1 FRAME int routed to the low/high priority interrupt line*/
#define   USBDevIntPri_EP_FAST	(1<<1) /*0/1 EP_FAST int routed to the low/high priority line*/

/* Endpoint interrupt registers - bits corresponds to  EP0 to EP31 */
#define USBEpIntSt_o	0x0030	/* USB Endpoint Interrupt Status (RO) */
#define USBEpIntEn_o	0x0034	/* USB Endpoint Interrupt Enable (R/W) */
#define USBEpIntClr_o	0x0038	/* USB Endpoint Interrupt Clear (WO) */
#define USBEpIntSet_o	0x003C	/* USB Endpoint Interrupt Set (WO) */
#define USBEpIntPri_o	0x0040	/* USB Endpoint Priority (WO) */
/* Endpoint realization registers */
#define USBReEp_o	0x0044	/* USB Realize Endpoint (R/W) */
#define USBEpInd_o	0x0048	/* USB Endpoint Index (WO) */
#define   USBEpInd_Ind		0x001F	/* Index for subsequent USBMaxPSize (WO) */
#define USBMaxPSize_o	0x004C	/* USB MaxPacketSize (R/W) */
#define   USBMaxPSize_Size	0x03FF	/* The maximum packet size value */
/* USB transfer registers */
#define USBRxData_o	0x0018	/* USB Receive Data (RO) */
#define USBRxPLen_o	0x0020	/* USB Receive Packet Length (RO) */
#define   USBRxPLen_PKT_LNGTH	(0x03FF) /*Remaining amount of bytes to be read from RAM*/
#define   USBRxPLen_DV		(1<<10) /*Data valid. 0 only for error ISO packet*/
#define   USBRxPLen_PKT_RDY	(1<<11) /*Packet length valid and packet is ready for reading*/
#define USBTxData_o	0x001C	/* USB Transmit Data (WO) */
#define USBTxPLen_o	0x0024	/* USB Transmit Packet Length (WO) */
#define   USBTxPLen_PKT_LNGTH	(0x03FF) /*Remaining amount of bytes to be written to the EP_RAM*/
#define USBCtrl_o	0x0028	/* USB Control (R/W) */
#define   USBCtrl_RD_EN		(1<<0) /*Read mode control*/
#define   USBCtrl_WR_EN		(1<<1) /*Write mode control*/
#define   USBCtrl_LOG_ENDPOINT	0x003C /*Logical Endpoint number*/
/* Command registers */
#define USBCmdCode_o	0x0010	/* USB Command Code (WO) */
#define   USBCmdCode_CMD_PHASE	0x0000FF00 /*The command phase*/
#define   USBCmdCode_CMD_CODE	0x00FF0000 /*The code for the command*/
#define USBCmdData_o	0x0014	/* USB Command Data (RO) */
/* DMA registers (LPC2146/8 only) */
#define USBDMARSt_o	0x0050	/* USB DMA Request Status (RO) */
#define USBDMARClr_o	0x0054	/* USB DMA Request Clear (WO) */
#define USBDMARSet_o	0x0058	/* USB DMA Request Set (WO) */
#define USBUDCAH_o	0x0080	/* USB UDCA Head (R/W) has to be aligned to 128 bytes */
#define USBEpDMASt_o	0x0084	/* USB Endpoint DMA Status (RO) */
#define USBEpDMAEn_o	0x0088	/* USB Endpoint DMA Enable (WO) */
#define USBEpDMADis_o	0x008C	/* USB Endpoint DMA Disable (WO) */
#define USBDMAIntSt_o	0x0090	/* USB DMA Interrupt Status (RO) */
#define USBDMAIntEn_o	0x0094	/* USB DMA Interrupt Enable (R/W) */
#define   USBDMAInt_EoT		(1<<0) /*End of Transfer Interrupt bit, 1 if USBEoTIntSt != 0*/
#define   USBDMAInt_New_DD_Rq	(1<<1) /*  New DD Request Interrupt bit, 1 if USBNDDRIntSt != 0*/
#define   USBDMAInt_SysError	(1<<2) /*System Error Interrupt bit, 1 if USBSysErrIntSt != 0*/
#define USBEoTIntSt_o	0x00A0	/* USB End of Transfer Interrupt Status (RO) */
#define USBEoTIntClr_o	0x00A4	/* USB End of Transfer Interrupt Clear (WO) */
#define USBEoTIntSet_o	0x00A8	/* USB End of Transfer Interrupt Set (WO) */
#define USBNDDRIntSt_o	0x00AC	/* USB New DD Request Interrupt Status (RO) */
#define USBNDDRIntClr_o	0x00B0	/* USB New DD Request Interrupt Clear (WO) */
#define USBNDDRIntSet_o	0x00B4	/* USB New DD Request Interrupt Set (WO) */
#define USBSysErrIntSt_o	0x00B8	/* USB System Error Interrupt Status (RO) */
#define USBSysErrIntClr_o	0x00BC	/* USB System Error Interrupt Clear (WO) */
#define USBSysErrIntSet_o	0x00C0	/* USB System Error Interrupt Set (WO) */
#define USB_MODULE_ID_o		0x00FC	/* USB Module ID */

/* Command Codes */
#define USB_CMD_SET_ADDR        0x00D00500
#define USB_CMD_CFG_DEV         0x00D80500
#define USB_CMD_SET_MODE        0x00F30500
#define USB_CMD_RD_FRAME        0x00F50500
#define USB_DAT_RD_FRAME        0x00F50200
#define USB_CMD_RD_TEST         0x00FD0500
#define USB_DAT_RD_TEST         0x00FD0200
#define USB_CMD_SET_DEV_STAT    0x00FE0500
#define USB_CMD_GET_DEV_STAT    0x00FE0500
#define USB_DAT_GET_DEV_STAT    0x00FE0200
#define USB_CMD_GET_ERR_CODE    0x00FF0500
#define USB_DAT_GET_ERR_CODE    0x00FF0200
#define USB_CMD_RD_ERR_STAT     0x00FB0500
#define USB_DAT_RD_ERR_STAT     0x00FB0200
#define USB_DAT_WR_BYTE(x)     (0x00000100 | ((x) << 16))
#define USB_CMD_SEL_EP(x)      (0x00000500 | ((x) << 16))
#define USB_DAT_SEL_EP(x)      (0x00000200 | ((x) << 16))
#define USB_CMD_SEL_EP_CLRI(x) (0x00400500 | ((x) << 16))
#define USB_DAT_SEL_EP_CLRI(x) (0x00400200 | ((x) << 16))
#define USB_CMD_SET_EP_STAT(x) (0x00400500 | ((x) << 16))
#define USB_CMD_CLR_BUF         0x00F20500
#define USB_DAT_CLR_BUF         0x00F20200
#define USB_CMD_VALID_BUF       0x00FA0500

/* Device Address Register Definitions */
#define USBC_DEV_ADDR_MASK       0x7F
#define USBC_DEV_EN              0x80

/* Device Configure Register Definitions */
#define USBC_CONF_DEVICE         0x01

/* Device Mode Register Definitions */
#define USBC_AP_CLK              0x01
#define USBC_INAK_CI             0x02
#define USBC_INAK_CO             0x04
#define USBC_INAK_II             0x08
#define USBC_INAK_IO             0x10
#define USBC_INAK_BI             0x20
#define USBC_INAK_BO             0x40

/* Device Status Register Definitions */
#define USBC_DEV_CON             0x01
#define USBC_DEV_CON_CH          0x02
#define USBC_DEV_SUS             0x04
#define USBC_DEV_SUS_CH          0x08
#define USBC_DEV_RST             0x10

/* Error Code Register Definitions */
#define USBC_ERR_EC_MASK         0x0F
#define USBC_ERR_EA              0x10

/* Error Status Register Definitions */
#define USBC_ERR_PID             0x01
#define USBC_ERR_UEPKT           0x02
#define USBC_ERR_DCRC            0x04
#define USBC_ERR_TIMOUT          0x08
#define USBC_ERR_EOP             0x10
#define USBC_ERR_B_OVRN          0x20
#define USBC_ERR_BTSTF           0x40
#define USBC_ERR_TGL             0x80

/* Endpoint Select Register Definitions */
#define USBC_EP_SEL_F            0x01
#define USBC_EP_SEL_ST           0x02
#define USBC_EP_SEL_STP          0x04
#define USBC_EP_SEL_PO           0x08
#define USBC_EP_SEL_EPN          0x10
#define USBC_EP_SEL_B_1_FULL     0x20
#define USBC_EP_SEL_B_2_FULL     0x40

/* Endpoint Status Register Definitions */
#define USBC_EP_STAT_ST          0x01
#define USBC_EP_STAT_DA          0x20
#define USBC_EP_STAT_RF_MO       0x40
#define USBC_EP_STAT_CND_ST      0x80

/* Clear Buffer Register Definitions */
#define USBC_CLR_BUF_PO          0x01

typedef struct
{
/* Device interrupt registers */
  REG32 DevIntSt;	/* USB Device Interrupt Status (RO) 0000 */
  REG32 DevIntEn;	/* USB Device Interrupt Enable (R/W) 0004 */
  REG32 DevIntClr;	/* USB Device Interrupt Clear (WO) 0008 */
  REG32 DevIntSet;	/* USB Device Interrupt Set (WO) 000C */
/* Command registers */
  REG32 CmdCode;	/* USB Command Code (WO) 0010 */
  REG32 CmdData;	/* USB Command Data (RO) 0014 */
/* USB transfer registers */
  REG32 RxData;		/* USB Receive Data (RO) 0018 */
  REG32 TxData;		/* USB Transmit Data (WO) 001C */
  REG32 RxPLen;		/* USB Receive Packet Length (RO) 0020 */
  REG32 TxPLen;		/* USB Transmit Packet Length (WO) 0024 */
  REG32 Ctrl;		/* USB Control (R/W) 0028 */
/* Device interrupt priority register */
  REG_8  USBDevIntPri;	/* USB Device Interrupt Priority (WO) 002C */
  REG_8  _pad0[3];
/* Endpoint interrupt registers */
  REG32 EpIntSt;	/* USB Endpoint Interrupt Status (RO) 0030 */
  REG32 EpIntEn;	/* USB Endpoint Interrupt Enable (R/W) 0034 */
  REG32 EpIntClr;	/* USB Endpoint Interrupt Clear (WO) 0038 */
  REG32 EpIntSet;	/* USB Endpoint Interrupt Set (WO) 003C */
  REG32 EpIntPri;	/* USB Endpoint Priority (WO) 0040 */
/* Endpoint realization registers */
  REG32 ReEp;		/* USB Realize Endpoint (R/W) 0044 */
  REG32 EpInd;		/* USB Endpoint Index (WO) 0048 */
  REG32 MaxPSize;	/* USB MaxPacketSize (R/W) 004C */
/* DMA registers (LPC2146/8 only) */
  REG32 DMARSt;		/* USB DMA Request Status (RO) 0050 */
  REG32 DMARClr;	/* USB DMA Request Clear (WO) 0054 */
  REG32 DMARSet;	/* USB DMA Request Set (WO) 0058 */
  REG32 _pad1[9];
  REG32 UDCAH;		/* USB UDCA Head (R/W) 0080 */
  REG32 EpDMASt;	/* USB Endpoint DMA Status (RO) 0084 */
  REG32 EpDMAEn;	/* USB Endpoint DMA Enable (WO) 0088 */
  REG32 EpDMADis;	/* USB Endpoint DMA Disable (WO) 008C */
  REG32 DMAIntSt;	/* USB DMA Interrupt Status (RO) 0090 */
  REG32 DMAIntEn;	/* USB DMA Interrupt Enable (R/W) 0094 */
  REG32 _pad2[2];
  REG32 EoTIntSt;	/* USB End of Transfer Interrupt Status (RO) 00A0 */
  REG32 EoTIntClr;	/* USB End of Transfer Interrupt Clear (WO) 00A4 */
  REG32 EoTIntSet;	/* USB End of Transfer Interrupt Set (WO) 00A8 */
  REG32 NDDRIntSt;	/* USB New DD Request Interrupt Status (RO) 00AC */
  REG32 NDDRIntClr;	/* USB New DD Request Interrupt Clear (WO) 00B0 */
  REG32 NDDRIntSet;	/* USB New DD Request Interrupt Set (WO) 00B4 */
  REG32 SysErrIntSt;	/* USB System Error Interrupt Status (RO) 00B8 */
  REG32 SysErrIntClr;	/* USB System Error Interrupt Clear (WO) 00BC */
  REG32 SysErrIntSet;	/* USB System Error Interrupt Set (WO) 00C0 */
  REG32 _pad3[0xE];
  REG32 MODULE_ID;	/* Module ID (RO) 00FC */
} usbRegs_t;

