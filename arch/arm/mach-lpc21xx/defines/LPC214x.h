/******************************************************************************
 *
 * $RCSfile: LPC214x.h,v $
 * $Revision: 1.4 $
 *
 * Header file for Philips LPC214x ARM Processors 
 * Copyright 2006 Pavel Pisa <pisa@cmp.felk.cvut.cz>
 *
 * No guarantees, warrantees, or promises, implied or otherwise.
 * May be used for hobby or commercial purposes provided copyright
 * notice remains intact or GPL license is applied.
 *
 *****************************************************************************/

#ifndef INC_LPC214x_H
#define INC_LPC214x_H

///////////////////////////////////////////////////////////////////////////////
// ISP_RAM2FLASH_BLOCK_SIZE for 210x CPU
#ifndef ISP_RAM2FLASH_BLOCK_SIZE
  #define ISP_RAM2FLASH_BLOCK_SIZE 	256
#endif /* ISP_RAM2FLASH_BLOCK_SIZE */

#include "LPC21xx.h"

#include "lpcUSB.h"
#include "lpcADC-214x.h"

// USB Phase Locked Loop Registers (PLL48)
#define PLLCON48        SCB->pll48.con    /* Control Register */
#define PLLCFG48        SCB->pll48.cfg    /* Configuration Register */
#define PLLSTAT48       SCB->pll48.stat   /* Status Register */
#define PLLFEED48       SCB->pll48.feed   /* Feed Register */

///////////////////////////////////////////////////////////////////////////////
// USB Device

#define USBIntSt (*(REG32*)0xE01FC1C0) /* USB Interrupt Status (R/W) */

#if 1

#define USB             ((usbRegs_t *)0xE0090000)

#define USBDevIntSt	USB->DevIntSt
#define USBDevIntEn	USB->DevIntEn
#define USBDevIntClr	USB->DevIntClr
#define USBDevIntSet	USB->DevIntSet
#define USBDevIntPri	USB->DevIntPri
#define USBEpIntSt	USB->EpIntSt
#define USBEpIntEn	USB->EpIntEn
#define USBEpIntClr	USB->EpIntClr
#define USBEpIntSet	USB->EpIntSet
#define USBEpIntPri	USB->EpIntPri
#define USBReEp		USB->ReEp
#define USBEpInd	USB->EpInd
#define USBMaxPSize	USB->MaxPSize
#define USBRxData	USB->RxData
#define USBRxPLen	USB->RxPLen
#define USBTxData	USB->TxData
#define USBTxPLen	USB->TxPLen
#define USBCtrl		USB->Ctrl
#define USBCmdCode	USB->CmdCode
#define USBCmdData	USB->CmdData
#define USBDMARSt	USB->DMARSt
#define USBDMARClr	USB->DMARClr
#define USBDMARSet	USB->DMARSet
#define USBUDCAH	USB->UDCAH
#define USBEpDMASt	USB->EpDMASt
#define USBEpDMAEn	USB->EpDMAEn
#define USBEpDMADis	USB->EpDMADis
#define USBDMAIntSt	USB->DMAIntSt
#define USBDMAIntEn	USB->DMAIntEn
#define USBEoTIntSt	USB->EoTIntSt
#define USBEoTIntClr	USB->EoTIntClr
#define USBEoTIntSet	USB->EoTIntSet
#define USBNDDRIntSt	USB->NDDRIntSt
#define USBNDDRIntClr	USB->NDDRIntClr
#define USBNDDRIntSet	USB->NDDRIntSet
#define USBSysErrIntSt	USB->SysErrIntSt
#define USBSysErrIntClr	USB->SysErrIntClr
#define USBSysErrIntSet	USB->SysErrIntSet
#define USB_MODULE_ID	USB->MODULE_ID
# else

#define USB_REGS_BASE             0xE0090000

#define USBDevIntSt	(*(REG32*)(USB_REGS_BASE+USBDevIntSt_o))
#define USBDevIntEn	(*(REG32*)(USB_REGS_BASE+USBDevIntEn_o))
#define USBDevIntClr	(*(REG32*)(USB_REGS_BASE+USBDevIntClr_o))
#define USBDevIntSet	(*(REG32*)(USB_REGS_BASE+USBDevIntSet_o))
#define USBDevIntPri	(*(REG_8*)(USB_REGS_BASE+USBDevIntPri_o))
#define USBEpIntSt	(*(REG32*)(USB_REGS_BASE+USBEpIntSt_o))
#define USBEpIntEn	(*(REG32*)(USB_REGS_BASE+USBEpIntEn_o))
#define USBEpIntClr	(*(REG32*)(USB_REGS_BASE+USBEpIntClr_o))
#define USBEpIntSet	(*(REG32*)(USB_REGS_BASE+USBEpIntSet_o))
#define USBEpIntPri	(*(REG32*)(USB_REGS_BASE+USBEpIntPri_o))
#define USBReEp		(*(REG32*)(USB_REGS_BASE+USBReEp_o))
#define USBEpInd	(*(REG32*)(USB_REGS_BASE+USBEpInd_o))
#define USBMaxPSize	(*(REG32*)(USB_REGS_BASE+USBMaxPSize_o))
#define USBRxData	(*(REG32*)(USB_REGS_BASE+USBRxData_o))
#define USBRxPLen	(*(REG32*)(USB_REGS_BASE+USBRxPLen_o))
#define USBTxData	(*(REG32*)(USB_REGS_BASE+USBTxData_o))
#define USBTxPLen	(*(REG32*)(USB_REGS_BASE+USBTxPLen_o))
#define USBCtrl		(*(REG32*)(USB_REGS_BASE+USBCtrl_o))
#define USBCmdCode	(*(REG32*)(USB_REGS_BASE+USBCmdCode_o))
#define USBCmdData	(*(REG32*)(USB_REGS_BASE+USBCmdData_o))
#define USBDMARSt	(*(REG32*)(USB_REGS_BASE+USBDMARSt_o))
#define USBDMARClr	(*(REG32*)(USB_REGS_BASE+USBDMARClr_o))
#define USBDMARSet	(*(REG32*)(USB_REGS_BASE+USBDMARSet_o))
#define USBUDCAH	(*(REG32*)(USB_REGS_BASE+USBUDCAH_o))
#define USBEpDMASt	(*(REG32*)(USB_REGS_BASE+USBEpDMASt_o))
#define USBEpDMAEn	(*(REG32*)(USB_REGS_BASE+USBEpDMAEn_o))
#define USBEpDMADis	(*(REG32*)(USB_REGS_BASE+USBEpDMADis_o))
#define USBDMAIntSt	(*(REG32*)(USB_REGS_BASE+USBDMAIntSt_o))
#define USBDMAIntEn	(*(REG32*)(USB_REGS_BASE+USBDMAIntEn_o))
#define USBEoTIntSt	(*(REG32*)(USB_REGS_BASE+USBEoTIntSt_o))
#define USBEoTIntClr	(*(REG32*)(USB_REGS_BASE+USBEoTIntClr_o))
#define USBEoTIntSet	(*(REG32*)(USB_REGS_BASE+USBEoTIntSet_o))
#define USBNDDRIntSt	(*(REG32*)(USB_REGS_BASE+USBNDDRIntSt_o))
#define USBNDDRIntClr	(*(REG32*)(USB_REGS_BASE+USBNDDRIntClr_o))
#define USBNDDRIntSet	(*(REG32*)(USB_REGS_BASE+USBNDDRIntSet_o))
#define USBSysErrIntSt	(*(REG32*)(USB_REGS_BASE+USBSysErrIntSt_o))
#define USBSysErrIntClr	(*(REG32*)(USB_REGS_BASE+USBSysErrIntClr_o))
#define USBSysErrIntSet	(*(REG32*)(USB_REGS_BASE+USBSysErrIntSet_o))
#define USB_MODULE_ID	(*(REG32*)(USB_REGS_BASE+USB_MODULE_ID_o))

#endif


///////////////////////////////////////////////////////////////////////////////
// A/D Converter
#define ADC0             ((adc214xRegs_t *)0xE0034000)

#define AD0CR            ADC0->cr       // Control Register 
#define AD0GDR           ADC0->gdr	// Global Data Register
#define AD0GSR           ADC0->gsr	// Global Start Register
#define AD0INTEN         ADC0->inten	// Interrupt Enable Register
#define AD0DR0           ADC0->dr0	// Channel 0 Data Register
#define AD0DR1           ADC0->dr1	// Channel 1 Data Register
#define AD0DR2           ADC0->dr2	// Channel 2 Data Register
#define AD0DR3           ADC0->dr3	// Channel 3 Data Register
#define AD0DR4           ADC0->dr4	// Channel 4 Data Register
#define AD0DR5           ADC0->dr5	// Channel 5 Data Register
#define AD0DR6           ADC0->dr6	// Channel 6 Data Register
#define AD0DR7           ADC0->dr7	// Channel 7 Data Register
#define AD0STAT          ADC0->stat	// Status Register

#define ADC1             ((adc214xRegs_t *)0xE0060000)

#define AD1CR            ADC0->cr       // Control Register 
#define AD1GDR           ADC0->gdr	// Global Data Register
#define AD1GSR           ADC0->gsr	// Global Start Register
#define AD1INTEN         ADC0->inten	// Interrupt Enable Register
#define AD1DR0           ADC0->dr0	// Channel 0 Data Register
#define AD1DR1           ADC0->dr1	// Channel 1 Data Register
#define AD1DR2           ADC0->dr2	// Channel 2 Data Register
#define AD1DR3           ADC0->dr3	// Channel 3 Data Register
#define AD1DR4           ADC0->dr4	// Channel 4 Data Register
#define AD1DR5           ADC0->dr5	// Channel 5 Data Register
#define AD1DR6           ADC0->dr6	// Channel 6 Data Register
#define AD1DR7           ADC0->dr7	// Channel 7 Data Register
#define AD1STAT          ADC0->stat	// Status Register


#endif /*INC_LPC21xx_H*/
