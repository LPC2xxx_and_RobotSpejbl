/**************************** dev_cntrl.h *******************************/
/* Copyright 2003/12/28 Aeolus Development				*/
/* All rights reserved.							*/
/*									*/
/* Redistribution and use in source and binary forms, with or without	*/
/* modification, are permitted provided that the following conditions	*/
/* are met:								*/
/* 1. Redistributions of source code must retain the above copyright	*/
/*   notice, this list of conditions and the following disclaimer.	*/
/* 2. Redistributions in binary form must reproduce the above copyright	*/
/*   notice, this list of conditions and the following disclaimer in the*/
/*   documentation and/or other materials provided with the 		*/
/*   distribution.							*/
/* 3. The name of the Aeolus Development or its contributors may not be	*/
/* used to endorse or promote products derived from this software 	*/
/* without specific prior written permission.				*/
/*									*/
/* THIS SOFTWARE IS PROVIDED BY THE AEOULUS DEVELOPMENT "AS IS" AND ANY	*/
/* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE	*/
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR	*/
/* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AEOLUS DEVELOPMENT BE	*/
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR	*/
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF	*/
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	*/
/* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,*/
/* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE */
/* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 	*/
/* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.			*/
/*									*/
/*  Provides definitions and prototypes for device drivers to run with	*/
/* newlib.								*/
/************************************************************************/
/*
*   TLIB revision history:
*   1 dev_cntrl.h 30-Dec-2003,10:34:12,`RADSETT' First archival version.
*   TLIB revision history ends.
*/

#ifndef DEV_CNTRL__H
#define DEV_CNTRL__H

#include <reent.h>

	/*  Structure used to define a device driver.			*/
struct device_table_entry {
    const char *name;		/*  Device name.			*/

	/*  Device open method.						*/
    int (*open)(struct _reent *r,const char *name, int flags, int mode);

	/*  Device close method.					*/
    int (*close)(struct _reent *r,int file);

	/*  Device read method.						*/
    _ssize_t (*read)(struct _reent *r,int file, void *ptr, size_t len);

	/*  Device write method.					*/
    _ssize_t (*write)(struct _reent *r,int file, const void *ptr, size_t len);

	/*  Device ioctl method (controls device operating parameters.	*/
    int (*ioctl)(struct _reent *r, int file, unsigned long request, void *ptr);
    };

	/*  List of devices.  Newlib adapatation uses this to determine	*/
	/* what devices are available and how to control them.  List	*/
	/* is terminated by a null pointer entry.			*/
	/*  Note:  Entries 0, 1, 2 are reserved to address stdin, 	*/
	/* stdout and stderr.  These must be able to work from a 	*/
	/* standard open w/o needing further setup if they are to be	*/
	/* transparent, alternatively they must be setup before any I/O	*/
	/* is done.							*/
extern const struct device_table_entry *device_table[];

	/*  Macros to split file number into two pieces.  The low order	*/
	/* 8 bits are used to index the specific device being addressed	*/
	/* (hopefully 256 devices is an unreasonbly large limit).  The 	*/
	/* remaining bits are used in a device specific fashion to 	*/
	/* allow each device driver to address multiple devices or 	*/
	/* files.							*/
#define DEVICE(x)	((x) & 0xFF)
	/*lint -emacro(702,SUB_FILE) right shift of signed qty	*/
#define SUB_FILE(x)	((x) >> 8) 

	/**** Existing device drivers that may be used.	****/
extern const struct device_table_entry com1;	/*  UART0.		*/
extern const struct device_table_entry sys;	/*  SYS -- Essentially 	*/
						/* a dummy.		*/
#endif /* DEV_CNTRL__H */
