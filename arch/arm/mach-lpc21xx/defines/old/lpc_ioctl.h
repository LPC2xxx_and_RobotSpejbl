/**************************** lpc_ioctl.h *******************************/
/* Copyright 2003/12/29 Aeolus Development				*/
/* All rights reserved.							*/
/*									*/
/* Redistribution and use in source and binary forms, with or without	*/
/* modification, are permitted provided that the following conditions	*/
/* are met:								*/
/* 1. Redistributions of source code must retain the above copyright	*/
/*   notice, this list of conditions and the following disclaimer.	*/
/* 2. Redistributions in binary form must reproduce the above copyright	*/
/*   notice, this list of conditions and the following disclaimer in the*/
/*   documentation and/or other materials provided with the 		*/
/*   distribution.							*/
/* 3. The name of the Aeolus Development or its contributors may not be	*/
/* used to endorse or promote products derived from this software 	*/
/* without specific prior written permission.				*/
/*									*/
/* THIS SOFTWARE IS PROVIDED BY THE AEOULUS DEVELOPMENT "AS IS" AND ANY	*/
/* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE	*/
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR	*/
/* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AEOLUS DEVELOPMENT BE	*/
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR	*/
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF	*/
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	*/
/* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,*/
/* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE */
/* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 	*/
/* EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.			*/
/*									*/
/*  I/O control.  Routines, enums and structures to support device	*/
/* control.								*/
/************************************************************************/
/*
*   TLIB revision history:
*   1 lpc_ioctl.h 30-Dec-2003,10:34:14,`RADSETT' First archival version.
*   TLIB revision history ends.
*/
#ifndef LPC_IOCTL__H
#define LPC_IOCTL__H

	/**** Request defintions. These define the requested actions.****/
#define UART_SETUP		(0x2100uL)


	/**** UART Defintions. ****/

	
#define UART_STOP_BITS_2	(4u)	/*  Sets to provide 2 stop bits */
#define UART_STOP_BITS_1	(0u)	/*  Sets to provide 1 stop bit	*/

	
#define UART_PARITY_NONE	(0u)	/*  Set to parity None		*/
#define UART_PARITY_ODD		(8u)	/*  Set to parity Odd		*/
#define UART_PARITY_EVEN	(0x18u)	/*  Set to parity Even		*/
#define UART_PARITY_STICK1	(0x28u)	/*  Set to parity stuck on	*/
#define UART_PARITY_STICK0	(0x38u)	/*  Set to parity stuck off	*/

#define UART_WORD_LEN_5		(0u)	/*  5 bit serial byte.		*/
#define UART_WORD_LEN_6		(1u)	/*  6 bit serial byte.		*/
#define UART_WORD_LEN_7		(2u)	/*  7 bit serial byte.		*/
#define UART_WORD_LEN_8		(3u)	/*  8 bit serial byte.		*/

	/*  serial_param -- structure to pass with ioctl request 	*/
	/* UART_SETUP to control the serial line characteristics.	*/
struct serial_param {
    unsigned long baud;
    unsigned int length;
    unsigned int parity;
    unsigned int stop;
    };



/************************** _ioctl_r ************************************/
/*  Support function.  Device specific control.				*/
/*  struct _reent *r	-- re-entrancy structure, used by newlib to 	*/
/*			support multiple threads of operation.		*/
/*  int fd		-- number referring to the open file. Generally	*/
/*			obtained from a corresponding call to open.	*/
/*  unsigned long request -- Number to indicate the request being made 	*/
/*			of the driver.					*/
/*  void *ptr		-- pointer to data that is either used by or 	*/
/*			set by the driver.  Varies by request.		*/
/*  Returns 0 if successful, otherwise errno will be set to indicate 	*/
/* the source of the error.						*/
int _ioctl_r( struct _reent *r, int fd, unsigned long request, void *ptr);

/************************** ioctl ***************************************/
/*  Support function.  Device specific control.  A shell to convert	*/
/* requests into a re-entrant form.					*/
/*  int fd		-- number referring to the open file. Generally	*/
/*			obtained from a corresponding call to open.	*/
/*  unsigned long request -- Number to indicate the request being made 	*/
/*			of the driver.					*/
/*  void *ptr		-- pointer to data that is either used by or 	*/
/*			set by the driver.  Varies by request.		*/
/*  Returns 0 if successful, otherwise errno will be set to indicate 	*/
/* the source of the error.						*/
int ioctl( int fd, unsigned long request, void *ptr);

#endif /* LPC_IOCTL__H */

