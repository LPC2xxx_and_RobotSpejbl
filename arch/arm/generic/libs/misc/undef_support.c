#include <cpu_def.h>
#include <irq_def.h>

static int undef_initialized = 0;

static unsigned long cpu_undef_stack[256];

struct undef_hook *undef_hook_chain = NULL;

static void undef_exception_handler(int excptnum, struct pt_regs *regs)
{
  /*unsigned int correction = thumb_mode(regs) ? 2 : 4;*/
  unsigned int correction = 0;
  struct undef_hook *hook;
  void *pc;
  unsigned long instr;

  regs->ARM_pc -= correction;

  pc = (void *)regs->ARM_pc;

  instr = *(unsigned long *)pc;

  for(hook = undef_hook_chain; hook; hook = hook->next) {
    if (((instr & hook->instr_mask) == hook->instr_val) &&
      ((regs->ARM_cpsr & hook->cpsr_mask) == hook->cpsr_val)) {
      if (hook->fn(regs, instr) == 0) {
        return;
      }
    }
  }

  for(;;){
    /* Fatal error */
  }
}

int register_undef_hook(struct undef_hook *hook)
{
  unsigned long flags;

  save_and_cli(flags);

  if(!undef_initialized) {
    set_cpu_exception_handler(ARM_EXCEPTION_UNDEF, (long)undef_exception_handler);
    set_cpu_exception_stack(ARM_EXCEPTION_UNDEF, (long)((char*)cpu_undef_stack+sizeof(cpu_undef_stack)-8));
    undef_initialized = 1;
  }

  hook->next = undef_hook_chain;
  undef_hook_chain = hook;
  restore_flags(flags);
  
  return 0;
}
