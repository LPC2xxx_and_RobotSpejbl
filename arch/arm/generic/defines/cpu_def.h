#ifndef _ARM_CPU_DEF_H
#define _ARM_CPU_DEF_H

struct pt_regs {
        long uregs[18];
};

#define ARM_cpsr        uregs[16]
#define ARM_pc          uregs[15]
#define ARM_lr          uregs[14]
#define ARM_sp          uregs[13]
#define ARM_ip          uregs[12]
#define ARM_fp          uregs[11]
#define ARM_r10         uregs[10]
#define ARM_r9          uregs[9]
#define ARM_r8          uregs[8]
#define ARM_r7          uregs[7]
#define ARM_r6          uregs[6]
#define ARM_r5          uregs[5]
#define ARM_r4          uregs[4]
#define ARM_r3          uregs[3]
#define ARM_r2          uregs[2]
#define ARM_r1          uregs[1]
#define ARM_r0          uregs[0]
#define ARM_ORIG_r0     uregs[17]

struct undef_hook {
        struct undef_hook *next;
        unsigned long instr_mask;
        unsigned long instr_val;
        unsigned long cpsr_mask;
        unsigned long cpsr_val;
        int (*fn)(struct pt_regs *regs, unsigned int instr);
};

int register_undef_hook(struct undef_hook *hook);

#define NR_IRQS 256

typedef struct irq_handler {
  void            (*handler)(int, void *, struct pt_regs *);
  unsigned long   flags;
  void            *dev_id;
  const char      *devname;
  struct irq_handler *next;
  short		  vectno;
} irq_handler_t;

#define	IRQH_ON_LIST	0x100	/* handler is used */

extern irq_handler_t *irq_array[NR_IRQS];
extern void          *irq_vec[NR_IRQS];

int add_irq_handler(int vectno,irq_handler_t *handler);

int del_irq_handler(int vectno,irq_handler_t *handler);

int test_irq_handler(int vectno,const irq_handler_t *handler);

void irq_redirect2vector(int vectno,struct pt_regs *regs);

/* IRQ handling code */

#define sti()                                                   \
        ({                                                      \
                unsigned long temp;                             \
        __asm__ __volatile__(                                   \
        "mrs    %0, cpsr                @ sti\n"                \
"       bic     %0, %0, #128\n"                                 \
"       msr     cpsr_c, %0"                                     \
        : "=r" (temp)                                           \
        :                                                       \
        : "memory", "cc");                                      \
        })

#define cli()                                                   \
        ({                                                      \
                unsigned long temp;                             \
        __asm__ __volatile__(                                   \
        "mrs    %0, cpsr                @ cli\n"                \
"       orr     %0, %0, #128\n"                                 \
"       msr     cpsr_c, %0"                                     \
        : "=r" (temp)                                           \
        :                                                       \
        : "memory", "cc");                                      \
        })

#define save_and_cli(flags)                                     \
        ({                                                      \
                unsigned long temp;                             \
                (void) (&temp == &flags);                       \
        __asm__ __volatile__(                                   \
        "mrs    %0, cpsr                @ save_and_cli\n"       \
"       orr     %1, %0, #128\n"                                 \
"       msr     cpsr_c, %1"                                     \
        : "=r" (flags), "=r" (temp)                             \
        :                                                       \
        : "memory", "cc");                                      \
        })

#define save_flags(flags)                                       \
        ({                                                      \
        __asm__ __volatile__(                                   \
        "mrs    %0, cpsr                @ save_flags\n"         \
        : "=r" (flags)                                          \
        :                                                       \
        : "memory", "cc");                                      \
        })

#define restore_flags(flags)                                    \
        __asm__ __volatile__(                                   \
        "msr    cpsr_c, %0              @ restore_flags\n"      \
        :                                                       \
        : "r" (flags)                                           \
        : "memory", "cc")


/* FIQ handling code */

#define fiq_sti()                                                   \
        ({                                                      \
                unsigned long temp;                             \
        __asm__ __volatile__(                                   \
        "mrs    %0, cpsr                @ sti\n"                \
"       bic     %0, %0, #64\n"                                  \
"       msr     cpsr_c, %0"                                     \
        : "=r" (temp)                                           \
        :                                                       \
        : "memory", "cc");                                      \
        })

#define fiq_cli()                                                   \
        ({                                                      \
                unsigned long temp;                             \
        __asm__ __volatile__(                                   \
        "mrs    %0, cpsr                @ cli\n"                \
"       orr     %0, %0, #64\n"                                  \
"       msr     cpsr_c, %0"                                     \
        : "=r" (temp)                                           \
        :                                                       \
        : "memory", "cc");                                      \
        })

#define fiq_save_and_cli(flags)                                     \
        ({                                                      \
                unsigned long temp;                             \
                (void) (&temp == &flags);                       \
        __asm__ __volatile__(                                   \
        "mrs    %0, cpsr                @ save_and_cli\n"       \
"       orr     %1, %0, #192\n"                                 \
"       msr     cpsr_c, %1"                                     \
        : "=r" (flags), "=r" (temp)                             \
        :                                                       \
        : "memory", "cc");                                      \
        })

void __cpu_coherent_range(unsigned long start, unsigned long end);

static inline void flush_icache_range(unsigned long start, unsigned long end)
{
	__cpu_coherent_range(start, end);
}

/* atomic access routines */

//typedef unsigned long atomic_t;

static inline void atomic_clear_mask(unsigned long mask, volatile unsigned long *addr)
{
        unsigned long flags;

        save_and_cli(flags);
        *addr &= ~mask;
        restore_flags(flags);
}

static inline void atomic_set_mask(unsigned long mask, volatile unsigned long *addr)
{
        unsigned long flags;

        save_and_cli(flags);
        *addr |= mask;
        restore_flags(flags);
}

static inline void set_bit(int nr, volatile unsigned long *addr)
{
        unsigned long flags;

        save_and_cli(flags);
        *addr |= 1<<nr;
        restore_flags(flags);
}

static inline void clear_bit(int nr, volatile unsigned long *addr)
{
        unsigned long flags;

        save_and_cli(flags);
        *addr &= ~(1<<nr);
        restore_flags(flags);
}

static inline int test_bit(int nr, volatile unsigned long *addr)
{
        return ((*addr) & (1<<nr))?1:0;
}

static inline int test_and_set_bit(int nr, volatile unsigned long *addr)
{
        unsigned long flags;
	long m=(1<<nr);
        long r;

        save_and_cli(flags);
        r=*addr;
	*addr=r|m;
        restore_flags(flags);
        return r&m?1:0;
}

#define __memory_barrier() \
 __asm__ __volatile__("": : : "memory")

/*masked fields macros*/

#define __val2mfld(mask,val) (((mask)&~((mask)<<1))*(val)&(mask))
#define __mfld2val(mask,val) (((val)&(mask))/((mask)&~((mask)<<1)))

static inline void outb(int port, int val) {
  *(volatile unsigned char *)(port)=val;
}

static inline unsigned char inb(int port) {
  return *(volatile unsigned char *)(port);
}

#endif /* _ARM_CPU_DEF_H */












