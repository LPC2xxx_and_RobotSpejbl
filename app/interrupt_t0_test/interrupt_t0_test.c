////////////////////////////////////////////////////////////////////////////////
//
//                 Philips LPC210X interrupt use example
//
// Description
// -----------
// This example demonstrates
//version 1.0 15/08/2005
//Author : Guillaume LAGARRIGUE
////////////////////////////////////////////////////////////////////////////////

#include <LPC210x.h>
#include <types.h>
#include <cpu_def.h>
#include "config.h"


#define NUM_LEDS 4
static int leds[] = { 0x10000, 0x20000, 0x40000, 0x80000 };



/*////////////////////////////////////////////////////////INITIALISATION FUNCTIONS///////////////////////////////////*/


/**
*  Function Name: lowInit()
*
* Description:
*    This function starts up the PLL then sets up the GPIO pins before
*    waiting for the PLL to lock.  It finally engages the PLL and
*    returns
*
* Calling Sequence: 
*    void
*
* Returns:
*    void
*  
*/
static void lowInit(void)
{
    // set PLL multiplier & divisor.
    // values computed from config.h
    PLLCFG = PLLCFG_MSEL | PLLCFG_PSEL;

    // enable PLL
    PLLCON = PLLCON_PLLE;
    PLLFEED = 0xAA;                       // Make it happen.  These two updates
    PLLFEED = 0x55;                       // MUST occur in sequence.

    // setup the parallel port pin
    IOCLR = PIO_ZERO_BITS;                // clear the ZEROs output
    IOSET = PIO_ONE_BITS;                 // set the ONEs output
    IODIR = PIO_OUTPUT_BITS;              // set the output bit direction

    // wait for PLL lock
    while (!(PLLSTAT & PLLSTAT_LOCK))
        continue;

    // enable & connect PLL
    PLLCON = PLLCON_PLLE | PLLCON_PLLC;
    PLLFEED = 0xAA;                       // Make it happen.  These two updates
    PLLFEED = 0x55;                       // MUST occur in sequence.

    // setup & enable the MAM
    MAMTIM = MAMTIM_CYCLES;
    MAMCR = MAMCR_FULL;

    // set the peripheral bus speed
    // value computed from config.h
    VPBDIV = VPBDIV_VALUE;                // set the peripheral bus clock speed
}


/**
*  Function Name: sysInit()
*
* Description:
*    This function is responsible for initializing the program
*    specific hardware
*
* Calling Sequence: 
*    void
*
* Returns:
*    void
*  
*/
static void sysInit(void)
{
    lowInit();                            // setup clocks and processor port pins

    MEMMAP = 1;              // map interrupt vectors space into FLASH

  //MEMMAP = 1;              // map interrupt vectors space into FLASH
  MEMMAP = 0x2; /** user RAM mode **/

}

/*////////////////////////////LEDS INITIALISATION///////////////////////*/

/**
* Initializes leds.
*/
static void ledInit()
{
    IODIR |= 0x000F0000; /*leds connected to P0.16 17 18 & 19 should blink*/
    IOSET = 0x00000000;   /* all leds are switched off */
}

/**
* Switches the leds off.
* @param led  switched off led number. (integer)
*/
static void ledOff(int led)  /* Ioclr.i =1   =>   IOset.i cleared */
{
    IOCLR = led;
}

/**
* Switches the leds on. 
* @param led  switched on led number. (integer)
*/
static void  ledOn(int led)  /*  Ioset.i = 1   =>  P0.i = 1    */
{
    IOSET = led;
}


/**
* Creates a delay
* @param d duration (unit not defined yet)
*/
void
delay(int d)
{
    volatile int x;
    int i;
    for (i = 0; i < 10; i++)
        for(x = d; x; --x)
            ;
}



/**
 * Switches on and off the 4 leds one after one:
 * 
 * A led is switched on when an interrupt occurs
 * then switched off when the next interrupt occurs
 * @param  void
 */
void timer0_isr(void) __attribute__ ((interrupt));

int i=0 ; 
int led_num=0 ;

void timer0_isr(void)
{   
     
    if (i ==0)
        {
            ledOn(leds[led_num]);
            i=1;
        }  
    
        
        else
        {
            ledOff(leds[led_num]);
            i =0;
            led_num++;
        }
        
        
    if (led_num == NUM_LEDS)
        {
            led_num=0;
        }

    T0IR       |= 0x00000001;               // Clear match0 interrupt
    VICVectAddr  = 0x00000000;
}




/**
 * initializes timer T0 to raise an interrupt and to reset T0counter when it matches T0MatchRegister0
 * @param  
 */
static void Init_timer(void)
{
    VICVectAddr0 = (unsigned)timer0_isr;        /* Set the ISR vector address  vector0 = highest priority */
    VICVectCntl0 = 0x20|4;                      /* Enable this vector and assign Timer IRQ to it */
    VICIntEnable = 0x00000010;                  /* Enable the interrupt of timer0*/

    
    T0MR0 = 0x0010000; //Match 0 control register = 65536
    T0MCR = 0x3; //resets timer0 and generates interrupt when timer0 counter match MR0 register

    T0TCR = 0x3; //Reset timer 0

}


int main(void)
{
    sysInit();
    ledInit();
    Init_timer();

    T0TCR = 0x01; //Run timer 0

    while(1)  //infinite loop
    {
    }

    return 0;
}
