    /******************************************************************************/
    /*  This file is a test of pwm unit of ARM board.
        It's the first test which works
        The loader works correctly too                                            */
    /******************************************************************************/
    /*                                                                            */
    /*  test_ARMBoard_pwm.c : DC motor control with PWM                           */
    /*                                                                            */
    /******************************************************************************/
                    
    
    
#include <types.h>
#include <LPC210x.h>
#include "config.h"
#include "pwm.h"
    
    

    /**
    *     * Function Name: lowInit()
    *
    * Description:
    *    This function starts up the PLL then sets up the GPIO pins before
    *    waiting for the PLL to lock.  It finally engages the PLL and
    *    returns
    *
    * Calling Sequence: 
    *    void
    *
    * Returns:
    *    void
    *
    * 
    */
    static void lowInit(void)
{
    // set PLL multiplier & divisor.
    // values computed from config.h
    PLLCFG = PLLCFG_MSEL | PLLCFG_PSEL;
    
    // enable PLL
    PLLCON = PLLCON_PLLE;
    PLLFEED = 0xAA;                       // Make it happen.  These two updates
    PLLFEED = 0x55;                       // MUST occur in sequence.
    
    // setup the parallel port pin
    IOCLR = PIO_ZERO_BITS;                // clear the ZEROs output
    IOSET = PIO_ONE_BITS;                 // set the ONEs output
    IODIR = PIO_OUTPUT_BITS;              // set the output bit direction
    
    // wait for PLL lock
    while (!(PLLSTAT & PLLSTAT_LOCK))
        continue;
    
    // enable & connect PLL
    PLLCON = PLLCON_PLLE | PLLCON_PLLC;
    PLLFEED = 0xAA;                       // Make it happen.  These two updates
    PLLFEED = 0x55;                       // MUST occur in sequence.
    
    // setup & enable the MAM
    MAMTIM = MAMTIM_CYCLES;
    MAMCR = MAMCR_FULL;
    
    // set the peripheral bus speed
    // value computed from config.h
    VPBDIV = VPBDIV_VALUE;                // set the peripheral bus clock speed
}
    
    
    /**
    * Function Name: sysInit()
    *
    * Description:
    *    This function is responsible for initializing the program
    *    specific hardware
    *
    * Calling Sequence: 
    *    void
    *
    * Returns:
    *    void
    * 
    */
    

    static void sysInit(void)
{
    lowInit();                            // setup clocks and processor port pins
    
    // set the interrupt controller defaults
#define RAM_RUN
#if defined(RAM_RUN)
    MEMMAP = MEMMAP_SRAM;                 // map interrupt vectors space into SRAM
#elif defined(ROM_RUN)
    MEMMAP = MEMMAP_FLASH;                // map interrupt vectors space into FLASH
#else
#error RUN_MODE not defined!
#endif
    VICIntEnClear = 0xFFFFFFFF;           // clear all interrupts
    VICIntSelect = 0x00000000;            // clear all FIQ selections
    VICDefVectAddr = (uint32_t)reset;     // point unvectored IRQs to reset()
    
 
}
    
  /**
 * Creates a delay
 * @param d duration (unit not defined yet)
   */
void
        delay(int d)
{
    volatile int x;
    int i;
    for (i = 0; i < 10; i++)
        for(x = d; x; --x)
            ;
}
  
    

    /**
     * this function has been created to test PWM unit of the ARM Board
     * It has been tested with the DC motor of M.Sojka ;-)
     * One decelerated phase with upward rotation then one accelerated speed phase with backward rotation
     * DC motor pin 2  = PWM2
     * DC motor pin 3  = PWM4
     * DC motor pin 25  = GND
     * @return 
     */
    int main() {
  
        int j;
        
        
        sysInit();
               
        Init_PWM(20000);
        Run_PWM();
        Set_PWM(2,0);
        Set_PWM(4,0);
        for (j = 0 ; j < 10 ; j++)      
            {
                Set_PWM (4, (9-j)*0.1);
                delay (500000);
            }
        for (j = 0 ; j < 10 ; j++)      
            {
                Set_PWM (2, j*0.1);
                delay (500000);
            }
       
      
        Stop_PWM();
       
       while (1) 
       {                               /* Loop forever */
           
       }
        return 0;
    }
