//////////////////////////////////////////////////
/*	test lpcanvca smer pc => lpc		*/
/*	program pro spejblarm desku		*/
/*						*/
/* Autor: Martin Rakovec			*/
/* E-mail: martin.rakovec@tiscali.cz		*/
//////////////////////////////////////////////////

/* LPC21xx definitions */
#include <lpc21xx.h>
/* CAN definitions*/
#include <can/canmsg.h>
#include <can/lpcan.h>
#include <can/lpcan_vca.h>
#include "led.h"

#define CFG_MSG_ID 0x4CC
#define LPC_ID 0x4AB
#define PC_ID 0x4BA
#define START_MSG_DATA 0xAB
#define STOP_MSG_DATA 0xBA
#define ACK_MSG_DATA 0xAA

/**
 * Main function 
 */
int main (void)  {
	VICIntEnClr = 0xFFFFFFFF;
	VICIntSelect = 0x00000000;	
	
	VPBDIV = 1;
	
	led_init();
	led_set(LED_GRN,1);

	/* CAN bus setup */
	uint32_t btr;
	vca_handle_t can;
	
	canmsg_t sendmsg = {.flags = 0, .length = 1};
	canmsg_t readmsg;
	
	lpcan_btr(&btr, 1000000 /*Bd*/, 10000000, 0/*SJW*/, 70/*%sampl.pt.*/, 0/*SAM*/);
	lpc_vca_open_handle(&can, 0/*device*/, 0/*flags*/, btr, 10, 11, 12);	

	uint8_t count = 0;
	while(1){
		/* if nothig is recieved, continue*/
		if(vca_rec_msg_seq(can,&readmsg,1)<0) continue;
		switch(readmsg.id){
		    case CFG_MSG_ID:
			switch(readmsg.data[0]){
			case START_MSG_DATA:
			    //set sendmsg.id and data(counter) at 0
			    sendmsg.id = LPC_ID+1;
			    count = 0;
			    led_set(LED_RED,0);
			    led_set(LED_YEL,0);
			    break;
			case STOP_MSG_DATA:
			    //send msg(counter)
			    sendmsg.data[0] = count;
			    vca_send_msg_seq(can,&sendmsg,1);
			    led_set(LED_RED,1);
			    break;
			case ACK_MSG_DATA:
			    //set sendmsg.id and data
			    sendmsg.id = LPC_ID;
			    sendmsg.data[0] = 0;
			    //send 1000 messages
			    int s = 1;
			    int i = 0;
			    while(i < 1000){
				vca_send_msg_seq(can,&sendmsg,1);
				if(sendmsg.data[0] == 0xFF) s = -1;
				if(sendmsg.data[0] == 0x00) s = 1;
				sendmsg.data[0] += s;
				i++;
			    }
			    //send stop message
			    sendmsg.id = CFG_MSG_ID;
			    sendmsg.data[0] = STOP_MSG_DATA;
			    
			    vca_send_msg_seq(can,&sendmsg,1);
			    vca_send_msg_seq(can,&sendmsg,1);
			    vca_send_msg_seq(can,&sendmsg,1);
			    vca_send_msg_seq(can,&sendmsg,1);
			    vca_send_msg_seq(can,&sendmsg,1);
			    
			    led_set(LED_YEL,1);
			    break;
			default: 
			    break;
			}
			break;
		    case PC_ID:
			//increment counter
			count++;
			break;
		    default:
			break;
		}	
	}
}



