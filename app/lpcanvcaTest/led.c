#include "led.h"

struct {
  volatile uint32_t *ioset, *ioclr, bit;
} led_ctl[] = {
  {(uint32_t*)&IOSET0, (uint32_t*)&IOCLR0, 1<<22},
  {(uint32_t*)&IOSET0, (uint32_t*)&IOCLR0, 1<<23},
  {(uint32_t*)&IOSET0, (uint32_t*)&IOCLR0, 1<<24},
  {(uint32_t*)&IOSET1, (uint32_t*)&IOCLR1, 1<<19}
};

void led_init() {
  PINSEL1 &= ~0x0003f000;
  PINSEL2 &= ~0x00000008;
  IODIR0 |= 0x01c00000;
  IODIR1 |= 0x00040000;
  IOCLR0 = 0x01c00000;
  IOCLR1 = 0x00040000;
}

void led_set(uint8_t led, uint8_t state) {
  if (state)
    *led_ctl[led].ioset = led_ctl[led].bit;
  else
    *led_ctl[led].ioclr = led_ctl[led].bit;
}

void led_toggle(uint8_t led) {
  if ((*led_ctl[led].ioset) & led_ctl[led].bit)
    *led_ctl[led].ioclr = led_ctl[led].bit;
  else
    *led_ctl[led].ioset = led_ctl[led].bit;
}

/* Tot vse. */
