//////////////////////////////////////////////////
/*	test lpcanvca smer pc => lpc		*/
/*	program pro spejblarm desku		*/
/*						*/
/* Autor: Martin Rakovec			*/
/* E-mail: martin.rakovec@tiscali.cz		*/
//////////////////////////////////////////////////

/* LPC21xx definitions */
#include <lpc21xx.h>
/* CAN definitions*/
#include <can/canmsg.h>
#include <can/lpcan.h>
#include <can/lpcan_vca.h>

/**
 * Main function 
 */
int main (void)  {

	MEMMAP = 0x1;	//flash
	//MEMMAP = 0x2;	//ram

	VICIntEnClr = 0xFFFFFFFF;
	VICIntSelect = 0x00000000;	

	/* CAN bus setup */
	uint32_t btr;
	vca_handle_t can;
	canmsg_t sendmsg = {.flags = 0, .id = 1, .length = 1, .data[0]=0};
	canmsg_t readmsg;
	/*20MHz je empiricky urcene cislo pro desku spejblarm(xtal 10MHz)*/
	/*s jinou konstantou zatim komunikace po can nebezi*/
	lpcan_btr(&btr, 1000000 /*Bd*/, 20000000, 0/*SJW*/, 70/*%sampl.pt.*/, 0/*SAM*/);
	lpc_vca_open_handle(&can, 0/*device*/, 0/*flags*/, btr, 10, 11, 12);	

	while(1){
		/* if nothig is recieved, continue*/
		if(vca_rec_msg_seq(can,&readmsg,1)<0) continue;
		sendmsg.data[0]++;
		if(readmsg.data[0] == 0xED)vca_send_msg_seq(can,&sendmsg,1);	
	}
}



