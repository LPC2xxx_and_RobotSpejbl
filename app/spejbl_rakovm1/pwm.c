/*
*  C Implementation: pwm
*
* Description: 
*
*
* Author: Martin Rakovec,,, <martin.rakovec@tiscali.cz>, (C) 2009
*
* Copyright: See COPYING file that comes with this distribution
*
*/

#include <pwm.h>

uint32_t PWM_PINSEL[] = {/**/1,/*PWM1*/1,/*PWM2*/15,/*PWM3*/3,/*PWM4*/17,/*PWM5*/11,/*PWM6*/19};

uint32_t *PWM_MR[] = {
	(uint32_t*)&(PWMMR0),
	(uint32_t*)&(PWMMR1),
	(uint32_t*)&(PWMMR2),
	(uint32_t*)&(PWMMR3),
	(uint32_t*)&(PWMMR4),
	(uint32_t*)&(PWMMR5),
	(uint32_t*)&(PWMMR6)
};

/**
 * Set registers (PWMPCR and PINSEL) for one PWM chanel
 * @param n PWM chanel (1-6)
 * @param double_edge double edge controlled mode (2-6), 0 for single edge
 */
void pwm_chanel(int n, int double_edge){
	/*Set PWM control register*/
	/*9.-14.bit for enable(1)|disable(0) PWM output*/
	/*2.-6.bit for double(1)|single(0) edge controlled mode(PWM2-6)*/
	PWMPCR |= ((1<<8) | (double_edge && n)) << n;
	if (n == 5){
		/*Set PINSEL1 bits (10 at 1 and 11 at 0) for PWM5 function*/
		PINSEL1 |= 1 << 10;
		PINSEL1 &= ~(1 << 11);
	}
	else{
		/*Set PINSEL0 bits (lower bit at 0, higher bit at 1) for PWM function*/
		PINSEL0 |= 1 << PWM_PINSEL[n];
		PINSEL0 &= ~(1 << (PWM_PINSEL[n]-1));
	}
}

/**
 * Set value for PWM chanel (single edge)
 * @param n PWM chanel
 * @param when value
 */
void pwm_set(int n, uint32_t when){
	*PWM_MR[n] = when;
	PWMLER |= 1 << n;
}

/**
 * Set value for PWM chanel (double edge)
 * @param n PWM chanel
 * @param from value
 * @param to value
 */
void pwm_set_double(int n, uint32_t from, uint32_t to){
	*PWM_MR[n-1] = from;
	*PWM_MR[n] = to;
	PWMLER |= 0x3 << (n-1);
}

/**
 * Initial settings for PWM
 * @param prescale PWM prescale
 * @param period PWM period
 */
void pwm_init(uint32_t prescale, uint32_t period){
	PWMPR = prescale - 1;	//set PWM prescale register
	PWMMR0 = period - 1;	//set PWM match register
	PWMLER |= 0x1;	//enable PWM Match0 latch
	PWMMCR |= 0x2;	//reset PWMTC on match0
	PWMTCR &= ~0x2;
	PWMTCR |= 0x9;	//eneble pwm  and counter
}
