/* Autor: Martin Rakovec	*/
#include <lpc21xx.h>                            /* LPC21xx definitions */
#include <deb_led.h>
#include <can/canmsg.h>
#include <can/lpcan.h>
#include <can/lpcan_vca.h>

vca_handle_t can;
uint8_t sendcount = 0;

/** ADC initialization 
* This function initialize AD convertor
*
* @param clk_div clock divider
*
*/
void adc_init(unsigned clk_div) {
	/* Set AD control registry */
	ADCR = ADC_CR_ADC0_m | ((clk_div&0xff)<<8)/*ADC clock*/ | ADC_CR_PDN_ON_m;  
	ADCR |= (1<<26); /* conversion started on falling edge of MAT0.1 */
}

void timer_irq() __attribute__((interrupt));

/** Timer inicialization 
* This function initializace timer0
* 
* @param prescaler prescaler
* @param period period
*
*/
void timer_init(uint32_t prescale, uint32_t period){
	T0PR = prescale - 1;	// set prescaler
	T0MR1 = period - 1;	// set period (match1)
	T0MCR = 0x3<<3;	//interrupt and counter reset on match1
	T0EMR = 0x1<<6 | 0x2; //set MAT0.1 low on match an hight now
	T0CCR = 0x0;	// no capture
	T0TCR |=0x1;	// start timer
}

/** Timer interrupt initialization
* Set VIC
* @param irq_vect vector number for timer interrupt
*/
void timer_init_irq(unsigned irq_vect){
	/* Set interrupt vector*/
	((uint32_t*)&VICVectAddr0)[irq_vect] = (uint32_t)timer_irq;
	((uint32_t*)&VICVectCntl0)[irq_vect] = 0x20 | 4;
	/* enable timer interrupt */
	VICIntEnable = 0x00000010;
}

uint16_t value;
/** Timer interrupt control
*/
void timer_irq(){	
	
	canmsg_t message;
	message.flags = 0;
	message.id = 0x400;
	/* wait for end of conversion */
	while((ADDR & (1<<31)) == 0);
	T0EMR |= 0x2; //set MAT0.1 hight
	/* Read value from AIN0*/
	value = ((ADDR>>6)&0x3ff);
	if (sendcount <10){
		message.length = 1;
		message.data[0] = value;
		vca_send_msg_seq(can,&message,1);
		sendcount++;
	}
	/* Clear interrupt flag */
	T0IR = 0xffffffff;
	VICVectAddr = 0;
}

void dummy_wait()
{
	unsigned int wait = 7000000;
	while(--wait);
}

int main (void)  {

	//MEMMAP = 0x1;	//flash
	MEMMAP = 0x2;	//ram

	VICIntEnClr = 0xFFFFFFFF;
	VICIntSelect = 0x00000000;	

	/* CAN bus setup */
	uint32_t btr;
	lpcan_btr(&btr, 1000000 /*Bd*/, 20000000, 0/*SJW*/, 70/*%sampl.pt.*/, 0/*SAM*/);
	lpc_vca_open_handle(&can, 0/*device*/, 0/*flags*/, btr, 10, 11, 12);	

	adc_init(3);
	timer_init(10,1000);
	timer_init_irq(13);

	
	canmsg_t message;
	message.flags = 0;
	message.id = 0x401;
	
	unsigned i = 0;

	while(1)
	{	
		i++;
		message.length = 1;
		message.data[0] = i;
		deb_led_change(LEDG);
		dummy_wait();
		vca_send_msg_seq(can,&message,1);
		sendcount=0;
	} 
}



