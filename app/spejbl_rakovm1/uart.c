//======================================================//
//							//
//	    Knihovna pro obsluhu UART portu		//
//							//
//------------------------------------------------------//
// Autor Martin Rakovec					//
//======================================================//

 #include <lpc21xx.h>  
 #include "uart.h"
 #include <system_def.h>

 #define  UART_DLAB		0x80
#define CLOCK_CPU 10000000l
#define CLOCK_CPU_APB 2*CLOCK_CPU

//////////////////UART0//////////////////////////////////

/**
* Inicializace portu UART0
* @param baudrate 	rychlost v baudech
* @param bits		velikost datoveho slova
* @param stopbit 	pocet stopbitu
* @param parit_en	parita zapnuta/vypnuta
* @param parit_mode	mod parity	
*/
void init_uart0(int baudrate, char bits, char stopbit, char parit_en, char parit_mode )	
{
	int pom , vala, valb;

	PINSEL0 |= ( PINSEL_1<< 0) | ( PINSEL_1 << 2);     
	/*mastaveni datoveho slova */
	U0LCR =UART_DLAB | bits | stopbit | parit_en | parit_mode;
	
	/*nastaveni rychlosti*/	
	pom =  (CLOCK_CPU_APB)/(16 * baudrate);	
	vala = (CLOCK_CPU_APB)/(16 * pom);
	valb = (CLOCK_CPU_APB/*CPU_APB_HZ*/)/(16 * (pom + 1));
	vala = baudrate - vala;
	valb = baudrate - valb;
	if (vala < 0) vala *= -1;
	if (valb < 0) valb *= -1;
	if (vala > valb) pom += 1;
	/*nastaveni preddelicky*/
	U0DLL = (char) (pom & 0xFF);
	U0DLM = (char) ((pom >> 8) & 0xFF); 
	U0LCR &= ~UART_DLAB;              // vynulovani DLAB 
 }

/**
* Nuceny prijem znaku z UART0
* @return	prijaty znak
*/
unsigned char uart0GetCh(void)
{
  while (!(U0LSR & 1));				// cekani na prichozi byte
    return (unsigned char)U0RBR;		// navraceni prijateho byte
}

/**
* Vyslani znaku na UART0
* @param ch	vysilany znak
*/
void uart0SendCh(char ch)
{
	while (!(U0LSR & 0x20));			// ceka na odeslani predchozich dat
	U0THR=ch;					// odeslani Byte
}

/**
* Vyslani zpravy na UART0
* @param msg[]	vysilana zprava
*/
void uart0SendMsg(char msg[]){
	int i;
	/*smycka pro postupne posilani znaku zpravy*/
	for (i = 0; i < strlen(msg); i++) uart0SendCh(msg[i]);
	
}

///////////UART1//////////////////////////////////////

/**
* Inicializace portu UART1
* @param baudrate 	rychlost v baudech
* @param bits		velikost datoveho slova
* @param stopbit 	pocet stopbitu
* @param pariti_en	parita zapnuta/vypnuta
* @param pariti_mode	mod parity	
*/
void init_uart1(int baudrate, char bits, char stopbit, char parit_en, char parit_mode )	
{
	int pom , vala, valb;

	PINSEL0 |= ( PINSEL_1<< 16) | ( PINSEL_1 << 18);     
	/*nastaveni datoveho slova*/
	U1LCR =UART_DLAB | bits | stopbit | parit_en | parit_mode;	
	/*nastaveni rychlosti*/
	pom =  (CLOCK_CPU_APB)/(16 * baudrate);	
	vala = (CLOCK_CPU_APB)/(16 * pom);
	valb = (CLOCK_CPU_APB)/(16 * (pom + 1));
	vala = baudrate - vala;
	valb = baudrate - valb;
	if (vala < 0) vala *= -1;
	if (valb < 0) valb *= -1;
	if (vala > valb) pom += 1;

	/*nastaveni preddelicky*/
	U1DLL = (char) (pom & 0xFF);
	U1DLM = (char) ((pom >> 8) & 0xFF);
	U1LCR &= ~UART_DLAB;              // vynulovani DLAB 

 }

/**
* Nuceny prijem znaku z UART1
* @return	prijaty znak
*/
unsigned char uart1GetCh(void)
{
  while (!(U1LSR & 1));                 	// cekani na prichozi byte
    return (unsigned char)U1RBR;		// navraceni prijateho byte
}

/**
* Vyslani znaku na UART1
* @param ch	vysilany znak
*/
void uart1SendCh(char ch)
{
	while (!(U1LSR & 0x20));		// ceka na odeslani predchozich dat
	U1THR=ch;				// odeslani Byte
	
}

/**
* Vyslani zpravy na UART1
* @param msg[]	vysilana zprava
*/
void uart1SendMsg(char msg[]){
	int i;
	/*smycka pro postupne posilani znaku zpravy*/
	for (i = 0; i < strlen(msg); i++) uart1SendCh(msg[i]);
}

