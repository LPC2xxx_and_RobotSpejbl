    /******************************************************************************/
    /*  This file is part of the uVision/ARM development tools                    */
    /*  Copyright KEIL ELEKTRONIK GmbH 2002-2004                                  */
    /******************************************************************************/
    /*                                                                            */
    /*  PWM.C:  LED Flasher                                                    */
    /*                                                                            */
    /******************************************************************************/
                    
    
    
#include <types.h>
#include <LPC210x.h>
#include "config.h"
#include "armVIC.h"
    
    
  
    /**
    *     * Function Name: lowInit()
    *
    * Description:
    *    This function starts up the PLL then sets up the GPIO pins before
    *    waiting for the PLL to lock.  It finally engages the PLL and
    *    returns
    *
    * Calling Sequence: 
    *    void
    *
    * Returns:
    *    void
    *
    * 
    */
    static void lowInit(void)
{
    // set PLL multiplier & divisor.
    // values computed from config.h
    PLLCFG = PLLCFG_MSEL | PLLCFG_PSEL;
    
    // enable PLL
    PLLCON = PLLCON_PLLE;
    PLLFEED = 0xAA;                       // Make it happen.  These two updates
    PLLFEED = 0x55;                       // MUST occur in sequence.
    
    // setup the parallel port pin
    IOCLR = PIO_ZERO_BITS;                // clear the ZEROs output
    IOSET = PIO_ONE_BITS;                 // set the ONEs output
    IODIR = PIO_OUTPUT_BITS;              // set the output bit direction
    
    // wait for PLL lock
    while (!(PLLSTAT & PLLSTAT_LOCK))
        continue;
    
    // enable & connect PLL
    PLLCON = PLLCON_PLLE | PLLCON_PLLC;
    PLLFEED = 0xAA;                       // Make it happen.  These two updates
    PLLFEED = 0x55;                       // MUST occur in sequence.
    
    // setup & enable the MAM
    MAMTIM = MAMTIM_CYCLES;
    MAMCR = MAMCR_FULL;
    
    // set the peripheral bus speed
    // value computed from config.h
    VPBDIV = VPBDIV_VALUE;                // set the peripheral bus clock speed
}
    
    
    /**
    * Function Name: sysInit()
    *
    * Description:
    *    This function is responsible for initializing the program
    *    specific hardware
    *
    * Calling Sequence: 
    *    void
    *
    * Returns:
    *    void
    * 
    */
    

    static void sysInit(void)
{
    lowInit();                            // setup clocks and processor port pins
    
    // set the interrupt controller defaults
#define RAM_RUN
#if defined(RAM_RUN)
    MEMMAP = MEMMAP_SRAM;                 // map interrupt vectors space into SRAM
#elif defined(ROM_RUN)
    MEMMAP = MEMMAP_FLASH;                // map interrupt vectors space into FLASH
#else
#error RUN_MODE not defined!
#endif
    VICIntEnClear = 0xFFFFFFFF;           // clear all interrupts
    VICIntSelect = 0x00000000;            // clear all FIQ selections
    VICDefVectAddr = (uint32_t)reset;     // point unvectored IRQs to reset()
    
    //  wdtInit();                            // initialize the watchdog timer
    //  initSysTime();                        // initialize the system timer
    //  uart0Init(UART_BAUD(HOST_BAUD), UART_8N1, UART_FIFO_8); // setup the UART
}
    
   void PWM0_isr(void) __attribute__ ((interrupt ("IRQ")));
   void PWM0_isr(void)
{
    PWMIR       |= 0x00000001;               // Clear match0 interrupt 
    VICVectAddr  = 0x00000000;
}  
    
    
    
    /**
    * *********************************** void init_PWM (void)***********************************************
    * This function initialise PWM registers in order to:
    *	-use PWM 2,4,6 channels
    *	-set the corresponding output pins enable
    *	-be able to modify the duty cycle while running
    * 
    */
    void init_PWM (void) {
          VICVectAddr8 = (unsigned)PWM0_isr;        /* Set the PWM ISR vector address */
          VICVectCntl8 = 0x00000028;                /* Set channel */
          VICIntEnable = 0x00000100;                /* Enable the interrupt */
    
        PINSEL0 |= 0x000A8000;                    /* Enable P0.7, 8 and P0.9 as alternate PWM outputs functions*/
        PINSEL0 &= 0xFFFABFFF;
        
        PWMPR    = 0x00000000;                    /* Load prescaler  */
    
        PWMPCR = 0x00005400;                      /* PWM channel 2 & 3 double edge control, output enabled */
        PWMMCR = 0x00000002;                      /* On match with timer reset the counter */
        PWMMR0 = 0x400;                           /* set cycle rate       */
     
        PWMMR2 = 0x100;                           /* set falling edge of PWM2  */
        PWMMR2 = 0x200;                           /* set falling edge of PWM2 */
        PWMMR6 = 0x300;                           /* set falling  edge of PWM3  */
        PWMLER = 0x55;                             /* enable shadow latch for match 1 - 6   */ 
        //PWMEMR = 0x00210A8E;                      /* Match 1 and Match 2 outputs set high  */
        PWMTCR = 0x00000002;                      /* Reset counter and prescaler           */ 
        PWMTCR = 0x00000009;                      /* enable counter and PWM, release counter from reset */
                
    }
    
    
    void main() {
    // unsigned int n;
    // int rx_char;
        sysInit();
        
        init_PWM();
    
        while (1) {                               /* Loop forever */
                
            
        }
    }
