//
// pwm.h
//
// Description: PWM unit configuration 
//
//
// Author: LAGARRIGUE <glagarri@etud.insa-toulouse.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include <LPC210x.h>
#include "config.h"


#define MINFREQ  (PCLK/(0xFFFFFFFF))

void Init_PWM(int freq);
void Set_PWM (int channel, float duty_cycle);
void Run_PWM (void);
void Stop_PWM (void);
