#include <types.h>
#include <lpc21xx.h>

#define LED_PINSEL     PINSEL1
#define LED_PINSEL_VAL 0xffff3fff;
#define LED_IODIR      IODIR0
#define LED_IOSET      IOSET0
#define LED_IOCLR      IOCLR0
#define LED_IOBIT      0x00800000

#define LED_RED 0
#define LED_YEL 1
#define LED_GRN 2
#define LED_BLU 3

/* LED init */
void led_init();
/* Set LED state */
void led_set(uint8_t led, uint8_t state);
/* Switch LED state */
void led_toggle(uint8_t led);

/* .oOo. */
