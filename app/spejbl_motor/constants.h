/****************************************/
/*	Constants definition for	*/
/*	  robot Spejbl - motor		*/
/****************************************/

/* CAN message IDs */
#define CAN_CLOCK_ID  0x401
#define CAN_CFGMSG_ID 0x4ab 
#define CAN_UNDEF_ID  0xf800

#define CANLOAD_ID 0x481 
//(*((volatile unsigned long *) 0x40000120))
#define MOTOR_ID CANLOAD_ID

/* CAN timing */
#define CAN_OVERSAMP 80

#define ADC_OVERSAMP 6
#define ADC_MA_LEN   (2*ADC_OVERSAMP)

/* position limits */
#define POS_MIN 0x060
#define POS_MAX 0x3a0

/* input channels */
#define ADCIN_POS 0
#define ADCIN_CURRENT 1

/* reserved value for power-off */
#define CTRL_OFF 0xffff
#define CTRL_MAX SHRT_MAX

//#define CTRL_INT_DIV  (1<<19)
#define CTRL_INT_DIV  (1<<15)
#define CTRL_INT_MAX  (CTRL_INT_DIV*PWM_PERIOD)
#define CTRL_AMP_MUL  (20.0/(12*(1<<11)))

/*controler constants*/
#define CTRL_PI_Q     ((4.0/7.0)*CTRL_INT_MAX*CTRL_AMP_MUL)
#define CTRL_PI_K     (0.6*CTRL_PI_Q)
#define CTRL_PI_Kn    (0.6*0.674*CTRL_PI_Q)

/* PWM and sampling timing */
#define PWM_PERIOD   3000
#define PWM_DEAD     60
