/****************************************/
/*	H-bridge control function	*/
/****************************************/

#include <types.h>
#include <lpc21xx.h>
#include "constants.h"

/* H-bridge set */
void hbridge_half_set(uint8_t side, uint32_t value);
/* H-bridge set */
void hbridge_set(int32_t value);
/* Switch off H-bridge */
void hbridge_off();
/* Init H-bridge */
void hbridge_init();
