#include <stdlib.h>
#include <types.h>
#include <limits.h>
#include <lpc21xx.h>
#include <cpu_def.h>
#include <string.h>
/* CAN definitions*/
#include <can/canmsg.h>
#include <can/lpcan.h>
#include <can/lpcan_vca.h>
/*****************/
#include "led.h"
#include "pwm.h"
#include "hbridge.h"
#include "constants.h"

#define adc_setup(src,clkdiv,on,start,edge) \
  (((src)&0x7) | ((((clkdiv)-1)&0xff)<<8) | ((!(!(on)))<<21) | \
  (((start)&0x7)<<24) | ((!(!(edge)))<<27))

/********* Variables **********/

/* A/D ISR <=> controller function variables */
volatile uint8_t timer_count = 0;
volatile uint32_t adc_i = 0;
volatile uint32_t adc_x = 0;
volatile int8_t current_negative = 0;

/* CAN handle */
vca_handle_t can;
/* CAN Bus Timing register value */
uint32_t btr = 0;
/* Own message for current position signalling */
canmsg_t tx_msg = {.flags = 0, .length = 8};
/* Recieved message */
canmsg_t rx_msg;

/* Command message ID and index */
uint16_t cmd_msg_id = CAN_UNDEF_ID, cmd_msg_ndx = 0; 

/* Command value, current position */
volatile int16_t rx_cmd_value = CTRL_OFF;
volatile uint8_t tx_request = 0;

/***/
volatile int16_t control_w = 0; 	/* reference current (*12) 	*/
volatile int32_t control_y; 		/* measured current (*12*80) 	*/
volatile int32_t control_x; 		/* measured position (*80) 	*/
volatile int32_t control_u; 		/* applied PWM (*80) 		*/
volatile int8_t control_on = 0; 	/* switches H-bridge on/off 	*/
/***/

uint8_t DBG_clk_seq = 0;

/* * * * */

void adc_irq() __attribute__((interrupt));

void timer_init() {
  /* zapni MAT0.1 na noze AIN0 */
  T0PR = 0; //prescale - 1
  T0MR1 = PWM_PERIOD/ADC_OVERSAMP-1; //period
  T0MCR = 0x2<<3; /* counter reset on match1 */
  T0EMR = 0x1<<6 | 0x2; /* MAT0.1 set low on match */
  T0CCR = 0x0; /* no capture */
  T0TCR |= 0x1; /* go! */

  sync_pwm_timer((uint32_t*)&T0TC);
}

void set_irq_handler(uint8_t source, uint8_t irq_vect, void (*handler)()) {
  /* set interrupt vector */
  ((uint32_t*)&VICVectAddr0)[irq_vect] = (uint32_t)handler;
  ((uint32_t*)&VICVectCntl0)[irq_vect] = 0x20 | source;
  /* enable interrupt */
  VICIntEnable = 1<<source;
}

inline void adc_init() {
  ADCR = adc_setup(1<<ADCIN_CURRENT, 14, 1, 0x4 /*MAT0.1*/, 1 /*FALL! edge*/);
}

/* Saturation */
inline int32_t add_sat(int32_t big, int32_t small, int32_t min, int32_t max) {
  if (big >= max - small)
    return max;
  if (big <= min - small)
    return min;
  return big + small;
}

/* PI controler */
int32_t pi_ctrl(int16_t w, int16_t y) {
  static int32_t s = 0;
  int32_t p, e, u, q;

  e = w;  e -= y;
  p = e;  p *= (int32_t)CTRL_PI_K;
  u = add_sat(p, s, -CTRL_INT_MAX, CTRL_INT_MAX);
  q = e;  q *= (int32_t)(CTRL_PI_K-CTRL_PI_Kn);
  s = add_sat(q, s, -CTRL_INT_MAX-p, CTRL_INT_MAX-p);

  return u/CTRL_INT_DIV;
}

/* MA filter */
int32_t ctrl_ma_filter(int32_t *mem, uint8_t *idx, int32_t val) {
  int32_t diff = val - mem[*idx];
  mem[*idx] = val;
  if (++*idx == CAN_OVERSAMP)
    *idx = 0;
  return diff;
}

/**** Control ****/
void control(int32_t ad_i, int16_t ad_x) {
  static int32_t cy = 0, cx = 0, cu = 0;
  static int ma_y[CAN_OVERSAMP],
    ma_x[CAN_OVERSAMP], ma_u[CAN_OVERSAMP];
  static uint8_t idx_y = 0, idx_x = 0, idx_u = 0;
  int32_t u;

  u = pi_ctrl(control_w, ad_i);

  if (control_on)
    hbridge_set(u);
  current_negative = (u < 0);

  cy += ctrl_ma_filter(ma_y, &idx_y, ad_i);
  cx += ctrl_ma_filter(ma_x, &idx_x, ad_x);
  cu += ctrl_ma_filter(ma_u, &idx_u, u);
  control_y = cy;
  control_x = cx;
  control_u = cu;
}

/*****/
void adc_irq() {
  static uint8_t ma_idx = 0;
  static int32_t ma_val[ADC_MA_LEN];
  static int8_t negative = 0;
  int32_t ad;
  int32_t ma_diff;
  int8_t last;

  /* read & clear irq flag */
  ad = ADDR;

  /* reset MAT0.1 */
  T0EMR = 0x1<<6 | 0x2; /* must be here due to ADC.5 erratum */

  if ((last = (timer_count == ADC_OVERSAMP-1))) {
    /* last sample before PWM period end */
    /* twice -- ADC.2 erratum workaround */
    ADCR = adc_setup(1<<ADCIN_POS, 14, 1, 0x0 /* now! */, 1);
    ADCR = adc_setup(1<<ADCIN_POS, 14, 1, 0x1 /* now! */, 1);
  }

  ad = (ad>>6)&0x3ff;

  if (negative)
    ad = -ad;
  /* shift value through MA filter */
  ma_diff = ad - ma_val[ma_idx];
  ma_val[ma_idx] = ad;
  if (++ma_idx == ADC_MA_LEN)
    ma_idx = 0;
  /* MA filter output (should be atomic): */
  adc_i += ma_diff;

  if (last) {
    while (((ad = ADDR)&0x80000000) == 0);
    adc_x = (ad>>6)&0x3ff;
    /* twice -- ADC.2 erratum workaround */
    ADCR = adc_setup(1<<ADCIN_CURRENT, 14, 1, 0x0 /*MAT0.1*/, 1);
    ADCR = adc_setup(1<<ADCIN_CURRENT, 14, 1, 0x4 /*MAT0.1*/, 1);
    timer_count = 0;
    negative = current_negative;
  }
  else
    ++timer_count;

  /* int acknowledge */
  VICVectAddr = 0;
}

/******/

void can_rx(){
  switch (rx_msg.id) {
  /*message from CAN clock*/
  case CAN_CLOCK_ID:
    if ((uint16_t)rx_cmd_value != CTRL_OFF) {
      control_w = rx_cmd_value;
      control_on = 1;
      DBG_clk_seq = rx_msg.data[0];
    }
    tx_request = 1;
    break;
  /*config message from control computer*/
  case CAN_CFGMSG_ID:
    /*is this message for this motor?*/
    if ((rx_msg.data[0] | ((rx_msg.data[1])<<8)) == MOTOR_ID) {
      /*ID of control message for this motor*/
      cmd_msg_id = rx_msg.data[2] | ((rx_msg.data[3])<<8);
      /*part of control message*/
      cmd_msg_ndx = rx_msg.data[4] | ((rx_msg.data[5])<<8);
    }
    break;
  default:
    /*control message for this motor*/
    if (rx_msg.id == cmd_msg_id) {
      /*current value*/
      rx_cmd_value = rx_msg.data[cmd_msg_ndx] | ((rx_msg.data[cmd_msg_ndx+1])<<8);
      /*switch off motor*/
      if ((uint16_t)rx_cmd_value == CTRL_OFF) {
	control_on = 0;
	hbridge_off();
      }
    }
    break;
  }
}

void can_tx() {

  /* x: 16b, y: 24b, u: 24b */
  uint16_t control_2 = (uint16_t)(control_x/2);
  tx_msg.data[0] = *((uint8_t*)&control_2);
  tx_msg.data[1] = *((uint8_t*)&control_2 + 1);
  tx_msg.data[2] = *((uint8_t*)&control_y);
  tx_msg.data[3] = *((uint8_t*)&control_y + 1);
  tx_msg.data[4] = *((uint8_t*)&control_y + 2);
  tx_msg.data[5] = *((uint8_t*)&control_u);
  tx_msg.data[6] = *((uint8_t*)&control_u + 1);
  tx_msg.data[7] = *((uint8_t*)&control_u + 2);
 /*debug message*/
 /* tx_msg.data[2] = *((uint8_t*)&cmd_msg_id+1);
  tx_msg.data[3] = *((uint8_t*)&cmd_msg_id);
  tx_msg.data[4] = *((uint8_t*)&cmd_msg_ndx+1);
  tx_msg.data[5] = *((uint8_t*)&cmd_msg_ndx);
  tx_msg.data[6] = *((uint8_t*)&rx_cmd_value+1);
  tx_msg.data[7] = *((uint8_t*)&rx_cmd_value);*/
  /* send message and do NOT wait*/
  vca_send_msg_seq(can,&tx_msg,1);
  tx_request = 0;
}

/*****************/

void sys_pll_init(uint32_t f_cclk, uint32_t f_osc) {
  uint32_t m, p, p_max;

  led_set(LED_YEL, 1);

  /* turn memory acceleration off */
  MAMCR = 0x0;

  /* compute PLL divider and internal osc multiplier */
  m = f_cclk/f_osc;
  p_max = 160000000/f_cclk;
  for (p = 3; ((1<<p) > p_max) && (p > 0); p = p>>1);
  /* switch PLL on */
  PLLCFG = (p<<5) | ((m-1)&0x1f);
  PLLCON = 0x1;
  PLLFEED = 0xaa;  PLLFEED = 0x55;
  /* wait for lock */
  while (!(PLLSTAT & (1<<10)));
  /* connect to clock */
  PLLCON = 0x3;
  PLLFEED = 0xaa;  PLLFEED = 0x55;

  /* turn memory acceleration on */
  MAMTIM = f_cclk/20000000;
  MAMCR = 0x2;

  led_set(LED_YEL, 0);
}

int main() {
  led_init();
  led_set(LED_GRN, 1);
  /* switch the CAN off due to clock change */
  //can_off(0);
  /* boost clock to 60MHz */
  sys_pll_init(60000000, 10000000);
  /* peripheral clock = CPU clock (60MHz) */
  VPBDIV = 1; /*!!! Pozor defaultne VPBDIV = 2 !!!*/

  /* init Vector Interrupt Controller */
  VICIntEnClr = 0xFFFFFFFF;
  VICIntSelect = 0x00000000;

  /* CAN bus setup */
  
  /*inicialice CANu*/
  /*Nyni plne funkcni na SpejblARM desce s 10MHz xtalem*/
  /*Zaroven funguje na euroboti desce s 10MHz xtalem*/
  /*Zde 60MHz kvuli pll*/
  lpcan_btr(&btr, 1000000 /*Bd*/, 60000000, 0/*SJW*/, 70/*%sampl.pt.*/, 0/*SAM*/);
  lpc_vca_open_handle(&can, 0/*device*/, 1/*flags*/, btr, 10, 11, 12);	

  tx_msg.id = MOTOR_ID;

  hbridge_init();
//  hbridge_set((PWM_PERIOD/ADC_OVERSAMP-2*PWM_DEAD));

  set_irq_handler(18 /*ADC*/, 13, adc_irq);
  adc_init();
  timer_init();


  for (;;) {
    if(vca_rec_msg_seq(can,&rx_msg,1)<1){
     if (timer_count == 1)
       control(adc_i, adc_x);
     if (tx_request)
       can_tx();
    }
    else can_rx();
  }
}
