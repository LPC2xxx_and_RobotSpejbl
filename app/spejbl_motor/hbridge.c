#include "hbridge.h"
#include "pwm.h"

void hbridge_half_set(uint8_t side, uint32_t value) {
  uint32_t *s_down, *d_down, *d_up;
  uint32_t t;

  if (side) {
    s_down = PWM_MR[6];  d_down = PWM_MR[4];  d_up = PWM_MR[3];
  }
  else {
    s_down = PWM_MR[5];  d_down = PWM_MR[2];  d_up = PWM_MR[1];
  }

  if (value < 2*PWM_DEAD) {
    /* 0% */
    *s_down = PWM_PERIOD;
    *d_down = PWM_PERIOD+1;
    *d_up = 0;
  }
  else if (value > PWM_PERIOD-2*PWM_DEAD) {
    /* 100% */
    *s_down = *d_down = 0;
    *d_up = PWM_PERIOD;
  }
  else {
    *d_up = t = PWM_PERIOD-PWM_DEAD;
    //*d_down = t -= value;
    /****** !!!!!!!!! *******/
    *d_down = t -= value - 2*PWM_DEAD;
    *s_down = t - PWM_DEAD;
  }

  PWMLER |= side ? 0x58 : 0x26;
}

void hbridge_set(int32_t value) {
  if (value >= 0) {
    hbridge_half_set(0, value);
    hbridge_half_set(1, 0);
  }
  else {
    hbridge_half_set(1, -value);
    hbridge_half_set(0, 0);
  }
}

void hbridge_off() {
  /* *s_down = 0 (L) */
  PWMMR5 = 0;
  PWMMR6 = 0;
  /* *d_down = PWM_PERIOD+1, *d_up = 0 (H) */
  PWMMR4 = PWM_PERIOD+1;
  PWMMR3 = 0;
  PWMMR2 = PWM_PERIOD+1;
  PWMMR1 = 0;
  PWMLER |= 0x58 | 0x26;
}

void hbridge_init() {
  /* PWM2,4 double-edged, PWM5,6 single-edged */
  pwm_channel(2, 1);
  pwm_channel(4, 1);
  pwm_channel(5, 0);
  pwm_channel(6, 0);
  /* both sides to GND */
  //hbridge_half_set(0, 0);
  //hbridge_half_set(1, 0);
  /* disconnect the bridge */
  hbridge_off();
  /* go */
  pwm_init(1, PWM_PERIOD);
}
