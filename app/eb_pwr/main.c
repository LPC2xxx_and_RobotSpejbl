////////////////////////////////////////////////////////////////////////////////
//
//                 Eurobot POWER BOAD  (with LPC2129)
//
// Description
// -----------
// This software controls the eurobot powerboard 
// Author : Jiri Kubias DCE CVUT
//
//
////////////////////////////////////////////////////////////////////////////////

#include <lpc21xx.h>                            /* LPC21xx definitions */
#include <string.h>
#include <deb_led.h>
#include <system_def.h>
#include <periph/can.h>
#include <can_ids.h>


#include "pwrstep.h"
#include "uart.h"

#define CAN_SPEED	1000000
#define CAN_ISR		0
#define ADC_ISR		1
#define TIME_ISR	2


#define CAN_TRY		20

#define ALERT_LOW	1
#define ALERT_MAIN	2
#define ALERT_BYE	3
#define ALERT_33V	4
#define ALERT_50V	5
#define ALERT_80V	6

#define BAT_CNT		10
#define	BAT_STAT_LOW	120000
#define BAT_STAT_MAIN	110000
#define BAT_STAT_BYE	105000

#define V33_MIN		30000
#define V50_MIN		45000
#define V80_MIN		75000

#define CAN_TIMEOUT	10

//extern unsigned int adc_val[4];

//#define TEST

can_msg_t msg;// = {.flags = 0, .dlc = 1};

unsigned int time_blink = 0;
unsigned int time_send_can = 0;
unsigned int time_alert = 0;

unsigned int time_timeout = 0;	
	
unsigned int can_send_cnt = 0;


void led_blik()
{
	if (time_blink == 0) time_blink = time_ms + 500;

	if (time_ms > time_blink)
	{
		deb_led_change(LEDG);
		time_blink = time_ms + 500;
	}

}

void send_alert(unsigned char type )
{
	
	
	msg.id = CAN_PWR_ALERT;
	msg.flags = 0;
	msg.dlc = 1;
	msg.data[0] = type;
	
	while (can_tx_msg(&msg));
			
}
		
unsigned int cnt_12V;
unsigned int cnt_10V;



void power_alert()
{
	if (time_alert == 0) time_alert = time_ms + 200;

	if (time_ms > time_alert)
	{
		
		if (adc_val[0] < BAT_STAT_BYE) 		// bat < 9,5V
		{
			deb_led_on(LEDR);
			send_alert(ALERT_BYE);
			pwr_50(PWR_OFF);
			//pwr_80(PWR_OFF);	
			pwr_33(PWR_OFF);
			

		}
		else if (adc_val[0] < BAT_STAT_MAIN) 		// bat < 12V
		{
			deb_led_on(LEDB);
			++cnt_10V;
			if (cnt_10V > BAT_CNT)
			{
				send_alert(ALERT_MAIN);
				pwr_50(PWR_OFF);
				//pwr_80(PWR_OFF);	
			}
			

		}	
		else if (adc_val[0] < BAT_STAT_LOW) 		// bat < 12V
		{
			deb_led_on(LEDY);
			++cnt_12V;
			if (cnt_12V > BAT_CNT)
				send_alert(ALERT_LOW);	
		}
		else 
			deb_led_off(LEDY);
		
		if (cnt_10V < BAT_CNT)
		{
			if (adc_val[3] < V80_MIN)
			{
				send_alert(ALERT_80V);	
			}
			
			if (adc_val[2] < V50_MIN)
			{
				send_alert(ALERT_50V);	
			}
			
			if (adc_val[1] < V33_MIN)
			{
				send_alert(ALERT_33V);	
			}
		}
		time_alert = time_ms + 500;
	}
}

void send_can()
{
	if (time_send_can == 0) time_send_can = time_ms + 200;

	if (time_ms > time_send_can)
	{
		deb_led_on(LEDB);

		msg.id = CAN_PWR_ADC1;
		msg.flags = 0;
		msg.dlc = 8;
		msg.data[0] = (((adc_val[0]) >> 24) & 0xFF);
		msg.data[1] = (((adc_val[0]) >> 16) & 0xFF);
		msg.data[2] = (((adc_val[0]) >> 8) & 0xFF);
		msg.data[3] = (((adc_val[0]) >> 0) & 0xFF);
		msg.data[4] = (((adc_val[1]) >> 24) & 0xFF);
		msg.data[5] = (((adc_val[1]) >> 16) & 0xFF);
		msg.data[6] = (((adc_val[1]) >> 8) & 0xFF);
		msg.data[7] = (((adc_val[1]) >> 0) & 0xFF);
			
		time_timeout = time_ms + CAN_TIMEOUT;
		//while(can_tx_msg(&msg) & (time_timeout >  time_ms))

		while (can_tx_msg(&msg));
			
		
		msg.id = CAN_PWR_ADC2;
		msg.flags = 0;
		msg.dlc = 8;
		msg.data[0] = (((adc_val[2]) >> 24) & 0xFF);
		msg.data[1] = (((adc_val[2]) >> 16) & 0xFF);
		msg.data[2] = (((adc_val[2]) >> 8) & 0xFF);
		msg.data[3] = (((adc_val[2]) >> 0) & 0xFF);
		msg.data[4] = (((adc_val[3]) >> 24) & 0xFF);
		msg.data[5] = (((adc_val[3]) >> 16) & 0xFF);
		msg.data[6] = (((adc_val[3]) >> 8) & 0xFF);
		msg.data[7] = (((adc_val[3]) >> 0) & 0xFF);
		
		time_timeout = time_ms + CAN_TIMEOUT;
		//while(can_tx_msg(&msg) & (time_timeout >  time_ms))
		
		while (can_tx_msg(&msg));
		deb_led_off(LEDB);
		time_send_can = time_ms + 500;
	}

}




void can_rx(can_msg_t *msg) {
	can_msg_t rx_msg;
	
	memcpy(&rx_msg, msg, sizeof(can_msg_t));
	

	if (rx_msg.id == CAN_PWR)
	{
		if(rx_msg.data[0] & (1<<0)) pwr_33(PWR_ON);
		if(rx_msg.data[0] & (1<<1)) pwr_50(PWR_ON);
		if(rx_msg.data[0] & (1<<2)) pwr_80(PWR_ON);
		
		if(rx_msg.data[0] & (1<<3)) pwr_33(PWR_OFF);
		if(rx_msg.data[0] & (1<<4)) pwr_50(PWR_OFF);
		if(rx_msg.data[0] & (1<<5)) pwr_80(PWR_OFF);
	}	
}



void init_perip(void)	   // inicializace periferii mikroprocesoru
{

	init_pwr();
	can_init_baudrate(CAN_SPEED, CAN_ISR, can_rx);
	init_adc(ADC_ISR);
	init_time (TIME_ISR);
	init_uart0((int)9600 ,UART_BITS_8, UART_STOP_BIT_1, UART_PARIT_OFF, 0 );


#ifdef TEST
	pwr_33(PWR_ON);
	pwr_50(PWR_ON);
	pwr_80(PWR_ON);
#else
	pwr_33(PWR_OFF);
	pwr_50(PWR_OFF);
	pwr_80(PWR_OFF);
#endif

	pwr_33(PWR_ON);
	pwr_50(PWR_ON);
	pwr_80(PWR_ON);
}


unsigned int time_delay = 0;


int main (void)  {


	init_perip();			// sys init MCU

	time_delay = time_ms + 1000;
	 
	while(time_ms < time_delay);

	while(1)
	{		   			
		led_blik();
		send_can();	//FIXME
		power_alert();

	} 
}



