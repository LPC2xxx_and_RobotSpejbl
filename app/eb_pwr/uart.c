
 #include <lpc21xx.h>  
 #include "uart.h"
 #include <system_def.h>




 #define  UART_DLAB		0x80


void init_uart0(int baudrate, char bits, char stopbit, char parit_en, char parit_mode )	
{			   	

	int pom , vala, valb;

	PINSEL0 |= ( PINSEL_1<< 0) | ( PINSEL_1 << 2);     

	U0LCR =UART_DLAB | bits | stopbit | parit_en | parit_mode;        // nastaven� datov�ho slova na 8 bit� a jeden stop bit bez parity, nastaven� DLAB na log "1"
	
	
	pom =  (CPU_APB_HZ)/(16 * baudrate);
	
	vala = (CPU_APB_HZ)/(16 * pom);
	valb = (CPU_APB_HZ)/(16 * (pom + 1));

	vala = baudrate - vala;
	valb = baudrate - valb;

	if (vala < 0) vala *= -1;
	if (valb < 0) valb *= -1;
	 	
	if (vala > valb) pom += 1;

	U0DLL = (char) (pom & 0xFF);
	U0DLM = (char) ((pom >> 8) & 0xFF);	 	                // nastaven� p�edd�li�ky na 57600Bd
	  
	U0LCR &= ~UART_DLAB;              // vynulov�n� DLAB 

 }


unsigned char uart1GetCh(void)			// Nuceny prijem z uart1
{
  while (!(U1LSR & 1));                 	// cekani na prichozi byte
    return (unsigned char)U1RBR;		// navraceni prijateho byte
}

unsigned char uart0GetCh(void)			// Nuceny prijem z uart1
{
  while (!(U0LSR & 1));				// cekani na prichozi byte
    return (unsigned char)U0RBR;		// navraceni prijateho byte
}

void uart1SendCh(char ch)			// vyslani znaku na uart1
{
	while (!(U1LSR & 0x20));		// ceka na odeslani predchozich dat
	U1THR=ch;				// odeslani Byte
	
}

void uart0SendCh(char ch)			// vyslani znaku na uart0
{
	while (!(U0LSR & 0x20));			// ceka na odeslani predchozich dat
	U0THR=ch;							// odeslani Byte
}


void init_uart1(int baudrate, char bits, char stopbit, char parit_en, char parit_mode )	
{			   	

	int pom , vala, valb;

	PINSEL0 |= ( PINSEL_1<< 16) | ( PINSEL_1 << 18);     

	U1LCR =UART_DLAB | bits | stopbit | parit_en | parit_mode;        // nastaven� datov�ho slova na 8 bit� a jeden stop bit bez parity, nastaven� DLAB na log "1"
	
	
	pom =  (CPU_APB_HZ)/(16 * baudrate);
	
	vala = (CPU_APB_HZ)/(16 * pom);
	valb = (CPU_APB_HZ)/(16 * (pom + 1));

	vala = baudrate - vala;
	valb = baudrate - valb;

	if (vala < 0) vala *= -1;
	if (valb < 0) valb *= -1;
	 	
	if (vala > valb) pom += 1;

	U1DLL = (char) (pom & 0xFF);
	U1DLM = (char) ((pom >> 8) & 0xFF);	 	                // nastaven� p�edd�li�ky na 57600Bd
	  
	U1LCR &= ~UART_DLAB;              // vynulov�n� DLAB 

 }



