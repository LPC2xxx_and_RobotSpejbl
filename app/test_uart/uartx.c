/******************************************************************************
 *
 * $RCSfile: $
 * $Revision: $
 *
 * This module provides interface routines to the LPC ARM UARTs.
 * Copyright 2004, R O SoftWare
 * No guarantees, warrantees, or promises, implied or otherwise.
 * May be used for hobby or commercial purposes provided copyright
 * notice remains intact.
 *
 * reduced to see what has to be done for minimum UART-support by mthomas
 *****************************************************************************/

// #warning "this is a reduced version of the R O Software code"

#include "uartx.h"

#define UART_LCR_DLAB  7

/**
 * initializes the desired UART perpipheral
 * @param uart   UART_SEL0 for UART0   and   UART_SEL1 for UART1
 * @param baud   Baud rate   (see uart.h for the type used - use UART_BAUD macro)
 * @param mode   mode - see typical modes (uart.h)
 * @param fmode fmode - see typical fmodes (uart.h)
 * NOTE: uartInit (UART_SEL0,B9600 , UART_8N1, UART_FIFO_8); to use UART0
 */
void uartInit(int uart, uint16_t baud, uint8_t mode, uint8_t fmode)
{
  // setup Pin Function Select Register (Pin Connect Block) 
  // make sure old values of Bits 0-4 are masked out and
  // set them according to UART0-Pin-Selectio
   
    
    if (uart == UART_SEL0)
        PINSEL0 |= 0x5;
    
    else if (uart == UART_SEL1)
        PINSEL0 |= 0x50000;
    
   
    
    URET(U0IER) = 0x00;             // disable all interrupts
    URET(U0IIR) = 0x00;             // clear interrupt ID register
    URET(U0LSR) = 0x00;             // clear line status register

  // set the baudrate - DLAB must be set to access DLL/DLM

    URET(U0LCR) = (1<<UART_LCR_DLAB); // set divisor latches (DLAB)
    
    URET(U0DLL) = (uint8_t)baud;         // set for baud low byte
    URET(U0DLM) = (uint8_t)(baud >> 8);  // set for baud high byte
  
  // set the number of characters and other
  // user specified operating parameters
  // Databits, Parity, Stopbits - Settings in Line Control Register
    URET(U0LCR) = (mode & ~(1<<UART_LCR_DLAB)); // clear DLAB "on-the-fly"
  // setup FIFO Control Register (fifo-enabled + xx trig) 
    URET(U0FCR) = fmode;
}

/**
 * prints one character on the desired UART peripheral
 * @param uart UART_SEL0 for UART0   and   UART_SEL1 for UART1
 * @param ch    one character
 * @return   return char  type: uint8_t
 */
int uartPutch(int uart, int ch)
{
    while (!(URET(U0LSR) & ULSR_THRE))          // wait for TX buffer to empty
    continue;                           // also either WDOG() or swap()

    URET(U0THR) = (uint8_t)ch;  // put char to Transmit Holding Register
  return (uint8_t)ch;      // return char ("stdio-compatible"?)
}

/**
 * writes a string
 * @param uart    UART_SEL0 for UART0   and   UART_SEL1 for UART1
 * @param string 
 * @return 
 */
const char *uartPuts(int uart, const char *string)
{
	char ch ;
       
	while ((ch = *string)) {
		if (uartPutch( uart, ch)<0) break;
		string++;
	}
       
	return string;
}


/**
 * checks if buffers are empty
 * @param uart  UART_SEL0 for UART0   and   UART_SEL1 for UART1
 * @return   1  if empty ; stg else if not
 */
int uartTxEmpty(int uart)
{
    return (URET(U0LSR) & (ULSR_THRE | ULSR_TEMT)) == (ULSR_THRE | ULSR_TEMT);
}

/**
 * Clears the TX fifo
 * @param uart  UART_SEL0 for UART0   and   UART_SEL1 for UART1
 */
void uartTxFlush(int uart)
{
    URET(U0FCR) |= UFCR_TX_FIFO_RESET;          // clear the TX fifo
}



/**
 * 
 * @param uart   UART_SEL0 for UART0   and   UART_SEL1 for UART1
 * @return   character on success, -1 if no character is available
 */
int uartGetch(int uart)
{
    if (URET(U0LSR) & ULSR_RDR)                 // check if character is available
        return URET(U0RBR);                       // return character

  return -1;
}
