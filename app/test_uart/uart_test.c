#include <LPC210x.h>
#include "uartx.h"



/**
 *  Function Name: lowInit()
 *
* Description:
 *    This function starts up the PLL then sets up the GPIO pins before
 *    waiting for the PLL to lock.  It finally engages the PLL and
 *    returns
 *
* Calling Sequence: 
 *    void
 *
* Returns:
 *    void
 *  
 */
static void lowInit(void)
{
    // set PLL multiplier & divisor.
    // values computed from config.h
    PLLCFG = PLLCFG_MSEL | PLLCFG_PSEL;

    // enable PLL
    PLLCON = PLLCON_PLLE;
    PLLFEED = 0xAA;                       // Make it happen.  These two updates
    PLLFEED = 0x55;                       // MUST occur in sequence.

    // setup the parallel port pin
    IOCLR = PIO_ZERO_BITS;                // clear the ZEROs output
    IOSET = PIO_ONE_BITS;                 // set the ONEs output
    IODIR = PIO_OUTPUT_BITS;              // set the output bit direction

    // wait for PLL lock
    while (!(PLLSTAT & PLLSTAT_LOCK))
        continue;

    // enable & connect PLL
    PLLCON = PLLCON_PLLE | PLLCON_PLLC;
    PLLFEED = 0xAA;                       // Make it happen.  These two updates
    PLLFEED = 0x55;                       // MUST occur in sequence.

    // setup & enable the MAM
    MAMTIM = MAMTIM_CYCLES;
    MAMCR = MAMCR_FULL;

    // set the peripheral bus speed
    // value computed from config.h
    VPBDIV = VPBDIV_VALUE;                // set the peripheral bus clock speed
}


/**
 *  Function Name: sysInit()
 *
* Description:
 *    This function is responsible for initializing the program
 *    specific hardware
 *
* Calling Sequence: 
 *    void
 *
* Returns:
 *    void
 *  
 */
static void sysInit(void)
{
    lowInit();                            // setup clocks and processor port pins

    MEMMAP = 1;              // map interrupt vectors space into FLASH

    VICIntEnClear = 0xFFFFFFFF;           // clear all interrupts
    VICIntSelect = 0x00000000;            // clear all FIQ selections
    VICDefVectAddr = (uint32_t)reset;     // point unvectored IRQs to reset()

}



/**
 * Creates a delay
 * @param d duration (unit not defined yet)
 */
void
        delay(int d)
{
    volatile int x;
    int i;
    for (i = 0; i < 10; i++)
        for(x = d; x; --x)
            ;
}

#define NUM_LEDS 4
static int leds[] = { 0x10000, 0x20000, 0x40000, 0x80000 };

/*////////////////////////////LEDS INITIALISATION///////////////////////*/

/**
 * Initializes leds.
 */
static void ledInit()
{
    IODIR |= 0x000F0000; /*leds connected to P0.16 17 18 & 19 should blink*/
    IOSET = 0x00000000;   /* all leds are switched off */
}

/**
 * Switches the leds off.
 * @param led  switched off led number. (integer)
 */
static void ledOff(int led)  /* Ioclr.i =1   =>   IOset.i cleared */
{
    IOCLR = led;
}

/**
 * Switches the leds on. 
 * @param led  switched on led number. (integer)
 */
static void  ledOn(int led)  /*  Ioset.i = 1   =>  P0.i = 1    */
{
    IOSET = led;
}




/**
 * this program uses UART1 to print hello as a test word
 * if everything is correct, leds 0 and 1 should be switched on
 * then the program ask to type a word and print it continuously (led 0 blinks at each write)
 * @param  
 * @return 
 */
int main(void)
{
    int i = 0;
    int written_int_word = -1;    
    char *testword = "hello";
    
    sysInit();
    
    ledInit();
    ledOn(leds[0]);
    
        
    uartInit(UART_SEL1, B9600 , UART_8N1, UART_FIFO_8);
 
    
    ledOn(leds[1]);
       
        uartPutch(UART_SEL1, '\n');
        uartPuts( UART_SEL1, testword);
        
        uartPuts( UART_SEL1, "\nPlease write one character:");
        
        uartPutch(UART_SEL1, '\n');
        while(written_int_word == -1)
            written_int_word = uartGetch(UART_SEL1);
        
        
        while(1)
        {  
            if (i++ & 1) ledOn(leds[0]);
            else ledOff(leds[0]);
                uartPutch(UART_SEL1, written_int_word);
                delay(200000);
              
        }
    
    return 0;
    } 
