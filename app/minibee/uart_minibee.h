//
// Author: Bc. Jiri Kubias <Jiri.kubias@gmail.com>, (C) 2008
//
// Copyright: (c) DCE FEE CTU - Department of Control Engeneering
// License: GNU GPL v.2
//

/**
 * @file   uart_minibee.h
 * @author Bc. Jiri Kubias , DCE FEL CTU 2008
 * 
 * @brief  Uart library - function prototypes
 * 
 */

#ifndef UART_MINIBEE_H
#define UART_MINIBEE_H

void uart_send_char(char val);
char uart_get_char(void);
void init_uart(void);
void send_rs_int(int val);
void send_rs_str(char data[]);



#endif
