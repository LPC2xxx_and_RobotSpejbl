<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="page">
    <name>index</name>
    <title>Zigbee radio MC13192 or MC13202</title>
    <filename>index</filename>
    <docanchor file="index">Introduction</docanchor>
  </compound>
  <compound kind="file">
    <name>main.c</name>
    <path>/home/data/robot/eurobot/sysless-lpc21xx/app/minibee/</path>
    <filename>main_8c</filename>
    <includes id="MC1319x_8h" name="MC1319x.h" local="yes" imported="no">MC1319x.h</includes>
    <includes id="spi_8h" name="spi.h" local="yes" imported="no">spi.h</includes>
    <includes id="uart__minibee_8h" name="uart_minibee.h" local="yes" imported="no">uart_minibee.h</includes>
    <member kind="function">
      <type>void</type>
      <name>sendData</name>
      <anchorfile>main_8c.html</anchorfile>
      <anchor>ee6000bc24613b40e31fa4879578fb31</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>recieveData</name>
      <anchorfile>main_8c.html</anchorfile>
      <anchor>abecc12f56c4bb08f5b9beb4e655742b</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>main</name>
      <anchorfile>main_8c.html</anchorfile>
      <anchor>840291bc02cba5474a4cb46a9b9566fe</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="variable">
      <type>struct Message</type>
      <name>MsgSnd</name>
      <anchorfile>main_8c.html</anchorfile>
      <anchor>0ede4cc1258337c4c600518d2cc34a5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct Message</type>
      <name>MsgRcv</name>
      <anchorfile>main_8c.html</anchorfile>
      <anchor>5ead1224beac6631f8f3ca0f57fffc2c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>MC1319x.c</name>
    <path>/home/data/robot/eurobot/sysless-lpc21xx/app/minibee/</path>
    <filename>MC1319x_8c</filename>
    <includes id="spi_8h" name="spi.h" local="yes" imported="no">spi.h</includes>
    <includes id="MC1319x_8h" name="MC1319x.h" local="yes" imported="no">MC1319x.h</includes>
    <includes id="MC1319xdef_8h" name="MC1319xdef.h" local="yes" imported="no">MC1319xdef.h</includes>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_RecievePaket</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>3fe506ae7cef806e892a0f736bf742e5</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>ext_isr</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>e42a33dfd728c53890e5ecb159f57c7f</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>dummy_wait</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>0ed86ef3cd64826822473dbbebe0d7a4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>MC_Woodoo</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>b088fe90babf0fdf82ee824d04858094</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_Reset</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>099634cdb7405d32200e6556fca27b6c</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_ED</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>98f73499d8a965377ebb976c8a682d0c</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_SendPaket</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>f4f8db7c7df1b7982e6b9a3bd217be0b</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_ReSendPaket</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>9263dab03a7f0a473ef7fada3e54c837</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>uint16_t</type>
      <name>MC_WhoAmI</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>1a3a6d44b176d3b528f464ca74a4f7b3</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_SetChannel</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>bfbc2cbe1edf2878ccb09cc58edffd22</anchor>
      <arglist>(uint8_t channel)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_SetClko</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>339d44adf7ab46d02a17a11208a25800</anchor>
      <arglist>(uint16_t tick, uint8_t enable)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_SetPa</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>02257409afdb9f37b481989507fb5024</anchor>
      <arglist>(uint8_t val)</arglist>
    </member>
    <member kind="variable">
      <type>struct Message *</type>
      <name>rcvBuf</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>eb0be220f20aa0923d8122c5c42c5971</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>struct Message *</type>
      <name>sndBuf</name>
      <anchorfile>MC1319x_8c.html</anchorfile>
      <anchor>0382b4fc5cd190a5c5e83670d1e4f2bb</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>MC1319x.h</name>
    <path>/home/data/robot/eurobot/sysless-lpc21xx/app/minibee/</path>
    <filename>MC1319x_8h</filename>
    <includes id="MC1319xdef_8h" name="MC1319xdef.h" local="yes" imported="no">MC1319xdef.h</includes>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_Reset</name>
      <anchorfile>MC1319x_8h.html</anchorfile>
      <anchor>099634cdb7405d32200e6556fca27b6c</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint16_t</type>
      <name>MC_WhoAmI</name>
      <anchorfile>MC1319x_8h.html</anchorfile>
      <anchor>1a3a6d44b176d3b528f464ca74a4f7b3</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_ED</name>
      <anchorfile>MC1319x_8h.html</anchorfile>
      <anchor>98f73499d8a965377ebb976c8a682d0c</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_SendPaket</name>
      <anchorfile>MC1319x_8h.html</anchorfile>
      <anchor>f4f8db7c7df1b7982e6b9a3bd217be0b</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_ReSend</name>
      <anchorfile>MC1319x_8h.html</anchorfile>
      <anchor>cc2bfa1d89a61fc6370a82404947bec3</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_RecievePaket</name>
      <anchorfile>MC1319x_8h.html</anchorfile>
      <anchor>3fe506ae7cef806e892a0f736bf742e5</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_SetClko</name>
      <anchorfile>MC1319x_8h.html</anchorfile>
      <anchor>339d44adf7ab46d02a17a11208a25800</anchor>
      <arglist>(uint16_t tick, uint8_t enable)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_SetPa</name>
      <anchorfile>MC1319x_8h.html</anchorfile>
      <anchor>02257409afdb9f37b481989507fb5024</anchor>
      <arglist>(uint8_t val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>dummy_wait</name>
      <anchorfile>MC1319x_8h.html</anchorfile>
      <anchor>0ed86ef3cd64826822473dbbebe0d7a4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>ext_isr</name>
      <anchorfile>MC1319x_8h.html</anchorfile>
      <anchor>2d5b82f5c627f8123fc48a6ab2795743</anchor>
      <arglist>(void) __attribute__((interrupt))</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>MC1319xdef.h</name>
    <path>/home/data/robot/eurobot/sysless-lpc21xx/app/minibee/</path>
    <filename>MC1319xdef_8h</filename>
    <class kind="struct">Message</class>
    <member kind="define">
      <type>#define</type>
      <name>LPC</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>3dee031a5e8ffec497faf7e5e5d42565</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>46fc8a17c0936cda0556a966cc2f62fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CE</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>f332f65aca07c44deeda884c8f6c353c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ATTN</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>67481fc0148df36eb20f078ff1ce2d7c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RXTXEN</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>75eaa04e13035b83f95c3598bf0fef44</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RST</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>c5d957e4fd3dc11cd97a54cf9ca057a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MOSI</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>5d3f11f2fdf8a7e27b975291e0c2c8cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>MISO</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>7334c540878c8c4d801fd75ed9fd8063</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SCK</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>2dd443a4430713d325ab86895a4a45eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLR</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>74bc9444cf3ef3289a07cea717e16764</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>SET</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>1e573d4c5b004a71c86c15800b65708f</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RD</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>efaf886fe6a8977a1a887fb491a6e82c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>WR</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>c1efd35edf6555320eb2a4b0b2d9c3a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RESET</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>b702106cf3b3e96750b6845ded4e0299</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RX_PKT_RAM</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>5dbcafbeb02080d09e78e52bcadb1fb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TX_PKT_RAM</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>ef9d1f4f568dc8d568c508ace69cb0c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TX_PKT_CTL</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>ca3f7b52cd37cf4e19aaccb3705492ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TX_PKT_CTL_RAM2_SELm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>5e21c8cf94564a8e982aafd43f2b51a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TX_PKT_CTL_PKT_LENGHT</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>4e6e1ca84ae3a3c72effbf7ba360fe72</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CCA_THRESH</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>880d164cbc98171e082c9554ff99dd02</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>300b7e28a313b19099713ea04f09fdaa</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK_ATTNm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>2058a9037b76ad2aa0e58b56e21a5d3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK_RAM_ADDRm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>fdb37cb7a5705480c01ed27837a4de2c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK_ARB_BUSYm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>cd2c8c3ab879d459f020937d48ef8f0c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK_STRM_DATAm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>04136886c78ee543a535469a04dc0024</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK_PPL_LOCKm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>b4cbcabfa5fc3a1cc629f850837e761e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK_ACOMAm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>6bcf0982d5b7ed6a894ea67b57d67696</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK_DOZEm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>f3080a2c1781eab4959dfc4ba207966c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK_TMR4m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>f6c0584bbcfed769be34f75ff7298fca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK_TMR3m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>7affba036c584bcc7443dbb3d395bbbe</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK_TMR2m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>3ccfe72ab064a6c1989c32d1e6e913cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_MASK_TMR1m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>5f6abe73e9473ae483fae1e33cef9130</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>32782596b83917d3ac2000ee2114807f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_TX_STRMm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>2b47fec88c7ae45d3e143c1204f8e205</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_RX_STRMm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>31405fd6c34f97776ce08e04b290e039</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCAm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>e31a0f9699124efbeba1b03605861222</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_TX_SENTm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>135b40fc49c8928232368b0e2ab945cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_RX_RCVDm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>a72b9d79fdf692d4745c46de3a8ac9e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_TMR_TRIG_ENm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>19db7d302b1adaca235f4b23643761a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCA_TYPE1m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>6a9ceab99f026a86acf0118f000a0ea5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCA_TYPE0m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>43a0428a419cb65541b776157ce03181</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCA_CCAm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>ad34e01523309e53b1bd2a39587b69bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCA_EDm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>def18f0318ff5e45578492b68c031bd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCA_XCVR_SEG1m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>f07851303ac030fa45293517e87bfd51</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCA_XCVR_SEG0m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>214e92f0776df62a191edfc135da40fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCA_XCVR_IDLEm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>1575fe96d0855965ede245beeb37def3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCA_XCVR_CCAEDm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>68c3ef1f27068d2137f4497f85d88a44</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCA_XCVR_PMRXm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>4cbc11d3d1be22405f6b4b1758f6b52d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCA_XCVR_PMTXm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>a9c7f23a2e1016d403b16eea7634ef69</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_A_CCA_XCVR_CLRm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>0994c15a3bcf145696c112fa2446baa3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_B</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>e1d91669b06dc7d7ea30e41d9048c24a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_B_TMR_LOADm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>52638c2633f541320f2287e7a3d41bcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_B_MISO_HIZ_ENm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>6e3dc0539fb74080b152173dbf5249d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_B_CLKO_DOZE_ENm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>47efa6299949f8282564b6ddff8361de</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_B_TX_DONEm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>2e92d84c42aa565f1c8a3a2aeb96b05e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_B_RX_DONEm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>7d578ade881dd6f42da81129c483768d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_B_USE_STM_MODEm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>4e862c709c5744ff81cafe421cd39230</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_B_HIB_ENm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>41b1e45b4f7834017facd0c0c271c3e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_B_DOZE_ENm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>3abcad46bcaadf1f96857f10e8153c17</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PA_ENABLE</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>03d8a3cfd38ec5b486574866d25e7160</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PA_ENABLE_PA_ENm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>11221505bac308b205cb5f44329089f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_C</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>13c2a9e1f2b382c5a68735dfd1c67ff9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_C_GPIO_ALT_ENm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>4fb17fccc237e0aa44bc163885f41607</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_C_CLKO_ENm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>3ceb2e0ba9fa782849bf58ef7a42dbd8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_C_TMR_PRESCALE2m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>501c7e4655ba80c362f870355239a89d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_C_TMR_PRESCALE1m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>0d1c78de1fe7d1f8c1bb3fe95ae4fc1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONTROL_C_TMR_PRESCALE0m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>dd87271c69ce838f76b39bcc28302637</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>48b3d5ea128025ef2bb93b69472e2e76</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_RATE2m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>b6a90e47e9204a2cdb9a8c0792a342dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_RATE1m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>0b119f41dc0193c0f581f7b41a14faf4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_RATE0m</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>b6727557837aeb16c4411bef5a05b286</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_16Mm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>21d5b7a3aa938c82af3ba51709bc939d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_8Mm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>a064e808fcaf27bb504d651963a1387c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_4Mm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>794670cd1c019221c6407cfd8818d5a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_2Mm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>af17045ee51561e7d132cf6d545dd579</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_1Mm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>479f24a20e83b23f6dc02f5356997d32</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_62Km</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>9c97032a5f2c63dc7a004832e00a2702</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_32Km</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>5685e76f4f1b7594670f20d9e0a3f10b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_16Km</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>d258c47d3822ab8e2280fefe8e8d670a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_ONm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>ae16c3c3c02814f174b355f2370e8345</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_OFFm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>60d6f5ef48ff9a7c1e25dad819f443d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CLKO_CTL_CLKO_CLRm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>81af51f369345a34d4d2c1fbf938b60c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPIO_DIR</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>637a20b456c93e8248c861fa48510841</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPIO_DATA_OUT</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>c7f8340f1080d59df32708a2455100d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>ca2fcb248af07823b5886b348147ef77</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH1</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>3d8cc422bd40604871cea8560533c349</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH2</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>5828d649515d40f2efa45adf909b40f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH3</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>2b9841efc56c22fef5b7761c07608c02</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH4</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>1873bb919beb0f3b00adc47a88f7b42a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH5</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>b6d2b844931e00597abf0791d1275935</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH6</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>1e1aa4ddfe8c9e2f3f9f5895cdd751c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH7</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>10c4723cb836a3fe91038f4bf8a778ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH8</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>3dc6818a0c97ca323a66da1fa17aecef</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH9</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>a8bf98d75a543e72b4e18b71b3c116bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH10</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>30f1a46a6bfd01ff7fdb761f08ccc00b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH11</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>6d53ae069f2db801a8aa5ca588154d5d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH12</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>be4a9b341b8ef487c2b52e6d453c8b2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH13</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>bd14afc4fda0fe79399f872485d06e93</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH14</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>8d0551fd82943350b1823e73d9b75f10</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH15</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>26942e0ee9e2a2bb7cb0d65706272419</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_INT_DIV_CH16</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>e626dced715d6d66b912f8d3ea2ae1e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>ee243f6fb1b3ce251b930cb54b23f79a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH1</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>f5068b92d6689a67ed049a0cba5169f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH2</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>50d85c92c1990a95c5aefcee73934ce6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH3</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>e421d0e3ff2c2e4509fe3d9b5b88af92</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH4</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>02c947074cd499e03ee4c220f90d1eab</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH5</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>6dd8f5ac6bcfbb4497d497445dcc0463</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH6</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>5e39d950d283afb15348988954157381</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH7</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>ebb64bc2122bdcbb14ced1f0a4582490</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH8</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>74c61e029b4de5ea1c2b5e1a153f7b76</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH9</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>019b4cdba93a4389755cda6734e65d50</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH10</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>b3c3c89927d46541d426782763d2e735</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH11</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>1f0a5c59a135d9727596cdfd05a846bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH12</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>72aee0649597e7a5fa3ca7eacda5ce99</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH13</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>9aa250768ccb29f252984121068d58df</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH14</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>7cb73d3ef359fea2c4890107c8bbc2e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH15</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>15e2d2c3f13c0d95bff881a29c3ebea1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LO1_NUM_CH16</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>384176be7d57b8074ca2f82b6c5b42af</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH1</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>f0006c05c4805a128d72844be86117e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH2</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>7bfda5ea1d061ec6f3bf37bc660bc617</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH3</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>1af5e53fb85e1e625e59fb8a09a489ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH4</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>f26e5f28479ab253427f9b7d38b770b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH5</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>2999354d1bb49b8821db71d70e940138</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH6</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>6de37bb572cb3af47d63bc56dafdc0ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH7</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>2c6e23deb902bffa8c8691933dbdc60e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH8</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>e2ac642a24cbf28a52d1c6169a9f9659</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH9</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>50fb7c4a3d6ea560bc713d5e6c87f195</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH10</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>fb83f0555eeaf966f6d3f150e40e7c3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH11</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>4ad013cb3d33a46238f5039a5c9b279c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH12</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>23fe1fc2d00a43d5575814d431a65606</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH13</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>48f3d8c16f245f9679badc36726b4398</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH14</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>b74d49877e12fdfa277f673cdfafc3c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH15</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>4f9cd9fe7fca8245504d7f4c09037012</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH16</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>7c20da487a0e4b366e20682602422d0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_11</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>add0e37c1b38c9e0955ef41d80b4a996</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_12</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>f3b93763bd0614b8386ce6f9564f45a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_13</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>c3dd7741bff2172736a7e254edb691de</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_14</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>772d39dab2bb87d7cd6d01bd9663b462</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_15</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>6d8d19096d0efb253ec8f962303d94e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_16</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>5bcb9b9838b8987b7ae303b08ca99a21</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_17</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>a42d94a638095e514bf313e8b2b61689</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_18</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>febe80e9aef311e427955b35711d159e</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_19</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>c984c397b20815cca07b749185d5159f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_20</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>8574fc3e1bbb87f8b5b8e9131cc34c10</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_21</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>a49bc4ffb27ccbb9b0d1a679478bea42</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_22</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>9fdf04c0081acb570253d3b985486740</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_23</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>b25eb6c47e4e0b25b3eb74edb0a29a09</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_24</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>ebd9e404761772dbf5cbb2104af05ffb</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_25</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>07cbd4348da4bb44921b3104dc113d16</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ZB_CH802_26</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>878672cb6bbb2bba1107c15a6bb67148</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PA_LVL</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>b837bb078e0fd8faa4ee20cd6d2c3834</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TMR_CMP1_A</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>17e25931d2b07605940ea4d52e8db955</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TMR_CMP1_B</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>b6341691cfe50e5635ecf6d10ef56c6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TMR_CMP2_A</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>e33c3e9cf354e579a97b396653b223ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TMR_CMP2_B</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>a657c55c046dd6d05bd252de05531019</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TMR_CMP3_A</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>f6965c3a07ed9d9bc6b3e538ae8d3eb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TMR_CMP3_B</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>574a0202652adc3814bab565246abc94</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TMR_CMP4_A</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>fd71dd54d591f2d3e3daafc17b5d1a34</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TMR_CMP4_B</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>6b421528226949439ebd37b8192e68a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TC2_PRIME</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>8e2f16b12eb58396b7c7935a74fd74cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>8cc14b27cb7ca6346eb2cb77f60a7162</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_PLL_LOCK_IRQm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>63045dd1094419c36eef6b9756207869</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_RAM_ADR_ERRm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>10816fac2f92317f815c40009cc03865</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_ARB_BUSY_ERRm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>a5b1710f897be1686888449ada16de3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_SRTM_DATA_ERRm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>9b34fa209fa94e518af14c75ff4bf332</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_ATTN_IRQm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>23a37ad91f39fd42da5dac455f33b425</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_DOZE_IRQm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>32115d02f58d9b2b80e47dea985ec2da</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_TMR1_IRQm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>7e554504f3365987340bfde581e318a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_RX_RCVD_IRQm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>53b72aad750dafa7c91272a75b8eb1ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_TX_SENT_IRQm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>d08e0d0ffe644e8e1e9711a59ce57c98</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_CCA_IRQm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>2ccc3e0f3c733a465a897b02cf9a3979</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_TMR3_IRQm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>60da12a1f658c65956e0d053f976ca7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_TMR4_IRQm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>01ca426dc8f08a51319499cab08265e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_TMR2_IRQm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>989b3258d9b8de2e632cb2aa65ed81c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_CCAm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>e49891dee228c6228fd3781b4b721109</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>IRQ_STATUS_CRC_VALIDm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>820c37f43676f86c52b55e69fb5447b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RST_IND</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>4a2e072154eaa212f2350194ae32599b</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RST_IND_RESET_INDm</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>8d2089e7cd64d0f70c3a4e3b36c20709</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CURRENT_TIME_A</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>94975bae26111e45a2af6937c4d04a91</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CURRENT_TIME_B</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>418cebf4c10e28e7f72b339ae3dff26f</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>GPIO_DATA_IN</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>f6602aa14449f25f424cf3cb17878b15</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CHIP_ID</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>b0aa3e54cd2934b67b9882957456c391</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>RX_STATUS</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>731d30ee0b5f9f0471fa8e742ef1784c</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMESTAMP_A</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>6e4773e12809f5b733361dbccda1289a</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>TIMESTAMP_B</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>33dd86aa81fb87c913253e5f3db9dad3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BER_ENABLE</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>02d472151e2c7429cf0d3bcbb2d2807d</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>BER_ENABLE_BER_EN</name>
      <anchorfile>MC1319xdef_8h.html</anchorfile>
      <anchor>69f2a6088bf5db62f8f994e598255e30</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>spi.h</name>
    <path>/home/data/robot/eurobot/sysless-lpc21xx/app/minibee/</path>
    <filename>spi_8h</filename>
    <includes id="MC1319xdef_8h" name="MC1319xdef.h" local="yes" imported="no">MC1319xdef.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>SPI_SPEED</name>
      <anchorfile>spi_8h.html</anchorfile>
      <anchor>0056b7fdd0f81fad9cd38e9fd389a4b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_Init</name>
      <anchorfile>spi_8h.html</anchorfile>
      <anchor>cb06ef92bde513fe8c00bb87ece1e408</anchor>
      <arglist>(int rx_isr_vect)</arglist>
    </member>
    <member kind="function">
      <type>uint16_t</type>
      <name>spi_Read</name>
      <anchorfile>spi_8h.html</anchorfile>
      <anchor>02fffb26d66333b3c6428ff7a3bbc3fd</anchor>
      <arglist>(uint8_t reg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_Write</name>
      <anchorfile>spi_8h.html</anchorfile>
      <anchor>9431e598f3537a5369afb01627ad24a3</anchor>
      <arglist>(uint8_t reg, uint16_t val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_ReadModifiWrite</name>
      <anchorfile>spi_8h.html</anchorfile>
      <anchor>45835fb3ddc2778b07b2d63b72ccef11</anchor>
      <arglist>(uint8_t reg, uint16_t ormask, uint16_t andmask)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_Write_Buf</name>
      <anchorfile>spi_8h.html</anchorfile>
      <anchor>a3d5ab927bca2d3c5deb2d49d3bd0be6</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_Read_Buf</name>
      <anchorfile>spi_8h.html</anchorfile>
      <anchor>fa22a171b888b8cd8676d160d69d1d7e</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>uint8_t</type>
      <name>MC_SetChannel</name>
      <anchorfile>spi_8h.html</anchorfile>
      <anchor>bfbc2cbe1edf2878ccb09cc58edffd22</anchor>
      <arglist>(uint8_t channel)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>disable_IRQ_pin</name>
      <anchorfile>spi_8h.html</anchorfile>
      <anchor>6ad5b67346e28c06a14b412451c9c042</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enable_IRQ_pin</name>
      <anchorfile>spi_8h.html</anchorfile>
      <anchor>df95af49d1fd2524c60090ba3499923a</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>spi_avr.c</name>
    <path>/home/data/robot/eurobot/sysless-lpc21xx/app/minibee/</path>
    <filename>spi__avr_8c</filename>
    <includes id="MC1319xdef_8h" name="MC1319xdef.h" local="yes" imported="no">MC1319xdef.h</includes>
    <member kind="function">
      <type>void</type>
      <name>spi_Init</name>
      <anchorfile>spi__avr_8c.html</anchorfile>
      <anchor>3243eee798d6c71f2e826659953b5480</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>uint16_t</type>
      <name>spi_Read</name>
      <anchorfile>spi__avr_8c.html</anchorfile>
      <anchor>02fffb26d66333b3c6428ff7a3bbc3fd</anchor>
      <arglist>(uint8_t reg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_Write</name>
      <anchorfile>spi__avr_8c.html</anchorfile>
      <anchor>9431e598f3537a5369afb01627ad24a3</anchor>
      <arglist>(uint8_t reg, uint16_t val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_Read_Buf</name>
      <anchorfile>spi__avr_8c.html</anchorfile>
      <anchor>fa22a171b888b8cd8676d160d69d1d7e</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_Write_Buf</name>
      <anchorfile>spi__avr_8c.html</anchorfile>
      <anchor>a3d5ab927bca2d3c5deb2d49d3bd0be6</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_ReadModifiWrite</name>
      <anchorfile>spi__avr_8c.html</anchorfile>
      <anchor>45835fb3ddc2778b07b2d63b72ccef11</anchor>
      <arglist>(uint8_t reg, uint16_t ormask, uint16_t andmask)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>spi_LPC.c</name>
    <path>/home/data/robot/eurobot/sysless-lpc21xx/app/minibee/</path>
    <filename>spi__LPC_8c</filename>
    <includes id="MC1319x_8h" name="MC1319x.h" local="yes" imported="no">MC1319x.h</includes>
    <includes id="spi_8h" name="spi.h" local="yes" imported="no">spi.h</includes>
    <member kind="function">
      <type>void</type>
      <name>disable_IRQ_pin</name>
      <anchorfile>spi__LPC_8c.html</anchorfile>
      <anchor>6ad5b67346e28c06a14b412451c9c042</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enable_IRQ_pin</name>
      <anchorfile>spi__LPC_8c.html</anchorfile>
      <anchor>df95af49d1fd2524c60090ba3499923a</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_Init</name>
      <anchorfile>spi__LPC_8c.html</anchorfile>
      <anchor>cb06ef92bde513fe8c00bb87ece1e408</anchor>
      <arglist>(int rx_isr_vect)</arglist>
    </member>
    <member kind="function">
      <type>uint16_t</type>
      <name>spi_Read</name>
      <anchorfile>spi__LPC_8c.html</anchorfile>
      <anchor>02fffb26d66333b3c6428ff7a3bbc3fd</anchor>
      <arglist>(uint8_t reg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_Write</name>
      <anchorfile>spi__LPC_8c.html</anchorfile>
      <anchor>9431e598f3537a5369afb01627ad24a3</anchor>
      <arglist>(uint8_t reg, uint16_t val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_Read_Buf</name>
      <anchorfile>spi__LPC_8c.html</anchorfile>
      <anchor>fa22a171b888b8cd8676d160d69d1d7e</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_Write_Buf</name>
      <anchorfile>spi__LPC_8c.html</anchorfile>
      <anchor>a3d5ab927bca2d3c5deb2d49d3bd0be6</anchor>
      <arglist>(struct Message *msg)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>spi_ReadModifiWrite</name>
      <anchorfile>spi__LPC_8c.html</anchorfile>
      <anchor>45835fb3ddc2778b07b2d63b72ccef11</anchor>
      <arglist>(uint8_t reg, uint16_t ormask, uint16_t andmask)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>uart_minibee.c</name>
    <path>/home/data/robot/eurobot/sysless-lpc21xx/app/minibee/</path>
    <filename>uart__minibee_8c</filename>
    <includes id="MC1319xdef_8h" name="MC1319xdef.h" local="yes" imported="no">MC1319xdef.h</includes>
    <member kind="function">
      <type>void</type>
      <name>uart_send_char</name>
      <anchorfile>uart__minibee_8c.html</anchorfile>
      <anchor>7025f92d7c751a4ff7812be0f67c2de1</anchor>
      <arglist>(char val)</arglist>
    </member>
    <member kind="function">
      <type>char</type>
      <name>uart_get_char</name>
      <anchorfile>uart__minibee_8c.html</anchorfile>
      <anchor>62268b25625c1c7803674cb13c5ec518</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init_uart</name>
      <anchorfile>uart__minibee_8c.html</anchorfile>
      <anchor>c7b3df0fa68526d64c732d5f916e34b1</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>send_rs_str</name>
      <anchorfile>uart__minibee_8c.html</anchorfile>
      <anchor>f67eb7f2c24524147b3c3b82f534e138</anchor>
      <arglist>(char data[])</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>send_rs_int</name>
      <anchorfile>uart__minibee_8c.html</anchorfile>
      <anchor>94258c25fb6ea7026fa3de55ce8d32d1</anchor>
      <arglist>(int val)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>uart_minibee.h</name>
    <path>/home/data/robot/eurobot/sysless-lpc21xx/app/minibee/</path>
    <filename>uart__minibee_8h</filename>
    <member kind="function">
      <type>void</type>
      <name>uart_send_char</name>
      <anchorfile>uart__minibee_8h.html</anchorfile>
      <anchor>7025f92d7c751a4ff7812be0f67c2de1</anchor>
      <arglist>(char val)</arglist>
    </member>
    <member kind="function">
      <type>char</type>
      <name>uart_get_char</name>
      <anchorfile>uart__minibee_8h.html</anchorfile>
      <anchor>62268b25625c1c7803674cb13c5ec518</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>init_uart</name>
      <anchorfile>uart__minibee_8h.html</anchorfile>
      <anchor>c7b3df0fa68526d64c732d5f916e34b1</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>send_rs_int</name>
      <anchorfile>uart__minibee_8h.html</anchorfile>
      <anchor>94258c25fb6ea7026fa3de55ce8d32d1</anchor>
      <arglist>(int val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>send_rs_str</name>
      <anchorfile>uart__minibee_8h.html</anchorfile>
      <anchor>f67eb7f2c24524147b3c3b82f534e138</anchor>
      <arglist>(char data[])</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>Message</name>
    <filename>structMessage.html</filename>
    <member kind="variable">
      <type>uint8_t</type>
      <name>done</name>
      <anchorfile>structMessage.html</anchorfile>
      <anchor>000a1958e29d9c3ef65e53c143be1884</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>error</name>
      <anchorfile>structMessage.html</anchorfile>
      <anchor>58cdbd3bb182ccba3f374eeec2e87f36</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>len</name>
      <anchorfile>structMessage.html</anchorfile>
      <anchor>0e2f37b0ed471c18c4d1a71a23981c23</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>uint8_t</type>
      <name>data</name>
      <anchorfile>structMessage.html</anchorfile>
      <anchor>dc2ecd460d7b67a67e55993c13f89a22</anchor>
      <arglist>[25]</arglist>
    </member>
  </compound>
</tagfile>
