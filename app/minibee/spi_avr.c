//
// Author: Bc. Jiri Kubias <Jiri.kubias@gmail.com>, (C) 2008
//
// Copyright: (c) DCE FEE CTU - Department of Control Engeneering
// License: GNU GPL v.2
//

/**
 * @file   spi_avr.c
 * @author Bc. Jiri Kubias , DCE FEL CTU 2008
 * 
 * @brief  SPI, IRQ and pin inicialization, platform dependent!
 * 	Contains support functions for SPI communiacations. 
 * 	This portage is not fully ported, some functions are missing.
 * 	This part is no longer supprted.
 * 
 */



#include "MC1319xdef.h"


void spi_Init(void)		// inits SPI and hold MC radio in reset state
{
	// setting SPI port
	PORTB |= 0xFF;							// enables pullups on PORT B
	DDRB = (1<<DD_MOSI)|(1<<DD_SCK);		// sets MOSI and CLK as output
	SPCR = (1<<SPR0) | (1<<SPR1) | (1<<MSTR) | (1<<SPE); // enable SPI, set 
									//Master mode,clock: fosc/4 , CPOl = CPHA =0

	// setting PD port
	PORTD = (1<<IRQ) | (1<<CE) | (1<<ATTN);	// enables pullups on IRQ and sets CE to HI (continue in nezt row)
	DDRD  = (1<<ATTN)|(1<<RXTXEN)|(1<<RST)| (1<<CE); // sete control bits to output	

}

uint16_t spi_Read (uint8_t reg)
{
	uint16_t data =0;

	reg &= 0x3F;	// protection before too high value

	CLR(CE);
	
	SPDR = reg | RD;// zapis prvniho byte	
	while((SPSR & 0x80) != 0x80);


	SPDR = 0;	// zapis druhyho byte a cteni
	while((SPSR & 0x80) != 0x80);
	data = (SPDR << 8);
	
	
	SPDR = 0;	// zapis druhyho byte a cteni
	while((SPSR & 0x80) != 0x80);
	data |= SPDR;
	
	SET(CE);

	return data;
}



void spi_Write (uint8_t reg, uint16_t val) // writes 16 bites from given register
{
	
	

	reg &= 0x3F;	// protection before too high value

	CLR(CE);
	
	SPDR = reg | WR;// zapis prvniho byte	
	while((SPSR & 0x80) != 0x80);


	SPDR = (val>>8) & 0xFF;	// zapis druhyho byte a cteni
	while((SPSR & 0x80) != 0x80);
	
	
	
	SPDR = val & 0xFF;	// zapis druhyho byte a cteni
	while((SPSR & 0x80) != 0x80);
	
	
	SET(CE);
}



void spi_Read_Buf (struct Message *msg) // write message to MC radio message buffer
{
	
	uint8_t i =0;
	uint8_t dummy =0;
	

	CLR(CE);
	
	SPDR = RX_PKT_RAM | RD;			// init write	
	while((SPSR & 0x80) != 0x80);



		SPDR = 0x00;	// dummy read
		while((SPSR & 0x80) != 0x80);
		SPDR = 0x00;	// dummy read
		while((SPSR & 0x80) != 0x80);

	for( i = 0; i < (msg->len - 2); i++)
	{

		SPDR = 0;		// cteni
		while((SPSR & 0x80) != 0x80);

		dummy = SPDR;

		if ((i % 2) == 0) msg->data[i + 1] = dummy;
		else msg->data[i - 1] = dummy;


	}
	
	
	if((msg->len % 2) ==1) 
	{
	
		SPDR = 0x00;	// dummy read
		while((SPSR & 0x80) != 0x80);
	}
	
	SET(CE);
}


void spi_Write_Buf (struct Message *msg) // write message to MC radio message buffer
{
	
	uint8_t i =0;
	

	CLR(CE);
	
	SPDR = TX_PKT_RAM | WR;			// init write	
	while((SPSR & 0x80) != 0x80);


	for( i = 0; i < msg->len; i++)
	{

		SPDR = msg->data[i];		// zapis druhyho byte a cteni
		while((SPSR & 0x80) != 0x80);
	}
	
	
	if((msg->len % 2) ==1) 
	{
	
		SPDR = 0x00;	// dummy write
		while((SPSR & 0x80) != 0x80);
	}
	
	SET(CE);
}

void spi_ReadModifiWrite (uint8_t reg, uint16_t ormask, uint16_t andmask) // writes 16 bites from given register
{	

	uint16_t data =0;

	reg &= 0x3F;	// protection before too high value

	CLR(CE);
	
	SPDR = reg | RD;// zapis prvniho byte	
	while((SPSR & 0x80) != 0x80);


	SPDR = 0;		// zapis druhyho byte a cteni
	while((SPSR & 0x80) != 0x80);
	data = (SPDR << 8);
	
	
	SPDR = 0;		// zapis druhyho byte a cteni
	while((SPSR & 0x80) != 0x80);
	data |= SPDR;
	
	SET(CE);

	data &= andmask;
	data |= ormask;

	CLR(CE);
	
	SPDR = reg | WR;// zapis prvniho byte	
	while((SPSR & 0x80) != 0x80);


	SPDR = (data>>8) & 0xFF;	// zapis druhyho byte a cteni
	while((SPSR & 0x80) != 0x80);
		
	
	SPDR = data & 0xFF;	// zapis druhyho byte a cteni
	while((SPSR & 0x80) != 0x80);
		
	SET(CE);
}
