//
// Author: Bc. Jiri Kubias <Jiri.kubias@gmail.com>, (C) 2008
//
// Copyright: (c) DCE FEE CTU - Department of Control Engeneering
// License: GNU GPL v.2
//
/**
 * @file   MC1319x.h
 * @author Bc. Jiri Kubias , DCE FEL CTU 2008
 * 
 * @brief  Function prototypes for MC1319x.c
 * 
 */


#ifndef MC1319x_H
#define MC1319x_H


#include "MC1319xdef.h"


uint8_t MC_Reset(void);		// generate reset on MC1319x
uint16_t MC_WhoAmI(void);	// returns ID of MC1319x
uint8_t MC_ED(void);		// measure ED on current channel
uint8_t MC_SendPaket(struct Message *msg); // send message wia zigbee
uint8_t MC_ReSend(struct Message *msg); // send message wia zigbee
uint8_t MC_RecievePaket(struct Message *msg);	// Initate recieving packet 
uint8_t MC_SetClko(uint16_t tick, uint8_t enable); // sets and enables CLKO
uint8_t MC_SetPa(uint8_t val); // sets PA level
void dummy_wait();		// dummy wait cycle

void ext_isr(void) __attribute__ ((interrupt));	///< protype if ISR function, platform dependent

#endif //MC1319x_H
