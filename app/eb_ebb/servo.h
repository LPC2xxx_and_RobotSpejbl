/**
 * @file   servo.h
 * 
 * @brief  FIXME
 * 	This file provides simply how-to use eb_ebb library.
 * 	From main function is called init_perip function 
 * 	where is initialized servos, engines, power switch,
 * 	CAN,ADC and serial port. After this initialization is shown 
 *  how to control each devices. This sample also include simply 
 *  sample of sending and receiving CAN message.
 * 
 */



#ifndef SERVO_H
#define SERVO_H



/** gobal time
 *  @note incremented twenty 20ms, overrun every 1194hours
 */
volatile unsigned int time_ms;

/** Initialize servos
 *  @note All three servos - should be fixed FIXME
 */
void init_servo(unsigned rx_isr_vect);


/** Sets serv position
 *  @return 0 
 *  @note VPB = APB   - name conflict FIXME
 *  @param	servo	define servo
 *  @param	position	new position for servo
 */
void set_servo(char servo, char position);

#endif
