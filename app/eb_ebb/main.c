// $Id$
// $Name$
// $ProjectName$

/**
 * \mainpage EB-ebb code sample
 *
 * \section Introduction
 * This codes are designed for use with ebBoard developed in DCE CVUT.
 * There are two purpose of this program: 
 *
 * 	1) library named ebb, it contains function for handling engines,
 * 	   servos, ADC (median or avery filtered) and power switch.
 *
 *	2) in main.c file is simply HOW-TO use this libraries
 *
 * Short description of ebb library:
 * 
 *	ADC � This library use 4 ADC channels which parameters I defined 
 *	in adc.h or adc_filtr.h.  There is two implementation of ADC output
 *	filter. First is simply averaging ((last value + actual value)/2),
 *	for this filter you have to include adc.h. The Second filter is 
 *	median filter in adc_filtr.h . The size of this filter is defined
 *	in adc_filtr.h. 
 *
 *	PowerSwitch � ebboard have one 6A power switch which can be used for
 *	 switching small loads (capacity or resistant recommended). In powswitch.h is defined initialization, on and off function.
 *
 *	Servos � ebboard have three servo connectors. In servo.h is defined 
 *	functions for setting them. The servo range is divided into 256 (0~255)
 *	 steps.
 *
 *	Engines � this part can controls up two DC motors (5A each) or in 
 *	special cases one motor (up to 10A). In engines.h is defined several 
 *	controls function. Read carefully description in this section. Is 
 *	important to understanding the way how to stop engines. 
 *
 */

/**
 * @file   main.c
 * 
* @brief  Demo application demonstrating use of eb_ebb library.
 * 	This file provides simply how-to use eb_ebb library.
 * 	From main function is called init_perip function 
 * 	where is initialized servos, engines, power switch,
 * 	CAN,ADC and serial port. After this initialization is shown 
 *  how to control each devices. This sample also include simply 
 *  sample of sending and receiving CAN message.
 * 
 */


#include <lpc21xx.h>                            /* LPC21xx definitions */
#include <errno.h>
#include <periph/can.h>
#include <string.h>
#include <deb_led.h>
#include "uart.h"
#include "servo.h"
#include "powswitch.h"
#include "engine.h"
//#include "adc.h"		// header ADC with averaging filter
#include "adc_filtr.h"		// header ADC with mean filter	




#define	CAN_SPEED 	1000000		///< CAN speed

#define CAN_SERVO	0x32		///< CAN ID for servo message

/**
 * Variables ISR values
 */
/*@{*/
#define CAN_ISR		0
#define ENG_ISR		1
#define ADC_ISR		3
#define SERVO_ISR	5
/*@}*/

#define SPEED		30	///<  Motor speed


/**
 *  Dummy wait (busy wait)
 *  This function shoul be removed in future
 */ 
void dummy_wait(void)
{
    int i = 1000000;
    
    while(--i);
}

/**
 *  This function is called when we recieve CAN message.
 *  Its called from CAN ISR
 *
 * @param *msg	structure of can message (can_msg_t)
 *
 */ 
void can_rx(can_msg_t *msg) {
	can_msg_t rx_msg;
	
	memcpy(&rx_msg, msg, sizeof(can_msg_t));

	switch (rx_msg.id) {		// demo sample parsing recieved message by ID
		case CAN_SERVO:
			
			set_servo(0, rx_msg.data[0]);
			set_servo(1, rx_msg.data[1]);
			set_servo(2, rx_msg.data[2]);
			break;

		default:
			break;
	}
}


/**
 *  Inicializes pepherials used in ebb library - sample use
 *
 */ 
void init_perip(void)	   
{
	init_servo(SERVO_ISR);
	init_pow_switch();

	init_engine_A();
	init_engine_B();

	init_uart0((int)9600 ,UART_BITS_8, UART_STOP_BIT_1, UART_PARIT_OFF, 0 );
	//init_adc(ADC_ISR);			// inicialization ADC with averaging filter
	init_adc_filter(ADC_ISR);		// inicialization ADC with mean filter
	can_init_baudrate(CAN_SPEED, CAN_ISR, can_rx);
}


/**
 *  Main function contains a small sample how to use ebb library.
 *
 */
int main (void)  {

	init_perip();			// sys init MCU

	set_servo(0, 0x80);		// set servo 1 to center position 
	set_servo(1, 0x80);		// set servo 2 to center position 
	set_servo(2, 0x80);		// set servo 3 to center position 

	engine_A_dir(ENGINE_DIR_FW);		// set engine A to direction forward
	engine_B_dir(ENGINE_DIR_BW);		// set engine B to direction backward
	engine_A_en(ENGINE_EN_ON);		// enables engine A
	engine_B_en(ENGINE_EN_ON);		// enables engine B
	engine_A_pwm(SPEED);			// sets speed on Engine A
	engine_B_pwm(SPEED);			// sets speed on Engine B

	
	can_msg_t msg;			// This part is sending CAN messge 
	msg.id = 0xAA;			// CAN message ID
	msg.flags = 0;			
	msg.dlc = 1;			// number of transmited data bytes
	msg.data[0] = 0x55;		// data byte
	while(can_tx_msg(&msg));	// sending function
	

	while(1)		// this will blink for ever
	{
		__deb_led_on(LEDG);
		dummy_wait();
		__deb_led_off(LEDG);
		dummy_wait();
	} 
}



