#include <lpc21xx.h>                          // LPC21XX Peripheral Registers
#include <types.h> 
#include "adc.h"
#include <stdlib.h>


#define ADCCH0 22	///< ADC0 value for PINSEL
#define ADCCH1 24	///< ADC1 value for PINSEL
#define ADCCH2 26	///< ADC2 value for PINSEL
#define ADCCH3 28	///< ADC3 value for PINSEL

/**
 *  ADC ISR routine. This routine reads selected ADC value and
 *  multiplies it by #ADC_MUL and adds #ADC_OFFSET to calculate the
 *  volage (3.25mV/div).  After this reading the next ADC channel is
 *  set up for measuring.
 */ 
void adc_isr(void) __attribute__ ((interrupt));
void adc_isr(void) 
{
	unsigned char chan =0;
	unsigned int val =0;


	chan = (char) ((ADDR>>24) & 0x07);
	val = (((((ADDR >> 6) & 0x3FF) * ADC_MUL + ADC_OFFSET) + adc_val[chan]) >> 1) ;



	adc_val[chan] = val;

	ADCR &= ~(ADC_CR_START_OFF_m);


	switch(chan)
	{
		case 0:
			ADCR = ((ADC_CR_ADC1_m) | (ADC_CR_CLKS_11_m) | (ADC_CR_PDN_ON_m) | (ADC_CR_START_NOW_m) | (20*ADC_CR_CLK_DIV_1_m));
			break;

		case 1:
			ADCR = ((ADC_CR_ADC2_m) | (ADC_CR_CLKS_11_m) | (ADC_CR_PDN_ON_m) | (ADC_CR_START_NOW_m) | (20*ADC_CR_CLK_DIV_1_m));
			break;
			
		case 2:
			ADCR = ((ADC_CR_ADC3_m) | (ADC_CR_CLKS_11_m) | (ADC_CR_PDN_ON_m) | (ADC_CR_START_NOW_m) | (20*ADC_CR_CLK_DIV_1_m));
			break;
			
		case 3:
			ADCR = ((ADC_CR_ADC0_m) | (ADC_CR_CLKS_11_m) | (ADC_CR_PDN_ON_m) | (ADC_CR_START_NOW_m) | (20*ADC_CR_CLK_DIV_1_m));
			break;															 
	}
	
	 VICVectAddr = 0;

}

/**
 *  Inicializes ADC service for all ADC (4) channels and installs ISR routine to VIC.
 *   Automaticly starts the conversion of first channel on given conversion frequency.
 */ 
void init_adc(unsigned rx_isr_vect)
{
	
	PINSEL1 |= ((PINSEL_1 << ADCCH0) | (PINSEL_1 << ADCCH1) | (PINSEL_1 << ADCCH2) | (PINSEL_1 << ADCCH3));		

	((uint32_t*)&VICVectCntl0)[rx_isr_vect] = 0x32;
	((uint32_t*)&VICVectAddr0)[rx_isr_vect] = (unsigned) adc_isr;
	VICIntEnable = 0x40000;

	ADCR = ((ADC_CR_ADC0_m) | (ADC_CR_CLKS_11_m) | (ADC_CR_PDN_ON_m) | (ADC_CR_START_NOW_m) | (ADC_VPB_DIV*ADC_CR_CLK_DIV_1_m));
}
