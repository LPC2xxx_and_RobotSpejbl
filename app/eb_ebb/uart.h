#ifndef UART_H
#define UART_H

#define UART_BITS_5	0x0
#define UART_BITS_6	0x1
#define UART_BITS_7	0x2
#define UART_BITS_8	0x3

#define UART_STOP_BIT_1		(0x0 << 2)
#define UART_STOP_BIT_2 	(0x1 << 2)

#define UART_PARIT_ENA		(0x1 << 3)
#define UART_PARIT_OFF		(0x0 << 3)
#define UART_PARIT_ODD   	(0x0 << 4)
#define UART_PARIT_EVEN  	(0x1 << 4)
#define UART_PARIT_FORCE_1 	(0x2 << 4)
#define UART_PARIT_FORCE_0 	(0x3 << 4)




void init_uart1(int baudrate, char bits, char stopbit, char parit_en, char parit_mode );
void init_uart0(int baudrate, char bits, char stopbit, char parit_en, char parit_mode );
unsigned char uart1GetCh(void);
unsigned char uart0GetCh(void);
void uart1SendCh(char ch);
void uart0SendCh(char ch);

#endif
