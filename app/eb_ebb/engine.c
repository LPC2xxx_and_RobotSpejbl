



#include <lpc21xx.h>
#include <deb_led.h>
#include <system_def.h>
#include "engine.h"

#define ENGINE_PIN_A	7	///< PWM output for engine A, pin P0.7
#define ENGINE_PIN_B	8	///< PWM output for engine B, pin P0.8

#define ENGINE_ENA	28	///< Enable output for engine A, pin P1.28
#define ENGINE_ENB	30	///< Enable output for engine B, pin P1.30

#define ENGINE_IN2A	27	///< Direction output for engine A , pin P1.27
#define ENGINE_IN2B	29	///< Direction output for engine A , pin P1.29

#define PWM_FREQ	20000	///< PWM frequency
#define PWM_RES		100	///< PWM resolution

#define PWM_STEP ((CPU_APB_HZ / PWM_FREQ)/PWM_RES + 1)	///< Calculates smallest step PWM for resolution 0~100%



unsigned int pwm_A = 0;		///< Actual value of PWM A
unsigned int pwm_B = 0;		///< Actual value of PWM B


/**
 *  Enables or disables the ouput on engine A. Causes smooth stop, without break.
 *
 * @param status	status parameters are defined in engine.h
 *
 */ 
void engine_A_en(unsigned status)
{
	if (status == ENGINE_EN_ON) 
		IO1SET |= (1<<ENGINE_ENA);
	else
		IO1CLR |= (1<<ENGINE_ENA);
}

/**
 *  Enables or disables the ouput on engine B. Causes smooth stop, without break.
 *
 * @param status	status parameters are defined in engine.h
 *
 */ 
void engine_B_en(unsigned status)
{
	if (status == ENGINE_EN_ON) 
		IO1SET |= (1<<ENGINE_ENB);
	else
		IO1CLR |= (1<<ENGINE_ENB);
}

/**
 *  Changes motor A direction
 *
 * @param dir	direction parameters are defined in engine.h
 *
 */ 
void engine_A_dir(unsigned dir)
{
	if (dir == ENGINE_DIR_BW) 
		IO1SET |= (1<<ENGINE_IN2A);
	else
		IO1CLR |= (1<<ENGINE_IN2A);

	engine_A_pwm(pwm_A);
	
}

/**
 *  Changes motor B direction
 *
 * @param dir	direction parameters are defined in engine.h
 *
 */ 
void engine_B_dir(unsigned dir)
{
	if (dir == ENGINE_DIR_BW) 
		IO1SET |= (1<<ENGINE_IN2B);
	else
		IO1CLR |= (1<<ENGINE_IN2B);

	engine_B_pwm(pwm_B);
}

/**
 *  Sets motor A PWM value
 *
 * @param pwm	Unsigned int, 0~100%
 *
 * @note 0 causes fast stop
 */ 
void engine_A_pwm(unsigned pwm)
{
	if (pwm>100) pwm = 100;

	pwm_A = pwm;
	if (IO1PIN & (1<<ENGINE_IN2A))	PWMMR2 = PWM_STEP  * (100 - pwm);
	else PWMMR2 = PWM_STEP  * (pwm);
	PWMLER |= PWMLER_LA2_m;

}

/**
 *  Sets motor B PWM value
 *
 * @param pwm	Unsigned int, 0~100%
 *
 * @note 0 causes fast stop
 */ 
void engine_B_pwm(unsigned pwm)
{
	if (pwm>100) pwm = 100;
	pwm_B = pwm;
	if (IO1PIN & (1<<ENGINE_IN2B))	PWMMR4 = PWM_STEP  * (100 - pwm);
	else PWMMR4 = PWM_STEP  * (pwm);	
	PWMLER |= PWMLER_LA4_m;

}


/**
 *  Inicializes Engine A - enables PWM outputs and sets IOs. Uses PWM channel - PWMMR2.
 *  If is somthing other set on PWM channels, it will affect the main PWM frequency	
 *  If the engine B is set it will afect its output for one cycle
 *
 * 
 */ 
void init_engine_A()
{
	PINSEL0 &= ~((PINSEL_3 << 14) );
	PINSEL0 |= (PINSEL_2 << 14) ;

	IO0DIR |= (1<<ENGINE_PIN_A);
	IO1DIR |= (1<<ENGINE_ENA) | (1<<ENGINE_IN2A);
	IO1SET |= (1<<ENGINE_ENA) | (1<<ENGINE_IN2A);
	
	PWMPR = 0;
	PWMMR0 = CPU_APB_HZ / 20000;

	PWMMR2 =0;	
	
	PWMPCR |= PWMPCR_PWMENA2_m;
	PWMLER |= PWMLER_LA0_m | PWMLER_LA2_m;
	PWMMCR |= PWMMCR_PWMMR0R_m;
	PWMTCR |= PWMTCR_CE_m | PWMTCR_EN_m;	
}

/**
 *  Inicializes Engine B - enables PWM outputs and sets IOs. Uses PWM channel - PWMMR2.
 *  If is somthing other set on PWM channels, it will affect the main PWM frequency	
 *  If the engine A is set it will afect its output for one cycle
 *
 * 
 */ 
void init_engine_B()
{
	PINSEL0 &= ~((PINSEL_3 << 16));
	PINSEL0 |= (PINSEL_2 << 16);

	IO0DIR |= (1<<ENGINE_PIN_B);
	IO1DIR |= (1<<ENGINE_ENB) | (1<<ENGINE_IN2B);

	IO1SET |= (1<<ENGINE_ENB) | (1<<ENGINE_IN2B);

	
	PWMPR = 0;
	PWMMR0 = CPU_APB_HZ / 20000;

	PWMMR4 = 0;
	
	PWMPCR |= PWMPCR_PWMENA4_m;
	PWMLER |= PWMLER_LA0_m | PWMLER_LA4_m;
	PWMMCR |= PWMMCR_PWMMR0R_m;
	PWMTCR |= PWMTCR_CE_m | PWMTCR_EN_m;	
}