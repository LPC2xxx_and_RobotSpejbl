#ifndef ADC_H
#define ADC_H


#define ADC_MUL 	1	///< This value multiplies mesured value from ADC
#define ADC_OFFSET	0	///< This valueis added to mesured value from ADC	

#define ADC_VPB_DIV	255	///< VPB divisor for ADC	FIXME




volatile unsigned int adc_val[4];	///<  measured ADC values





/**	inicializes ADC lines and starts converion (use ISR)
 *  @param rx_isr_vect  ISR vector
 */
void init_adc(unsigned rx_isr_vect);

#endif
