/** cerveaumotor Spejbla **/

#include <stdlib.h>
#include <types.h>
#include <limits.h>
#include <lpc21xx.h>
#include <periph/can.h>
#include <string.h>
#include "pwm.h"

/* * */

/* CAN message IDs */
#define CAN_CLOCK_ID  0x401
//#define CAN_CLOCK_ID  0x481
#define CAN_CFGMSG_ID 0x4ab
#define CAN_UNDEF_ID  0xf800

/* unique motor/CAN ID */
#define CANLOAD_ID (*((volatile unsigned long *) 0x40000120))
#define MOTOR_ID CANLOAD_ID

/* potentiometer bounds */
#define POS_MIN 0x060
#define POS_MAX 0x3a0
/* reserved value for power-off */
#define CTRL_OFF 0xffff
#define CTRL_MAX SHRT_MAX

/* own message for current position signalling */
can_msg_t tx_msg = {.flags = 0, .dlc = 2};

/* command message ID and index */
uint16_t cmd_msg_id = CAN_UNDEF_ID, cmd_msg_ndx = 0;

/* command value, current position */
volatile uint16_t cmd_value = CTRL_OFF, rx_cmd_value = CTRL_OFF,
  position, pos_data;
volatile uint8_t tx_request = 0;

/* * */

void timer_irq() __attribute__((interrupt));

void timer_init(uint32_t prescale, uint32_t period) {
  T0PR = prescale - 1;
  T0MR1 = period - 1;
  T0MCR = 0x3<<3; /* interrupt and counter reset on match1 */
  T0EMR = 0x1<<6 | 0x2; /* set MAT0.1 low on match and high now */
  T0CCR = 0x0; /* no capture */
  T0TCR |= 0x1; /* go! */
}

void timer_init_irq(unsigned irq_vect) {
  /* set interrupt vector */
  ((uint32_t*)&VICVectAddr0)[irq_vect] = (uint32_t)timer_irq;
  ((uint32_t*)&VICVectCntl0)[irq_vect] = 0x20 | 4;
  /* enable timer int */
  VICIntEnable = 0x00000010;
}

void adc_init(unsigned clk_div) {
  ADCR = 0x01 | ((clk_div&0xff)<<8) | 0x200000;
  ADCR |= 0x04000000; /* conversion started on falling edge of MAT0.1 */
}

/* * */

inline void sample() {
  /* get last position measured (weak point! sampling jitter!) */
  position = pos_data;
  /* request transmission of obtained value by idle loop */
  tx_request = 1;
}

inline void actuate() {
  /* set previously received command rx_cmd_value */
  if (rx_cmd_value == CTRL_OFF) {
    if (cmd_value != CTRL_OFF) {
      /* switch H-bridge off */
      pwm_set(4, 0);
      pwm_set(6, 0);      
    }
  }
  else
    if (cmd_value == CTRL_OFF) {
      /* switch H-bridge on */

    }
  /* set the value */
  cmd_value = rx_cmd_value;
}

/* .oOo. * controller * .oOo. */

#define REG_P_GAIN (0.0012 * 15)
#define REG_P_GAIN_INV ((int16_t)(1.0/REG_P_GAIN))
inline int16_t regulate(uint16_t w, uint16_t y) {
  int16_t e = (int16_t)w - (int16_t)y;
  int32_t m;

  if (e > REG_P_GAIN_INV)
    return(CTRL_MAX);
  if (e < -REG_P_GAIN_INV)
    return(-CTRL_MAX);
  /* REG_P_GAIN << 1 */
  m = (int32_t)e*CTRL_MAX;
  return(m/REG_P_GAIN_INV);
}

void timer_irq() {
  int16_t u;

  /* wait for A/D conversion to finish */
  while ((ADDR&0x80000000) == 0);
  T0EMR |= 0x2; /* set MAT0.1 high again */
  pos_data = (ADDR>>6)&0x3ff;
  if (cmd_value != CTRL_OFF) {
    /* compute the control */
    u = regulate(cmd_value, pos_data);
    /* apply it */
    if (((pos_data < POS_MIN) && (u < 0)) ||
	((pos_data > POS_MAX) && (u > 0))) {
      /* protect the gear against self-destruction */
      pwm_set(4, 0);
      pwm_set(6, 0);
    }
    else {
      if (u > 0) {
	pwm_set(4, PWMMR0*u/CTRL_MAX);
	pwm_set(6, 0);
      }
      else {
	pwm_set(4, 0);
	pwm_set(6, PWMMR0*(-u)/CTRL_MAX);
      }
    }
  }
  /* clear int flag */
  T0IR = 0xffffffff;
  /* int acknowledge */
  VICVectAddr = 0;
}

void can_rx(can_msg_t *msg) {
  can_msg_t rx_msg;

  memcpy(&rx_msg, msg, sizeof(can_msg_t));
  switch (rx_msg.id) {
  case CAN_CLOCK_ID:
    sample();
    actuate();
    break;
  case CAN_CFGMSG_ID:
    if (((uint16_t*)rx_msg.data)[0] == MOTOR_ID) {
      cmd_msg_id = ((uint16_t*)rx_msg.data)[1];
      cmd_msg_ndx = ((uint16_t*)rx_msg.data)[2];
    }
    break;
  default:
    if (rx_msg.id == cmd_msg_id)
      rx_cmd_value = ((uint16_t*)rx_msg.data)[cmd_msg_ndx];
    break;
  }
}

int main() {
  /* peripheral clock = CPU clock (10MHz) */
  VPBDIV = 1;
  /** map exception handling to RAM **/
  MEMMAP = 0x2;
  /* init Vector Interrupt Controller */
  VICIntEnClr = 0xFFFFFFFF;
  VICIntSelect = 0x00000000;
  /* * */
  /* 10MHz VPB, 1000kb/s TSEG1=6, TSEG2=3, SJW= */
  can_init(0x250000, 14, can_rx);
  tx_msg.id = MOTOR_ID;
  /***/
  adc_init(3);
  pwm_channel(4, 0);
  pwm_channel(6, 0);
  pwm_init(1, 500 /*20kHz*/);
  /* initially, switch H-bridge off */
  pwm_set(4, 0);
  pwm_set(6, 0);
  /* * */
  timer_init(10, 1000 /*1000Hz*/);
  timer_init_irq(13);
  /* jamais fatigue */
  for (;;)
    if (tx_request) {
      *((uint16_t*)tx_msg.data) = position;
      while (can_tx_msg(&tx_msg));
      tx_request = 0;
    }
}

/* KOHE||, */
